-- -----------------------------------------------------
-- Data for table `sacta`.`privilegios`
-- -----------------------------------------------------
START TRANSACTION;
USE `sacta`;
INSERT INTO `sacta`.`privilegios` (`idprivilegio`, `privilegio`) VALUES (1, 'Participante');
INSERT INTO `sacta`.`privilegios` (`idprivilegio`, `privilegio`) VALUES (2, 'Credenciador');
INSERT INTO `sacta`.`privilegios` (`idprivilegio`, `privilegio`) VALUES (3, 'Organizador');

COMMIT;


-- -----------------------------------------------------
-- Data for table `sacta`.`situacoes`
-- -----------------------------------------------------
START TRANSACTION;
USE `sacta`;
INSERT INTO `sacta`.`situacoes` (`idsituacao`, `situacao`) VALUES (1, 'Pre-Inscrito');
INSERT INTO `sacta`.`situacoes` (`idsituacao`, `situacao`) VALUES (2, 'Confirmado');

COMMIT;


-- -----------------------------------------------------
-- Data for table `sacta`.`categoria`
-- -----------------------------------------------------
START TRANSACTION;
USE `sacta`;
INSERT INTO `sacta`.`categoria` (`idcategoria`, `categoria`, `valor`) VALUES (1, 'Ouvinte', 15.00);

COMMIT;


-- -----------------------------------------------------
-- Data for table `sacta`.`tiposAtividade`
-- -----------------------------------------------------
START TRANSACTION;
USE `sacta`;
INSERT INTO `sacta`.`tiposAtividade` (`idtipoAtividade`, `tipoAtividade`) VALUES (1, 'Competicao');
INSERT INTO `sacta`.`tiposAtividade` (`idtipoAtividade`, `tipoAtividade`) VALUES (2, 'Conferencia');
INSERT INTO `sacta`.`tiposAtividade` (`idtipoAtividade`, `tipoAtividade`) VALUES (3, 'Minicurso');
INSERT INTO `sacta`.`tiposAtividade` (`idtipoAtividade`, `tipoAtividade`) VALUES (4, 'Palestra');
INSERT INTO `sacta`.`tiposAtividade` (`idtipoAtividade`, `tipoAtividade`) VALUES (5, 'Outro');

COMMIT;


-- -----------------------------------------------------
-- Data for table `sacta`.`tipoIntervalo`
-- -----------------------------------------------------
START TRANSACTION;
USE `sacta`;
INSERT INTO `sacta`.`tipoIntervalo` (`idtipoIntervalo`, `intervalo`) VALUES (1, 'Coffee Break');
INSERT INTO `sacta`.`tipoIntervalo` (`idtipoIntervalo`, `intervalo`) VALUES (2, 'Almoco');
INSERT INTO `sacta`.`tipoIntervalo` (`idtipoIntervalo`, `intervalo`) VALUES (3, 'Intervalo');

COMMIT;

START TRANSACTION;
USE `sacta`;
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (12, 'Acre', 'AC', 1);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (27, 'Alagoas', 'AL', 2);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (16, 'Amapá', 'AP', 1);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (13, 'Amazonas', 'AM', 1);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (29, 'Bahia', 'BA', 2);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (23, 'Ceará', 'CE', 2);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (53, 'Distrito Federal', 'DF', 5);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (32, 'Espírito Santo', 'ES', 3);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (52, 'Goiás', 'GO', 5);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (21, 'Maranhão', 'MA', 2);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (51, 'Mato Grosso', 'MT', 5);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (50, 'Mato Grosso do Sul', 'MS', 5);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (31, 'Minas Gerais', 'MG', 3);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (15, 'Pará', 'PA', 1);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (25, 'Paraíba', 'PB', 2);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (41, 'Paraná', 'PR', 4);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (26, 'Pernambuco', 'PE', 2);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (22, 'Piauí', 'PI', 2);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (33, 'Rio de Janeiro', 'RJ', 3);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (24, 'Rio Grande do Norte', 'RN', 2);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (43, 'Rio Grande do Sul', 'RS', 4);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (11, 'Rondônia', 'RO', 1);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (14, 'Roraima', 'RR', 1);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (42, 'Santa Catarina', 'SC', 4);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (35, 'São Paulo', 'SP', 3);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (28, 'Sergipe', 'SE', 2);
Insert into Estados (CodigoUf, Nome, Uf, Regiao) values (17, 'Tocantins', 'TO', 1);
COMMIT;



START TRANSACTION;
USE `sacta`;
INSERT INTO `sacta`.`evento` (`idEvento`, `titulo`, `dataInicio`, `dataFim`, `local`, `idEstado`, `cidade`, `responsavel`, `email`, `telefone`, `descricao`, `cargaHoraria`, `idiomaEvento`, `sigla`) VALUES ('1', '10ª Semana Acadêmica do Campus Tecnológico de Alegrete', '2018-06-25 08:30:00', '2018-06-25 22:30', 'Unipampa Alegrete', '21', 'Alegrete', 'CEC', 'a@a.com', '0', 'descricao-db', '60', 'Brasil', 'SACTA X');

COMMIT;