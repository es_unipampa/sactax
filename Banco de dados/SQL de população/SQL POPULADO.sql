-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 20-Jun-2018 às 11:01
-- Versão do servidor: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cecalegr_sacta`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividades`
--

CREATE TABLE `atividades` (
  `idatividade` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `inicio` datetime NOT NULL,
  `fim` datetime NOT NULL,
  `cargaHoraria` time NOT NULL,
  `idtipoAtividade` int(11) NOT NULL,
  `idintervaloAtividade` int(11) DEFAULT NULL,
  `idlocalAtividade` int(11) NOT NULL,
  `ministrante` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `atividades`
--

INSERT INTO `atividades` (`idatividade`, `nome`, `descricao`, `inicio`, `fim`, `cargaHoraria`, `idtipoAtividade`, `idintervaloAtividade`, `idlocalAtividade`, `ministrante`) VALUES
(1, 'Palestra Oficial de Abertura', 'Público alvo: LIVRE', '2018-06-25 14:00:00', '2018-06-25 15:30:00', '02:00:00', 4, NULL, 1, ' '),
(2, 'Aditivos Quimicos para concretos e argamassa', 'Público alvo: Engenharia Civil', '2018-06-25 16:00:00', '2018-06-25 18:00:00', '02:00:00', 4, 2, 1, 'Marcos Rorato'),
(3, 'Python e suas principais Bibliotecas', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-25 19:00:00', '2018-06-25 22:30:00', '04:00:00', 3, 3, 4, 'Wesley Ferreira'),
(4, 'Arduino e Prototipagem (Teoria/Prática - Básico)', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-25 19:00:00', '2018-06-25 22:30:00', '04:00:00', 3, 3, 3, 'David Martins'),
(5, 'Barragens de Terra', 'Público alvo: Engenharia Civil - Engenharia Agrícola', '2018-06-25 19:00:00', '2018-06-25 20:30:00', '02:00:00', 4, NULL, 14, 'Jaelson Budny'),
(6, 'Photoshop para iniciantes', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-25 19:00:00', '2018-06-25 22:30:00', '04:00:00', 3, 3, 2, 'Adriel Rodrigues'),
(7, 'Prototipagem Virtual - Simulação via CFD - Higra: Inovação e Sustentabilidade', 'Público alvo: Engenharia Elétrica', '2018-06-26 08:30:00', '2018-06-26 10:00:00', '02:00:00', 4, NULL, 1, 'Greco Tusset de Moura - Diretor Técnico da HIGRA'),
(8, 'Oficina Excel Básico - Parte 1 e Parte 2', 'Público alvo: Livre', '2018-06-26 08:30:00', '2018-06-26 12:00:00', '04:00:00', 3, 4, 14, 'Equipe Excel Unipampa'),
(9, 'Oficina - Argamassa Estabilizada - Parte 1 e Parte 2', 'Público alvo: Engenharia Civil', '2018-06-26 08:30:00', '2018-06-26 12:00:00', '04:00:00', 3, 4, 6, 'Aldo Temp e Equipe'),
(10, 'Energia Elétrica na Irrigação da Lavoura de Arroz', 'Público alvo: Engenharia Elétrica - Engenharia Agrícola', '2018-06-26 08:30:00', '2018-06-26 10:00:00', '02:00:00', 4, NULL, 15, 'Sirnei Nunes Da Silva'),
(11, 'Bombas anfíbias e turbogeradores anfíbios - Higra: Inovação e Sustentabilidade', 'Público alvo: Engenharia Elétrica', '2018-06-26 10:30:00', '2018-06-26 12:00:00', '02:00:00', 4, NULL, 1, 'Greco Tusset de Moura - Diretor Técnico da HIGRA'),
(12, 'Instalações de Gás', 'Público alvo: Engenharia Civil', '2018-06-26 10:30:00', '2018-06-26 12:00:00', '02:00:00', 4, NULL, 11, 'Dieison Gabbi Fantineli'),
(13, 'Segurança nas Rodovias', 'Público alvo: Livre', '2018-06-26 14:00:00', '2018-06-26 15:30:00', '02:00:00', 4, NULL, 1, 'Policial Rodoviário Federal Fogassa'),
(14, 'Minicurso Introdução ao MatLab - Turma 1', 'Público alvo: Engenharia Elétrica', '2018-06-26 14:00:00', '2018-06-26 18:00:00', '04:00:00', 3, 6, 2, '	Maurício de Souza Ferreira, Caroline Bremm e Cristian Muller'),
(15, 'Responsabilidade Técnica', 'Público alvo: Engenharia Civil', '2018-06-26 14:00:00', '2018-06-26 15:30:00', '02:00:00', 4, NULL, 6, 'Katea - Inspetora Geral CREA RS'),
(16, 'CREA e o papel da fiscalização e inspetorias na sociedade', 'Público alvo: Engenharia Civil', '2018-06-26 14:00:00', '2018-06-26 15:30:00', '02:00:00', 4, NULL, 15, 'Katia Messa / Mauro Briao'),
(17, 'Minicurso: \"SolidWorks - Saindo do Básico\"', 'Público alvo: Engenharia Mecânica', '2018-06-26 14:00:00', '2018-06-26 18:00:00', '04:00:00', 3, 6, 4, 'Fernando Hendges'),
(18, 'IoT - Seguranca da Informacao na Prática', 'Público alvo: Ciência da Computação', '2018-06-26 16:00:00', '2018-06-26 18:00:00', '02:00:00', 4, NULL, 1, 'Paulo Lino'),
(19, 'Licitações', 'Público alvo: Engenharia Civil', '2018-06-26 16:00:00', '2018-06-26 18:00:00', '02:00:00', 4, NULL, 15, 'José Rojas'),
(20, 'Materiais Cerâmicos Refratários', 'Público alvo: Engenharia Civil', '2018-06-26 16:00:00', '2018-06-26 18:00:00', '02:00:00', 4, NULL, 6, '	Alessandro Nunes'),
(21, 'Competição de Inteligência Artificial para jogos', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-26 19:00:00', '2018-06-26 22:30:00', '04:00:00', 1, 8, 4, 'Prof. Jean Cheiran'),
(22, 'Arduino e Prototipagem (T/P - Intermediário)', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-26 19:00:00', '2018-06-26 22:30:00', '04:00:00', 3, 8, 2, 'David Martins'),
(23, 'Uma Análise Crítica dos 130 Anos de Abolição da Escravatura', 'Público alvo: Livre', '2018-06-26 19:00:00', '2018-06-26 20:30:00', '02:00:00', 4, NULL, 1, 'Prof. Francisco Carlos Duarte Vitória'),
(24, '	Oportunidades e Desafios Após a Graduação', 'Público alvo: Engenharia Civil', '2018-06-26 19:00:00', '2018-06-26 20:30:00', '02:00:00', 4, NULL, 6, 'Alunos Egressos'),
(25, 'Atuação do Engenherio Civil na Engenharia Aeroespacial', 'Público alvo: Engenharia Civil', '2018-06-26 19:00:00', '2018-06-26 20:30:00', '02:00:00', 4, NULL, 15, 'Cesar Cristaldo'),
(26, 'Método construtivo: Parede de concreto', 'Público alvo: Livre', '2018-06-26 21:00:00', '2018-06-26 22:30:00', '02:00:00', 4, NULL, 6, 'Daniela Padoin'),
(27, 'Minicurso Introdução ao MatLab - Turma 2', 'Público alvo: Engenharia Elétrica', '2018-06-27 08:30:00', '2018-06-27 12:00:00', '04:00:00', 3, 9, 2, 'Maurício de Souza Ferreira, Caroline Bremm e Cristian Muller'),
(28, 'Minicurso Manutenção de Ar condicionado automotivo', 'Público alvo: Engenharia Mecânica', '2018-06-27 08:30:00', '2018-06-27 12:00:00', '04:00:00', 3, 9, 10, 'Henrique Vicente'),
(29, '“Power World - Análise de Sistemas de potência\"', 'Público alvo: Engenharia Elétrica', '2018-06-27 08:30:00', '2018-06-27 12:00:00', '04:00:00', 3, 9, 3, 'Prof.ª Ana Paula Mello'),
(30, 'Manutenção e instalação de bombas centrífugas de irrigação', 'Público alvo: Engenharia Mecânica - Engenharia Acrícola', '2018-06-27 08:30:00', '2018-06-27 12:00:00', '04:00:00', 3, 9, 9, 'Luziele Oliveira de Oliveira'),
(31, '	Oficina Excel Básico - Parte 3 e Parte 4', 'Público alvo: Livre', '2018-06-27 08:30:00', '2018-06-27 12:00:00', '04:00:00', 3, 9, 14, 'Equipe Excel Unipampa'),
(32, '	Minicurso Introdução ao LateX', 'Público alvo: Livre', '2018-06-27 14:00:00', '2018-06-27 18:00:00', '04:00:00', 3, 11, 2, 'Victor Israel, Melissa Bilher, Sinara Corrêa, Diogo Deus, Luan Miguel e Cristian Muller'),
(33, 'Desafio Pentesting: implementação e exploração', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-27 14:00:00', '2018-06-27 18:00:00', '04:00:00', 1, 11, 4, 'Isadora Ferrão'),
(34, '	Engenharia Agrícola - O Futuro da Agricultura Brasileira', 'Engenharia Agrícola', '2018-06-27 14:00:00', '2018-06-27 15:30:00', '02:00:00', 4, NULL, 11, 'Eng. Agrícola Douglas Adolpho'),
(35, 'Aditivos Quimicos', 'Público alvo: Engenharia Civil', '2018-06-27 16:00:00', '2018-06-27 18:00:00', '02:00:00', 4, NULL, 4, 'Mateus Rorato'),
(36, '	Introdução a processamento paralelo com OpenMP (30 vagas)', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-27 19:00:00', '2018-06-27 22:30:00', '04:00:00', 3, 13, 4, 'Matheus Serpa'),
(37, 'Programação visual com JavaScript', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-27 19:00:00', '2018-06-27 22:30:00', '04:00:00', 3, 13, 3, 'Vitor Ribeiro'),
(38, 'Minicurso Automação com Shell', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-27 19:00:00', '2018-06-27 22:30:00', '04:00:00', 3, 13, 2, 'Sherlon Almeida'),
(39, 'Mecanica Básica', 'Público alvo: Livre', '2018-06-27 19:00:00', '2018-06-27 20:30:00', '02:00:00', 4, NULL, 1, 'Geraldo Haeming'),
(40, 'Os Pilares da engenharia - A importância das disciplinas básicas na vida profissional', 'Público alvo: Livre', '2018-06-27 19:00:00', '2018-06-27 20:30:00', '02:00:00', 4, NULL, 15, 'Cesar Cristaldo'),
(41, 'Palestra Geração Distribuída - Energia Fotovoltaica no Brasil', 'Público alvo: Engenharia Elétrica', '2018-06-28 08:30:00', '2018-06-28 10:00:00', '02:00:00', 4, NULL, 16, 'Prof. Guilherme Silva / Eng. Breno Barrera'),
(42, 'Minicurso Análise de Falhas de Componentes Mecânicos', 'Público alvo: Engenharia Mecânica', '2018-06-28 08:30:00', '2018-06-28 10:00:00', '02:00:00', 3, NULL, 2, 'Isadora Goss, José Marcelo e Ricardo Schneiders'),
(43, 'Mesa Redonda Geração Distribuída - Energia Fotovoltaica no Brasil', 'Público alvo: Engenharia Elétrica', '2018-06-28 10:30:00', '2018-06-28 12:00:00', '02:00:00', 5, NULL, 16, 'Prof. Guilherme Silva / Eng. Breno Barrera'),
(44, 'Obra do Consorcio de Revitalização do Arroio do Regalado', 'Público alvo: Engenharia Civil', '2018-06-28 10:30:00', '2018-06-28 12:00:00', '02:00:00', 4, NULL, 1, 'Carlos Alexandre / Alisson Cooper / João Maria'),
(45, '	Redes Elétricas Inteligentes', 'Público alvo: Engenharia Elétrica', '2018-06-28 14:00:00', '2018-06-28 15:30:00', '02:00:00', 4, NULL, 15, 'Prof. Dr. Eng. Mauricio Sperandio'),
(46, 'Capitalismo Consciente', 'Público alvo: Livre', '2018-06-28 14:00:00', '2018-06-28 15:30:00', '02:00:00', 4, NULL, 1, 'Thomas Eckschmidt'),
(47, 'Métodos avançados para diagnóstico em energia', 'Público alvo: Engenharia Mecânica', '2018-06-28 14:00:00', '2018-06-28 15:30:00', '02:00:00', 4, NULL, 11, 'Nattan Caetano'),
(48, 'desenho mecânico (solid works) para iniciantes', 'Público alvo: Engenharia Mecânica (CALOUROS)', '2018-06-28 14:00:00', '2018-06-28 18:00:00', '04:00:00', 3, 16, 4, 'Nathalia Silva'),
(49, 'Aprovação de Projetos na Prefeitura', 'Público alvo: Engenharia Civil', '2018-06-28 14:00:00', '2018-06-28 15:30:00', '02:00:00', 4, NULL, 6, 'Danieli Pozzebon'),
(50, 'IEEE UFSM - Ramo Estudantil', 'Público alvo: Engenharia Elétrica', '2018-06-28 16:00:00', '2018-06-28 18:00:00', '02:00:00', 4, NULL, 6, 'Alunos UFSM'),
(51, 'Minicurso Latex Intermediário: Apresentações, gráficos e imagens (TIKZ) - 30 vagas pré-requisito Lat', 'Público alvo: Livre', '2018-06-28 16:00:00', '2018-06-28 18:00:00', '02:00:00', 3, NULL, 2, 'Victor Israel, Diogo Deus e Cristian Muller'),
(52, 'Cultura norte americana', 'Público alvo: Livre', '2018-06-28 16:00:00', '2018-06-28 18:00:00', '02:00:00', 4, NULL, 11, '5 estudantes em intercâmbio'),
(53, 'Empreendedorismo', 'Público alvo: Livre', '2018-06-28 16:00:00', '2018-06-28 18:00:00', '02:00:00', 3, NULL, 1, 'Onélio Pilleco'),
(54, 'Minicurso Ferramenta Netlogo para simulação de Agentes', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-28 19:00:00', '2018-06-28 22:30:00', '04:00:00', 3, 18, 4, 'Celso Nobre'),
(55, 'Oficina Currículo Lattes e Classificação de Periodicos', 'Público alvo: Engenharia Civil', '2018-06-28 19:00:00', '2018-06-28 20:30:00', '02:00:00', 3, NULL, 2, 'Marco Tier'),
(56, 'Investimento Anjo', 'Público alvo: Livre', '2018-06-28 21:00:00', '2018-06-28 22:30:00', '02:00:00', 4, NULL, 1, 'Advogado Luiz Garrido Giulliano Tozzi'),
(57, 'OpenDSS - Fluxo de potência em redes de distribuição – Modelos Avançados para', 'Público alvo: Engenharia Elétrica', '2018-06-29 08:30:00', '2018-06-29 12:00:00', '04:00:00', 3, 19, 2, 'Henrique Eichkoff, João Machiavelli, Ana Paula Mello'),
(58, 'Palestra com aLgcom', 'Público alvo: Engenharia Elétrica - Engenharia de Telecomunicações', '2018-06-29 14:00:00', '2018-06-29 15:30:00', '02:00:00', 4, NULL, 1, 'Willian'),
(59, 'Pesquisas desenvolvidas na Eng MEC', 'Público alvo: Engenharia Mecânica', '2018-06-29 14:00:00', '2018-06-29 15:30:00', '02:00:00', 4, NULL, 6, 'Fernando Hendges, Isadora Goss e Yuri Salvador'),
(60, 'OpenDSS - Fluxo de potência em redes de distribuição – Modelos Avançados para', 'Público alvo: Engenharia Elétrica', '2018-06-29 14:00:00', '2018-06-29 18:00:00', '04:00:00', 3, 21, 2, '	Henrique Eichkoff, João Machiavelli, Ana Paula Mello'),
(61, 'Minicurso Simulink', 'Público alvo: Engenharia Elétrica', '2018-06-29 14:00:00', '2018-06-29 18:00:00', '04:00:00', 3, 21, 4, 'Prof. Guilherme Silva'),
(62, 'Implantação Rodoviária de travessia Urbana BR-487', 'Público alvo: Engenharia Civil', '2018-06-29 14:00:00', '2018-06-29 15:30:00', '02:00:00', 4, NULL, 15, 'Maurício Savoldi'),
(63, 'Nova Ponte do Guaíba', 'Público alvo: Engenharia Civil', '2018-06-29 16:00:00', '2018-06-29 18:00:00', '02:00:00', 4, NULL, 1, 'Hiratan Pinheiro da Silva'),
(64, 'Equações diferenciais, Geometria Analítica e Modelo computacional para viagens espaciais', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-29 16:00:00', '2018-06-29 18:00:00', '02:00:00', 4, NULL, 6, 'Celso Nobre'),
(65, 'Relato de experiência de estágio', 'Público alvo: Engenharia Mecânica', '2018-06-29 16:00:00', '2018-06-29 18:00:00', '02:00:00', 4, NULL, 11, 'Hortência Noronha, Anderson Silva e Renato Panziera'),
(66, 'Testes automatizados utilizando Cucumber e Ruby (30 vagas)', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-29 19:00:00', '2018-06-29 22:30:00', '04:00:00', 3, 23, 4, 'Yury Alencar'),
(67, 'Relato de egresso da Unipampa', 'Público alvo: Ciência da Computação - Engenharia de Software', '2018-06-29 19:00:00', '2018-06-29 20:30:00', '02:00:00', 4, NULL, 11, 'Prof. Marcelo Caggiani'),
(68, 'Nanotecnologias em Materiais', 'Público alvo: Engenharia Civil', '2018-06-29 19:00:00', '2018-06-29 20:30:00', '02:00:00', 4, NULL, 1, 'Aldo Temp');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `idcategoria` int(11) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `valor` float(4,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`idcategoria`, `categoria`, `valor`) VALUES
(1, 'Ouvinte', 15.00);

-- --------------------------------------------------------

--
-- Estrutura da tabela `credenciamentoatividades`
--

CREATE TABLE `credenciamentoatividades` (
  `idcredenciamentoAtividade` int(11) NOT NULL,
  `credenciador_idpessoa` int(11) NOT NULL,
  `participante_idpessoa` int(11) NOT NULL,
  `idatividade` int(11) NOT NULL,
  `horario` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `credenciamentoevento`
--

CREATE TABLE `credenciamentoevento` (
  `idcredenciamentoEvento` int(11) NOT NULL,
  `credenciador_idpessoa` int(11) NOT NULL,
  `participante_idpessoa` int(11) NOT NULL,
  `horario` datetime NOT NULL,
  `materialEntregue` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `estados`
--

CREATE TABLE `estados` (
  `idEstado` int(11) NOT NULL,
  `codigoUf` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `Uf` char(2) NOT NULL,
  `regiao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `estados`
--

INSERT INTO `estados` (`idEstado`, `codigoUf`, `nome`, `Uf`, `regiao`) VALUES
(1, 12, 'Acre', 'AC', 1),
(2, 27, 'Alagoas', 'AL', 2),
(3, 16, 'Amapá', 'AP', 1),
(4, 13, 'Amazonas', 'AM', 1),
(5, 29, 'Bahia', 'BA', 2),
(6, 23, 'Ceará', 'CE', 2),
(7, 53, 'Distrito Federal', 'DF', 5),
(8, 32, 'Espírito Santo', 'ES', 3),
(9, 52, 'Goiás', 'GO', 5),
(10, 21, 'Maranhão', 'MA', 2),
(11, 51, 'Mato Grosso', 'MT', 5),
(12, 50, 'Mato Grosso do Sul', 'MS', 5),
(13, 31, 'Minas Gerais', 'MG', 3),
(14, 15, 'Pará', 'PA', 1),
(15, 25, 'Paraíba', 'PB', 2),
(16, 41, 'Paraná', 'PR', 4),
(17, 26, 'Pernambuco', 'PE', 2),
(18, 22, 'Piauí', 'PI', 2),
(19, 33, 'Rio de Janeiro', 'RJ', 3),
(20, 24, 'Rio Grande do Norte', 'RN', 2),
(21, 43, 'Rio Grande do Sul', 'RS', 4),
(22, 11, 'Rondônia', 'RO', 1),
(23, 14, 'Roraima', 'RR', 1),
(24, 42, 'Santa Catarina', 'SC', 4),
(25, 35, 'São Paulo', 'SP', 3),
(26, 28, 'Sergipe', 'SE', 2),
(27, 17, 'Tocantins', 'TO', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `evento`
--

CREATE TABLE `evento` (
  `idEvento` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `dataInicio` datetime NOT NULL,
  `dataFim` datetime NOT NULL,
  `local` varchar(100) NOT NULL,
  `idEstado` int(11) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `responsavel` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefone` bigint(11) NOT NULL,
  `descricao` text NOT NULL,
  `logo` blob NOT NULL,
  `cargaHoraria` int(11) NOT NULL,
  `idiomaEvento` varchar(100) NOT NULL,
  `sigla` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `evento`
--

INSERT INTO `evento` (`idEvento`, `titulo`, `dataInicio`, `dataFim`, `local`, `idEstado`, `cidade`, `responsavel`, `email`, `telefone`, `descricao`, `logo`, `cargaHoraria`, `idiomaEvento`, `sigla`) VALUES
(1, '10ª Semana Acadêmica do Campus Tecnológico de Alegrete', '2018-06-25 08:30:00', '2018-06-29 22:30:00', 'Unipampa Alegrete', 21, 'Alegrete', 'CEC', 'a@a.com', 0, 'descricaododb', 0x3f, 60, 'Brasil', 'SACTA X');

-- --------------------------------------------------------

--
-- Estrutura da tabela `intervalosatividades`
--

CREATE TABLE `intervalosatividades` (
  `idintervaloAtividade` int(11) NOT NULL,
  `idtipoIntervalo` int(11) NOT NULL,
  `inicio` datetime NOT NULL,
  `fim` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `intervalosatividades`
--

INSERT INTO `intervalosatividades` (`idintervaloAtividade`, `idtipoIntervalo`, `inicio`, `fim`) VALUES
(1, 1, '2018-06-25 15:30:00', '2018-06-25 16:00:00'),
(2, 3, '2018-06-25 18:00:00', '2018-06-25 19:00:00'),
(3, 1, '2018-06-25 20:30:00', '2018-06-25 21:00:00'),
(4, 1, '2018-06-26 10:00:00', '2018-06-25 10:30:00'),
(5, 2, '2018-06-26 12:00:00', '2018-06-26 14:00:00'),
(6, 1, '2018-06-26 15:30:00', '2018-06-26 16:00:00'),
(7, 3, '2018-06-26 18:00:00', '2018-06-26 19:00:00'),
(8, 1, '2018-06-26 20:00:00', '2018-06-26 21:00:00'),
(9, 1, '2018-06-27 10:00:00', '2018-06-27 10:30:00'),
(10, 2, '2018-06-27 12:00:00', '2018-06-27 14:00:00'),
(11, 1, '2018-06-27 15:30:00', '2018-06-27 16:00:00'),
(12, 3, '2018-06-27 18:00:00', '2018-06-27 19:00:00'),
(13, 1, '2018-06-27 20:30:00', '2018-06-27 21:00:00'),
(14, 1, '2018-06-28 10:00:00', '2018-06-28 10:30:00'),
(15, 2, '2018-06-28 12:00:00', '2018-06-28 14:00:00'),
(16, 1, '2018-06-28 15:30:00', '2018-06-28 16:00:00'),
(17, 3, '2018-06-28 18:00:00', '2018-06-28 19:00:00'),
(18, 1, '2018-06-28 20:30:00', '2018-06-28 21:00:00'),
(19, 1, '2018-06-29 10:00:00', '2018-06-29 10:30:00'),
(20, 2, '2018-06-29 12:00:00', '2018-06-29 14:00:00'),
(21, 1, '2018-06-29 15:30:00', '2018-06-29 16:00:00'),
(22, 3, '2018-06-29 18:00:00', '2018-06-29 19:00:00'),
(23, 1, '2018-06-29 20:30:00', '2018-06-29 21:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `localatividades`
--

CREATE TABLE `localatividades` (
  `idlocalAtividade` int(11) NOT NULL,
  `local` varchar(45) NOT NULL,
  `limiteVagas` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `localatividades`
--

INSERT INTO `localatividades` (`idlocalAtividade`, `local`, `limiteVagas`) VALUES
(1, 'Auditório Márcia Cristina Cera (Sala 101)', 150),
(2, 'Laboratório de Informática 01', 30),
(3, 'Laboratório de Informática 02', 30),
(4, 'Laboratório de Informática 05', 30),
(5, 'Sala 103', 70),
(6, 'Sala 203', 70),
(7, 'Sala 104 (CEC)', 70),
(8, 'Sala 104 B (Estudos)', 70),
(9, 'LAMAP - Mecanização Agrícola do Pampa', 70),
(10, 'Laborátorio Mecânica', 70),
(11, 'Sala 209', 70),
(12, 'Hall de Entrada', 70),
(13, 'Sala 312', 70),
(14, 'Sala 201', 70),
(15, 'Sala 301', 70),
(16, 'Laboratório de Informática 03', 30);

-- --------------------------------------------------------

--
-- Estrutura da tabela `participantes_has_atividades`
--

CREATE TABLE `participantes_has_atividades` (
  `idpessoa` int(11) NOT NULL,
  `idatividade` int(11) NOT NULL,
  `inscricao` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoas`
--

CREATE TABLE `pessoas` (
  `idpessoa` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pessoas`
--

INSERT INTO `pessoas` (`idpessoa`, `nome`, `email`) VALUES
(1, 'Gustavo Bittencourt Satheler', 'gustavosatheler@gmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `privilegios`
--

CREATE TABLE `privilegios` (
  `idprivilegio` int(11) NOT NULL,
  `privilegio` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `privilegios`
--

INSERT INTO `privilegios` (`idprivilegio`, `privilegio`) VALUES
(1, 'Participante'),
(2, 'Credenciador'),
(3, 'Organizador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `situacoes`
--

CREATE TABLE `situacoes` (
  `idsituacao` int(11) NOT NULL,
  `situacao` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `situacoes`
--

INSERT INTO `situacoes` (`idsituacao`, `situacao`) VALUES
(1, 'Pre-Inscrito'),
(2, 'Confirmado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo`
--

CREATE TABLE `tipo` (
  `idtipo` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipointervalo`
--

CREATE TABLE `tipointervalo` (
  `idtipoIntervalo` int(11) NOT NULL,
  `intervalo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipointervalo`
--

INSERT INTO `tipointervalo` (`idtipoIntervalo`, `intervalo`) VALUES
(1, 'Coffee Break'),
(2, 'Almoço'),
(3, 'Intervalo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tiposatividade`
--

CREATE TABLE `tiposatividade` (
  `idtipoAtividade` int(11) NOT NULL,
  `tipoAtividade` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tiposatividade`
--

INSERT INTO `tiposatividade` (`idtipoAtividade`, `tipoAtividade`) VALUES
(1, 'Competição'),
(2, 'Conferência'),
(3, 'Minicurso'),
(4, 'Palestra'),
(5, 'Mesa Redonda'),
(6, 'Outros');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `idpessoa` int(11) NOT NULL,
  `senha` varchar(45) NOT NULL,
  `telefone` bigint(11) NOT NULL,
  `idprivilegio` int(11) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  `idsituacao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`idpessoa`, `senha`, `telefone`, `idprivilegio`, `idcategoria`, `idsituacao`) VALUES
(1, '7c4a8d09ca3762af61e59520943dc26494f8941b', 55999367788, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atividades`
--
ALTER TABLE `atividades`
  ADD PRIMARY KEY (`idatividade`,`idtipoAtividade`,`idlocalAtividade`),
  ADD KEY `fk_atividades_tiposAtividade1_idx` (`idtipoAtividade`),
  ADD KEY `fk_atividades_intervalosAtividades1_idx` (`idintervaloAtividade`),
  ADD KEY `fk_atividades_localAtividades1_idx` (`idlocalAtividade`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indexes for table `credenciamentoatividades`
--
ALTER TABLE `credenciamentoatividades`
  ADD PRIMARY KEY (`idcredenciamentoAtividade`,`credenciador_idpessoa`,`participante_idpessoa`,`idatividade`),
  ADD KEY `fk_credenciamentoAtividades_atividades1_idx` (`idatividade`),
  ADD KEY `fk_credenciamentoAtividades_usuarios1_idx` (`credenciador_idpessoa`),
  ADD KEY `fk_credenciamentoAtividades_usuarios2_idx` (`participante_idpessoa`);

--
-- Indexes for table `credenciamentoevento`
--
ALTER TABLE `credenciamentoevento`
  ADD PRIMARY KEY (`idcredenciamentoEvento`,`credenciador_idpessoa`,`participante_idpessoa`),
  ADD KEY `fk_credenciamentoEvento_usuarios1_idx` (`credenciador_idpessoa`),
  ADD KEY `fk_credenciamentoEvento_usuarios2_idx` (`participante_idpessoa`);

--
-- Indexes for table `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`idEstado`);

--
-- Indexes for table `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`idEvento`,`idEstado`),
  ADD KEY `fk_Evento_Estado_idx` (`idEstado`);

--
-- Indexes for table `intervalosatividades`
--
ALTER TABLE `intervalosatividades`
  ADD PRIMARY KEY (`idintervaloAtividade`,`idtipoIntervalo`),
  ADD KEY `fk_intervalosAtividades_tipoIntervalo1_idx` (`idtipoIntervalo`);

--
-- Indexes for table `localatividades`
--
ALTER TABLE `localatividades`
  ADD PRIMARY KEY (`idlocalAtividade`);

--
-- Indexes for table `participantes_has_atividades`
--
ALTER TABLE `participantes_has_atividades`
  ADD PRIMARY KEY (`idpessoa`,`idatividade`),
  ADD KEY `fk_participantes_has_atividades_atividades1_idx` (`idatividade`),
  ADD KEY `fk_participantes_has_atividades_usuarios1_idx` (`idpessoa`);

--
-- Indexes for table `pessoas`
--
ALTER TABLE `pessoas`
  ADD PRIMARY KEY (`idpessoa`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- Indexes for table `privilegios`
--
ALTER TABLE `privilegios`
  ADD PRIMARY KEY (`idprivilegio`);

--
-- Indexes for table `situacoes`
--
ALTER TABLE `situacoes`
  ADD PRIMARY KEY (`idsituacao`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`idtipo`);

--
-- Indexes for table `tipointervalo`
--
ALTER TABLE `tipointervalo`
  ADD PRIMARY KEY (`idtipoIntervalo`);

--
-- Indexes for table `tiposatividade`
--
ALTER TABLE `tiposatividade`
  ADD PRIMARY KEY (`idtipoAtividade`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idpessoa`,`idprivilegio`,`idcategoria`,`idsituacao`),
  ADD KEY `fk_usuarios_privilegio1_idx` (`idprivilegio`),
  ADD KEY `fk_usuarios_pessoas1_idx` (`idpessoa`),
  ADD KEY `fk_usuarios_categoria1_idx` (`idcategoria`),
  ADD KEY `fk_usuarios_situacoes1_idx` (`idsituacao`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atividades`
--
ALTER TABLE `atividades`
  MODIFY `idatividade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `credenciamentoatividades`
--
ALTER TABLE `credenciamentoatividades`
  MODIFY `idcredenciamentoAtividade` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `credenciamentoevento`
--
ALTER TABLE `credenciamentoevento`
  MODIFY `idcredenciamentoEvento` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `estados`
--
ALTER TABLE `estados`
  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `evento`
--
ALTER TABLE `evento`
  MODIFY `idEvento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `intervalosatividades`
--
ALTER TABLE `intervalosatividades`
  MODIFY `idintervaloAtividade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `localatividades`
--
ALTER TABLE `localatividades`
  MODIFY `idlocalAtividade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pessoas`
--
ALTER TABLE `pessoas`
  MODIFY `idpessoa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `privilegios`
--
ALTER TABLE `privilegios`
  MODIFY `idprivilegio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `situacoes`
--
ALTER TABLE `situacoes`
  MODIFY `idsituacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `idtipo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipointervalo`
--
ALTER TABLE `tipointervalo`
  MODIFY `idtipoIntervalo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tiposatividade`
--
ALTER TABLE `tiposatividade`
  MODIFY `idtipoAtividade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `atividades`
--
ALTER TABLE `atividades`
  ADD CONSTRAINT `fk_atividades_intervalosAtividades1` FOREIGN KEY (`idintervaloAtividade`) REFERENCES `intervalosatividades` (`idintervaloAtividade`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_atividades_localAtividades1` FOREIGN KEY (`idlocalAtividade`) REFERENCES `localatividades` (`idlocalAtividade`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_atividades_tiposAtividade1` FOREIGN KEY (`idtipoAtividade`) REFERENCES `tiposatividade` (`idtipoAtividade`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `credenciamentoatividades`
--
ALTER TABLE `credenciamentoatividades`
  ADD CONSTRAINT `fk_credenciamentoAtividades_atividades1` FOREIGN KEY (`idatividade`) REFERENCES `atividades` (`idatividade`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_credenciamentoAtividades_usuarios1` FOREIGN KEY (`credenciador_idpessoa`) REFERENCES `usuarios` (`idpessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_credenciamentoAtividades_usuarios2` FOREIGN KEY (`participante_idpessoa`) REFERENCES `usuarios` (`idpessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `credenciamentoevento`
--
ALTER TABLE `credenciamentoevento`
  ADD CONSTRAINT `fk_credenciamentoEvento_usuarios1` FOREIGN KEY (`credenciador_idpessoa`) REFERENCES `usuarios` (`idpessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_credenciamentoEvento_usuarios2` FOREIGN KEY (`participante_idpessoa`) REFERENCES `usuarios` (`idpessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `evento`
--
ALTER TABLE `evento`
  ADD CONSTRAINT `fk_Evento_Estado` FOREIGN KEY (`idEstado`) REFERENCES `estados` (`idEstado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `intervalosatividades`
--
ALTER TABLE `intervalosatividades`
  ADD CONSTRAINT `fk_intervalosAtividades_tipoIntervalo1` FOREIGN KEY (`idtipoIntervalo`) REFERENCES `tipointervalo` (`idtipoIntervalo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `participantes_has_atividades`
--
ALTER TABLE `participantes_has_atividades`
  ADD CONSTRAINT `fk_participantes_has_atividades_atividades1` FOREIGN KEY (`idatividade`) REFERENCES `atividades` (`idatividade`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_participantes_has_atividades_usuarios1` FOREIGN KEY (`idpessoa`) REFERENCES `usuarios` (`idpessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_categoria1` FOREIGN KEY (`idcategoria`) REFERENCES `categorias` (`idcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_pessoas1` FOREIGN KEY (`idpessoa`) REFERENCES `pessoas` (`idpessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_privilegio1` FOREIGN KEY (`idprivilegio`) REFERENCES `privilegios` (`idprivilegio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_situacoes1` FOREIGN KEY (`idsituacao`) REFERENCES `situacoes` (`idsituacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
