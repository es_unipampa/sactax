SELECT 	atividades.idAtividade,
    atividades.nome,
    atividades.descricao,
    atividades.inicio,
    atividades.fim,
    tiposatividade.tipoatividade,
    localatividades.local,
    localatividades.limiteVagas,
    atividades.ministrante, 
    (usuarios.idpessoa = 2) as estaInscrito

FROM atividades, tiposatividade, localatividades, usuarios, participantes_has_atividades

WHERE usuarios.idpessoa = 2
	AND usuarios.idpessoa = participantes_has_atividades.idpessoa
	AND participantes_has_atividades.idatividade = atividades.idatividade
    
    AND atividades.idtipoAtividade = tiposatividade.idtipoAtividade 
	AND atividades.idlocalAtividade = localatividades.idlocalAtividade 

ORDER BY atividades.inicio--;