SELECT atividades.idAtividade,
		atividades.nome, 
		atividades.descricao, 
		atividades.inicio, 
		atividades.fim, 
		tiposatividade.tipoatividade, 
		localatividades.local, 
		localatividades.limiteVagas, 
		atividades.ministrante,
		(usuarios.idpessoa = 1) as estaInscrito

FROM usuarios
LEFT JOIN participantes_has_atividades ON usuarios.idpessoa = 1 AND participantes_has_atividades.idpessoa = usuarios.idpessoa
RIGHT JOIN atividades ON participantes_has_atividades.idAtividade = atividades.idatividade

LEFT JOIN tiposatividade ON atividades.idtipoatividade = tiposatividade.idtipoatividade
LEFT JOIN localatividades ON atividades.idlocalAtividade = localatividades.idlocalAtividade

ORDER BY atividades.idAtividade--;