SELECT atividades.idAtividade,
		atividades.nome, 
        atividades.descricao, 
        atividades.inicio, 
        atividades.fim, 
        tiposatividade.tipoatividade, 
        localatividades.local, 
        localatividades.limiteVagas, 
        atividades.ministrante,
        (usuarios.idpessoa = 1) as estaInscrito

FROM atividades
LEFT JOIN participantes_has_atividades ON participantes_has_atividades.idAtividade = atividades.idatividade
LEFT JOIN usuarios ON usuarios.idpessoa = 1 AND usuarios.idpessoa = participantes_has_atividades.idpessoa
LEFT JOIN tiposatividade ON atividades.idtipoatividade = tiposatividade.idtipoatividade
LEFT JOIN localatividades ON atividades.idlocalAtividade = localatividades.idlocalAtividade

ORDER BY atividades.inicio--;