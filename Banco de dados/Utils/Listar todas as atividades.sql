SELECT atividades.idAtividade, atividades.nome, atividades.descricao, atividades.inicio, atividades.fim, tiposatividade.tipoatividade, localatividades.local, localatividades.limiteVagas, atividades.ministrante 

FROM atividades, tiposatividade, localatividades 

WHERE atividades.idtipoAtividade = tiposatividade.idtipoAtividade 
	AND atividades.idlocalAtividade = localatividades.idlocalAtividade 
    
ORDER BY atividades.inicio--;