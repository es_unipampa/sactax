SELECT 	
	usuarios.idpessoa,
	atividades.idAtividade,
    atividades.nome,
    atividades.descricao,
    atividades.inicio,
    atividades.fim,
    tiposatividade.tipoatividade,
    localatividades.local,
    localatividades.limiteVagas,
    atividades.ministrante 

FROM atividades, tiposatividade, localatividades, usuarios, participantes_has_atividades

WHERE atividades.idtipoAtividade = tiposatividade.idtipoAtividade 
	AND atividades.idlocalAtividade = localatividades.idlocalAtividade 
	AND usuarios.idpessoa = participantes_has_atividades.idpessoa
	AND participantes_has_atividades.idatividade = atividades.idatividade

ORDER BY atividades.inicio--;