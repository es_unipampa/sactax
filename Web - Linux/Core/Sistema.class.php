<?php

namespace Core;

use Models\Evento;
use \InvalidArgumentException;
use \PDOException;

defined("ACESSO_DIRETO") or die("<h1>Access denied</h1>");

class Sistema {

    private $controlador;

    public function __construct(string $controlador = "home") {
        $this->controlador = $controlador;
    }

    private function inicializar() {
        $urlData = $this->getUrlData();

        $con = $urlData["controlador"];
        $acao = $urlData["acao"];
        $parametros = $urlData["parametros"];

        $controlador = $this->makeController($con);

        if (!file_exists($controlador["caminho"])) {
            $controlador = $this->makeController($this->controlador);

            require_once($controlador["caminho"]);

            $controlador = new $controlador["classe"]();
            $controlador->setParametros($acao);

            if (method_exists($controlador, $con)) {
                return $controlador->{$con}();
            }

            return header("Location: /");
        }

        require_once($controlador["caminho"]);

        if (!class_exists($controlador["classe"])) {
            return header("Location: /");
        }
        $controlador = new $controlador["classe"]();
        $controlador->setParametros($parametros);

        $acao = preg_replace('/[^a-zA-Z]/i', '', $acao);

        try {
            if (method_exists($controlador, $acao)) {
                return $controlador->{$acao}();
            } else {
                return $controlador->index();
            }
        } catch (InvalidArgumentException $ex) {
            return header(sprintf("HTTP/1.1 %d %s", $ex->getCode(), utf8_decode($ex->getMessage())));
        } catch (PDOException $ex) {
            switch ($ex->errorInfo[1]) {
                default:
                    $mensagem = $ex->getMessage();
                    break;
                case 1062:
                    $mensagem = "Já existe um participante cadastrado com o e-mail informado.";
                    break;
            }

            return header(sprintf("HTTP/1.1 400 %s", utf8_decode((new \Views\Estruturas\JSON())->gerar($mensagem))));
        }

        // GERAR UMA PÁGINA DE ERRO
        header("Location: /");
    }

    private function classeController(string $dataController) {
        $controlador = preg_replace("/[^a-zA-Z]/i", '', $dataController);
        return($controlador . "Controller");
    }

    private function caminhoController(string $controller) {
        return PATH_CONTROLLERS . DIRECTORY_SEPARATOR . $controller . '.class.php';
    }

    private function debugSistema(string $caminhoControlador, string $area) {
        debug("Dados Sistema", $area);
        debug("Controller", ($this->controlador instanceof Controller) ? __NAMESPACE__ . "/" . get_class($this->controlador) : __NAMESPACE__ . "/" . $this->controlador);
        debug("Ação", $this->acao);
        debug("Parâmetros", $this->parametros);
        debug("Caminho controlador", preg_replace("/\\\\/", "/", $caminhoControlador));
    }

    public function getUrlData() {
        if (isset($_GET['caminho'])) {
            $caminho = $_GET['caminho'];
            $caminho = rtrim($caminho, '/');
            $caminho = filter_var($caminho, FILTER_SANITIZE_URL);
            $caminho = explode('/', $caminho);

            $controlador = $caminho[0];
            $acao = !isset($caminho[1]) ? "" : $caminho[1];

            unset($caminho[0]);
            unset($caminho[1]);

            $parametros = array_values($caminho);
        }

        $urlData = array();
        $urlData["controlador"] = isset($controlador) ? $controlador : $this->controlador;
        $urlData["acao"] = isset($acao) ? $acao : "";
        $urlData["parametros"] = isset($parametros) ? $parametros : array();

        return $urlData;
    }

    public function makeController(string $nomeControlador) {
        $nomeNormalizado = $this->classeController($nomeControlador);
        return array(
            "classe" => "Controllers\\" . $nomeNormalizado,
            "caminho" => $this->caminhoController($nomeNormalizado)
        );
    }

    public function exibir() {
        die($this->inicializar());
    }

}
