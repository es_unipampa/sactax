<?php

function debug($titulo, $objeto) {
    if ((defined("DESENVOLVIMENTO") && DESENVOLVIMENTO)) {
        $saida = $objeto;

        if (is_array($saida)) {
            $array = "[";

            for ($i = 0; $i < count($saida); $i++) {
                $array .= $saida[$i];

                if (count($saida) != ($i + 1)) {
                    $array .= ", ";
                }
            }

            $saida = $array . ']';
        }

        echo "<script>console.log('PHP Debug -> " . $titulo . ": " . $saida . "');</script>";
    }
}

function diaSemana(DateTime $data){
    switch ($data->format("l")){
        case "Monday":
            return "Segunda-feira";
        case "Tuesday":
            return "Terça-feira";
        case "Wednesday":
            return "Quarta-feira";
        case "Thursday":
            return "Quinta-feira";
        case "Friday":
            return "Sexta-feira";
        case "Saturday":
            return "Sábado";
        default:
            return "Domingo";
    }
}

function nomeMes(DateTime $data){
    switch ($data->format("M")){
        case "Jan":
            return "Janeiro";
        case "Feb":
            return "Fevereiro";
        case "Mar":
            return "Março";
        case "Apr":
            return "Abril";
        case "May":
            return "Maio";
        case "Jun":
            return "Junho";
        case "Jul":
            return "Julho";
        case "Aug":
            return "Agosto";
        case "Sep":
            return "Setembro";
        case "Oct":
            return "Outubro";
        case "Nov":
            return "Novembro";
        default:
            return "Dezembro";
    }
}

function forcarSSL(){
    if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on"){
        header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    }
}