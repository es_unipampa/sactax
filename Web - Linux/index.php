<?php

use Core\Sistema;

define("SYSTEM_MINIMUM_PHP", "7.1");

if (version_compare(PHP_VERSION, SYSTEM_MINIMUM_PHP, '<')) {
    die("Configure o PHP para a versão " . SYSTEM_MINIMUM_PHP . " ou mais recente para este sistema funcionar corretamente.");
}

define("ACESSO_DIRETO", true);

if (!defined("DEFINES")) {
    define("PATH_BASE", __DIR__);
    require_once PATH_BASE . "/funcoes.php";
    require_once PATH_BASE . "/autoload.php";
    require_once PATH_BASE . "/defines.php";
    require_once PATH_BASE . "/email.php";
}

DEFINE("SUBDOMINIO", current(explode('.', $_SERVER["HTTP_HOST"])));

forcarSSL();

switch (SUBDOMINIO) {
    case "api":
        $padrao = "API";
        break;
    default:
        $padrao = "Home";
}

$app = new Sistema($padrao);
$app->exibir();

// FAZER A CHEGAGEM DE ENVIO DE HEADER
// die(headers_sent() ? "Enviado" : "Não enviado");