<?php

function confirmacaoInscricao($nome, $email) {
    $to = $email;
    $subject = 'Confirmação da pré-inscrição para 10ª Semana Acadêmica do Campus Tecnológico de Alegrete';

    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'From: SACTA X <nao.responda@cec-alegrete.com.br>' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'X-Mailer: PHP/' . phpversion();

    $message = '<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<p style="margin:0cm;margin-bottom:.0001pt;background:white"><span
style="font-size:13.5pt;font-family:"Arial",sans-serif;color:#222222">Bem-vindo,
<b>' . $nome . '</b>!</span></p>

<p style="margin:0cm;margin-bottom:.0001pt;background:white"><span
style="font-family:"Arial",sans-serif;color:#222222">Esta é confirmação da pré-inscrição
na Semana Acadêmica 2018!</span></p>

<p style="margin:0cm;margin-bottom:.0001pt;background:white"><span
style="font-family:"Arial",sans-serif;color:#222222">A pré-inscrição não
garante participação no evento.</span></p>

<p style="margin:0cm;margin-bottom:.0001pt;background:white"><span
style="font-family:"Arial",sans-serif;color:#222222">A &nbsp;sua inscrição será
efetivada apenas após confirmação do pagamento realizado no credenciamento.</span></p>

<p style="margin:0cm;margin-bottom:.0001pt;background:white">&nbsp;</p>

<p style="margin:0cm;margin-bottom:.0001pt;background:white"><b><span
style="font-family:"Arial",sans-serif;color:#222222">Orientações</span></b></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">Para o CREDENCIAMENTO
levar documento de identidade com foto (CNH, RG, passaporte, CT), e pagar uma
taxa de R$ 15,00 que dará acesso às palestras, minicursos e visitas técnicas
que o participante deverá se cadastrar previamente e que fica condicionada ao
número de vagas disponíveis;<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">Após o seu credenciamento no
evento, você receberá um email com o QR-CODE que o identificará em todas as
atividades da SACTA 2018 e deverá ser utilizado durante todo o evento para
registro de sua participação em cada atividade;<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">Em cada sala terá controle
de entrada/saída pelo pessoal da organização, que é feito pela leitura do
QR-CODE recebido em seu email, para agilizar o processo, o participante deve
deixar o QR-CODE já na tela do seu celular (ou impresso) para acesso às
atividades;<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">Toda a entrada e toda a
saída de cada participante deverá ser registrada.<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">A inscrição de um
participante em duas palestras simultâneas resultará em anulação da
participação em ambas;<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">Para se inscrever nas
PALESTRA, VISITAS TÉCNICAS, COMPETIÇÕES e MINI-CURSOS são por meio de inscrição
pelo site;<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">No momento de inscrição
nas atividades é informado o número total de vagas e quantas ainda estão
disponíveis; <o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">O número de vagas
quaisquer atividades da SACTA 2018 depende do espaço físico do local. A
prioridade é pela ordem de inscrição, sendo gerado no próprio sistema uma lista
de espera;<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">As listas de presença das
palestras serão entregue aos professores para que cada um decida se vai dar
falta para quem não for na semana acadêmica;<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">A tolerância máxima para
entrada em cada atividade será de 10 minutos, após isso, se sua entrada não for
registrada, o sistema permitirá que os usuários na lista de espera entrem, até
que a sala atinja seu limite, por isso recomenda-se os que estão na lista,
aguardem próximo ao local da atividade<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">Será proibida a entrada
nas atividades após 15 minutos, após o inicio.<o:p></o:p></span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p style="margin:0cm;margin-bottom:.0001pt"><b><span style="font-family:"Arial",sans-serif;
color:#222222">Quanto a certificação</span></b></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l1 level1 lfo2;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">O participante terá
direito ao certificado caso comparecer em uma ou mais palestras válidas.<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l1 level1 lfo2;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">Cada palestra será
considerada válida, ou seja, entrará na contagem de horas válidas para a
certificação, caso o participante estiver presente em mais de 70% da palestra.<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l1 level1 lfo2;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">As palestras podem ter uma
duração variada, então a contagem de tempo de cada palestra será feita
individualmente. Se uma palestra de duas horas apenas dure uma hora e meia, o
participante deverá estar presente em 70% de uma hora e meia. Porém essas duas
horas que foram registradas para a palestra que irão aparecer no certificado.<o:p></o:p></span></p>

<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:47.0pt;
margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l1 level1 lfo2;tab-stops:
list 36.0pt;vertical-align:baseline"><![if !supportLists]><span
style="font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#222222"><span
style="mso-list:Ignore">·<span style="font:7.0pt "Times New Roman"">&nbsp; </span></span></span><![endif]><span
style="font-family:"Arial",sans-serif;color:#222222">A PALESTRA DE ENCERRAMENTO
vale 4h de atividade e serão servidos frios e salgadinhos nesta atividade. Quem
participar desta atividade recebe automaticamente quatro horas, que equivale ao
tempo de duas atividades.<o:p></o:p></span></p>

<p style="margin:0cm;margin-bottom:.0001pt;background:white">&nbsp;</p>

<p style="margin:0cm;margin-bottom:.0001pt;background:white"><span
style="font-family:"Arial",sans-serif;color:#222222">Atividades fora da
programação serão notificadas nos telões ou emails cadastrados no sistema.</span></p>

<p style="margin:0cm;margin-bottom:.0001pt;background:white">&nbsp;</p>

<p style="margin:0cm;margin-bottom:.0001pt;background:white"><o:p>&nbsp;</o:p></p>

<p style="margin:0cm;margin-bottom:.0001pt"><b><span style="font-family:"Arial",sans-serif;
color:#222222;background:white">Semana Acadêmica do Campus Tecnológico de
Alegrete de 2018</span></b></p>

</div>

</div>

</body>

</html>';

    $enviado = mail($to, $subject, $message, $headers);
    
    return $enviado;
}

function pagamentoConfirmado($nome, $email, $valor, $token){
    $to = $email;
    $subject = 'Comprovante de Pagamento - 10ª Semana Acadêmica do Campus Tecnológico de Alegrete';

    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'From: SACTA X <nao.responda@cec-alegrete.com.br>' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'X-Mailer: PHP/' . phpversion();

    $message = '<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<body lang=EN-GB style="tab-interval:36.0pt">

<h2 align=center style="margin-top:22.5pt;margin-right:0cm;margin-bottom:7.5pt;
margin-left:0cm;text-align:center;line-height:42.0pt"><span style="font-size:
13.5pt;font-family:"Comic Sans MS";color:black;background:white">10ª Semana
Acadêmica do Campus Tecnológico de Alegrete</span></h2>

<p class=MsoNormal align=center style="text-align:center"><span
style="font-family:"Comic Sans MS"">Centro Estudantil Campus Alegrete</span></p>

<p class=MsoNormal align=center style="text-align:center"><span
style="font-family:"Comic Sans MS"">Fundação Universidade Federal do Pampa</span></p>

<p class=MsoNormal align=center style="text-align:center"><o:p>&nbsp;</o:p></p>

<p class=MsoNormal align=center style="text-align:center"><span
style="font-family:"Comic Sans MS"">===== COMPROVANTE DE PAGAMENTO =====</span></p>

<p class=MsoNormal align=center style="text-align:center"><o:p>&nbsp;</o:p></p>

<p class=MsoNormal align=center style="text-align:center"><o:p>&nbsp;</o:p></p>

<p class=MsoNormal align=center style="text-align:center"><span
style="font-family:"Comic Sans MS"">Recebemos de <b>' . $nome . '</b> o valor de
R$' . $valor . ' referente à 01 inscrição na&nbsp;<b><span style="color:black">10ª
Semana Acadêmica do Campus Tecnológico de Alegrete.</span></b></span></p>

<p class=MsoNormal align=center style="text-align:center"><o:p>&nbsp;</o:p></p>

<div>

<div>

<p class=MsoNormal align=center style="text-align:center"><b><span
style="font-family:"Arial",sans-serif;color:black"><img width=400 height=400
id="_x0000_i1025"
src="https://chart.googleapis.com/chart?chs=400x400&amp;cht=qr&amp;chl=' . $token . '"><o:p></o:p></span></b></p>

</div>

<div>

<p class=MsoNormal align=center style="text-align:center"><span
style="font-family:"Courier New";color:black">UTILIZE ESSE QR-CODE PARA ACESSAR
AS SUAS ATIVIDADES</span><span style="color:black"><o:p></o:p></span></p>

</div>

<div>

<p class=MsoNormal align=center style="text-align:center"><b><span
style="font-family:"Arial",sans-serif;color:black"><o:p>&nbsp;</o:p></span></b></p>

</div>

<div>

<p class=MsoNormal align=center style="text-align:center"><b><span
style="font-family:"Arial",sans-serif;color:black"><o:p>&nbsp;</o:p></span></b></p>

</div>

<div>

<p class=MsoNormal align=center style="text-align:center"><b><span
style="font-family:"Arial",sans-serif;color:black"><o:p>&nbsp;</o:p></span></b></p>

</div>

<div>

<p class=MsoNormal align=center style="text-align:center"><b><span
style="font-family:"Arial",sans-serif;color:black"><o:p>&nbsp;</o:p></span></b></p>

</div>

<div>

<p class=MsoNormal align=center style="text-align:center"><b><span
style="font-family:"Arial",sans-serif;color:black"><o:p>&nbsp;</o:p></span></b></p>

</div>

<div>

<p class=MsoNormal align=center style="text-align:center"><b><span
style="font-family:"Arial",sans-serif;color:black"><o:p>&nbsp;</o:p></span></b></p>

</div>

<p class=MsoNormal align=right style="text-align:right"><b><span
style="font-family:"Arial",sans-serif;color:black"><o:p>&nbsp;</o:p></span></b></p>

<div>

<div>

<p class=MsoNormal><b><i><u><span style="font-family:"Arial",sans-serif;
color:black">GUARDE ESTE RECIBO ATÉ RECEBER SEU CERTIFICADO.</span></u></i></b><b><span
style="font-family:"Arial",sans-serif;color:black"><o:p></o:p></span></b></p>

</div>

<p style="margin:0cm;margin-bottom:.0001pt"><b
id="gmail-m_568729570860226637gmail-docs-internal-guid-43a8fbba-2f13-4e49-e1a5-341cede068c5"
style="text-align:start"><span style="font-family:"Arial",sans-serif;
color:#222222;background:white">Semana Acadêmica do Campus Tecnológico de
Alegrete de 2018</span></b><span style="font-size:9.5pt;font-family:"Arial",sans-serif;
color:#222222;background:white"><o:p></o:p></span></p>

<p class=MsoNormal><i><span style="color:black"><o:p>&nbsp;</o:p></span></i></p>

</div>

<p class=MsoNormal><span style="color:black"><o:p>&nbsp;</o:p></span></p>

</div>

<p class=MsoNormal><span style="color:black"><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style="text-align:center"><o:p>&nbsp;</o:p></p>

</div>

</div>

</body>

</html>';

    $enviado = mail($to, $subject, $message, $headers);
}