<?php

use Models\DAO\EventoDAO;
use Models\Evento;

defined("ACESSO_DIRETO") or die("<h1>Access denied</h1>");

date_default_timezone_set('America/Sao_Paulo');

// Definição global
$partes = explode(DIRECTORY_SEPARATOR, PATH_BASE);

// Definições de caminho
define("PATH_ROOT", implode(DIRECTORY_SEPARATOR, $partes));
define("PATH_CONTROLLERS", PATH_ROOT . DIRECTORY_SEPARATOR . "Controllers");
define("PATH_CORE", PATH_ROOT . DIRECTORY_SEPARATOR . "Core");
define("PATH_MODELS", PATH_ROOT . DIRECTORY_SEPARATOR . "Models");
define("PATH_VIEWS", PATH_ROOT . DIRECTORY_SEPARATOR . "views");

define("URL", "/");
define("PATH_CDN", URL . "views/cdn");

// Definições de desenvolvedor
define("DESENVOLVIMENTO", false);

if (DESENVOLVIMENTO) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
} else {
    error_reporting(0);
}

// Definições banco de dados
define("DB_HOST", "br900.hostgator.com.br");
define("DB_NAME", "cecalegr_sacta");
define("DB_USER", "cecalegr_sactax");
define("DB_PASSWORD", "-s07f@_a2=r=");
define("DB_CHARSET", "UTF8");
define("DB_DEBUG", defined("DESENVOLVIMENTO") ? DESENVOLVIMENTO : false);

// Informações do Sistema
$evento = (new EventoDAO())->consultar();

define("EVENTO_TITULO", $evento->getTitulo());
define("EVENTO_DATA_INICIO", $evento->getStringDataInicio());
define("EVENTO_HORARIO_INICIO", $evento->getStringHorarioInicio());
define("EVENTO_DATA_FIM", $evento->getStringDataFim());
define("EVENTO_HORARIO_FIM", $evento->getStringHorarioFim());
define("EVENTO_LOCAL", $evento->getLocal());
define("EVENTO_ESTADO", $evento->getEstado());
define("EVENTO_CIDADE", $evento->getCidade());
define("EVENTO_RESPONSAVEL", $evento->getResponsavel());
define("EVENTO_EMAIL", $evento->getEmail());
define("EVENTO_TELEFONE", $evento->getTelefone());
define("EVENTO_DESCRICAO", $evento->getDescricao());
define("EVENTO_LOGO", $evento->getLogo());
define("EVENTO_CARGA_HORARIA", $evento->getCargaHoraria());
define("EVENTO_IDIOMA_EVENTO", $evento->getIdiomaEvento());
define("EVENTO_SIGLA", $evento->getSigla());
