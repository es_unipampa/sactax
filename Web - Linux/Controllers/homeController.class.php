<?php

namespace Controllers;

use Views\HomeView;
use Models\Pessoa;
use Models\Usuario;
use Models\Participante;
use Models\DAO\ParticipanteDAO;
use Views\Estruturas\HTML;
use Models\Evento;
use Models\DAO\ContatoDAO;

defined("ACESSO_DIRETO") or die("<h1>Access denied</h1>");

class HomeController extends Controller {

    public function __construct() {
        
    }

    public function index() {       
        $html = new HTML("HomeView", "Página Inicial");
        $html->addCSS("https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i", true);
        $html->addCSS("bootstrap4/css/bootstrap.min.css");
        $html->addCSS("animate/animate.min.css");
        $html->addCSS("ionicons/css/ionicons.min.css");
        $html->addCSS("magnific-popup/magnific-popup.css");
        $html->addCSS("fonts/FontAwesome/font-awesome.min.css");
        $html->addCSS("css/home.css");

        $html->addJavascript("jquery/js/jquery.min.js");
        $html->addJavascript("jquery/js/jquery-migrate.min.js");
        $html->addJavascript("jquery/js/jquery.mask.min.js");
        $html->addJavascript("bootstrap4/js/bootstrap.bundle.min.js");
        $html->addJavascript("easing/easing.min.js");
        $html->addJavascript("wow/wow.min.js");
        $html->addJavascript("superfish/hoverIntent.js");
        $html->addJavascript("superfish/superfish.min.js");
        $html->addJavascript("magnific-popup/magnific-popup.min.js");
        $html->addJavascript("magnific-popup/magnific-popup.min.js");
        $html->addJavascript("js/home.js");

        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST))) {
            $api = new APIController();
            $api->cadastrarParticipante();

            $autenticar = $api->autenticarUsuario($_POST["email"], $_POST["senha"]);
            $usuario = json_decode($autenticar, true);
            session_start();
            parent::iniciarSessao($usuario["retorno"]);
            return $autenticar;
        } else {
            $html->gerar();
        }
    }

    public function contato() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST))) {

            $nome = $_POST["nome"];
            $email = $_POST["email"];
            $assunto = $_POST["assunto"];
            $mensagem = $_POST["mensagem"];

            if ($nome != "" && $email != "" && $assunto != "" && $mensagem != "") {
                $contatoDAO = new ContatoDAO();
                $contato = $contatoDAO->registar($nome, $email, $assunto, $mensagem);
                return $contato;
            } else {
                return "Preencha todos os campos.";
            }
        }
    }

}
