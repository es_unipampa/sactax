<?php

namespace Controllers;

use Views\Estruturas\JSON;
use \InvalidArgumentException;
use Models\Categoria;
use Models\Usuario;
use Models\Pessoa;
use Models\Privilegio;
use Models\Situacao;
use Models\Token;
use Models\DAO\CredenciamentoEventoDAO;
use Models\DAO\UsuarioDAO;
use Models\DAO\AtividadesDAO;
use Models\DAO\ContatoDAO;

/**
 * Description of LoginController
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class APIController extends Controller {

    private $json;

    public function __construct() {
        $this->json = new JSON();
    }

    public function index() {

        $msg = array("status" => array(
                "mensagem" => "Sistema de Interface de Programação de Aplicações à partir de uma Transferência de Estado Representacional - SACTA X",
                "codigoStatus" => 401
        ));

        header("Content-type:application/json; charset=UTF-8");
        die(json_encode($msg, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
//        return $this->solicitacaoInvalida();
    }

    public function credenciarParticipanteEvento() {

        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["token"]) && isset($_POST["idParticipante"]) && isset($_POST["entregue"])) {

            $token = new Token($_POST["token"]);
            $idcredenciador = $token->idUser();
            $idparticipante = $_POST["idParticipante"];
            $entregue = ((int) $_POST["entregue"]);

            $credenciamentoEventoDAO = new CredenciamentoEventoDAO();
            $usuarioDAO = new UsuarioDAO();
            try {
                $credenciamentoEventoDAO->credenciarParticipante($idcredenciador, $idparticipante, $entregue);
                $re = $usuarioDAO->credenciar($idparticipante);

                $arreacadado = $usuarioDAO->valorArrecadado();
                $pessoasConfirmadas = $usuarioDAO->pessoasConfirmadas();

                $json = array();
                $json["mensagem"] = $re;
                $json["arrecadado"] = $arreacadado;
                $json["confirmadas"] = $pessoasConfirmadas;

                $info = $usuarioDAO->consultarInformacoesEmail($idparticipante);

                $tokenParticipante = new Token();
                pagamentoConfirmado($info["nome"], $info["email"], $info["valor"], $token->gerar($idparticipante, $info["nome"]));
                return $this->json->gerar($json);
            } catch (\PDOException $ex) {
                switch ($ex->errorInfo[1]) {
                    default:
                        $mensagem = $ex->getMessage();
                        break;
                    case 1062:
                        $mensagem = "Participante já credenciado.";
                        break;
                }

                $this->solicitacaoInvalida($mensagem, 400);
            }
        }
        return $this->solicitacaoInvalida();
    }

    public function cadastrarParticipante() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["nome"]) && isset($_POST["email"]) && isset($_POST["telefone"]) && isset($_POST["senha"]) && isset($_POST["confsenha"])) {

            $this->verificarCampoVazio();

            $nome = $_POST["nome"];
            $email = $_POST["email"];
            $telefone = $_POST["telefone"];
            $senha = $_POST["senha"];
            $confsenha = $_POST["confsenha"];

            $pessoa = new Pessoa(mb_strtoupper($nome, "UTF-8"), $email);
            $privilegio = new Privilegio(1, "");
            $categoria = new Categoria(1, "");
            $situacao = new Situacao(1, "");

            $usuario = new Usuario($pessoa, $telefone, $privilegio, $categoria, $situacao);
            $usuario->setSenha($senha, $confsenha);
            $usuarioDAO = new UsuarioDAO();

            $retorno = $usuarioDAO->inserir($usuario);
            confirmacaoInscricao($nome, $email);

            return $this->json->gerar($retorno);
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function autenticarUsuario(string $email = null, string $senha = null) {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && ((isset($_POST["email"]) && isset($_POST["senha"])) || (($email != null) && ($senha != null)))) {

            $this->verificarCampoVazio();

            if (isset($_POST)) {
                $email = $_POST["email"];
                $senha = sha1($_POST["senha"]);
            }

            $usuarioDAO = new UsuarioDAO();
            $usuario = $usuarioDAO->autenticar($email, $senha);
            return $this->json->gerar($usuario);
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function validarCredenciador() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["codigoAcesso"])) {

            $this->verificarCampoVazio();

            $codigoAcesso = $_POST["codigoAcesso"];

            $usuarioDAO = new UsuarioDAO();
            $usuario = $usuarioDAO->autenticar($email, $senha);
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function gerarCodigoAcessoCredenciador() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["idPessoa"])) {

            $this->verificarCampoVazio();

            $id = $_POST["idPessoa"];

            $usuarioDAO = new UsuarioDAO();
            $usuario = $usuarioDAO->consultar();
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function consultarInformacoesUsuario() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["token"])) {

            $this->verificarCampoVazio();

            $token = new Token($_POST["token"]);

            $usuarioDAO = new UsuarioDAO();
            $usuario = $usuarioDAO->consultarInformacoes($token->idUser());

            return $this->json->gerar($usuario->toJSON());
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function consultarAtividades() {
        $atividadesDAO = new AtividadesDAO();
        $atividades = $atividadesDAO->consultarFormatado();
        header("Content-type:application/json; charset=UTF-8");
        return json_encode($atividades, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public function consultarTodasAtividadesParticipante(string $token = null) {

        if (isset($_POST) && !empty($_POST) && !empty($_POST["token"])) {
            $token = new Token($_POST["token"]);
        } else {
            $token = new Token($token);
        }

        $iduser = $token->idUser();

        if ($token != null) {
            $atividadesDAO = new AtividadesDAO();
            $atividades = $atividadesDAO->consultarFormatadoParticipante($iduser);
            return json_encode($atividades, JSON_UNESCAPED_UNICODE);
        }

        $this->solicitacaoInvalida();
    }

    public function inscreverAtividade() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["idAtividade"])) {
            $atividadesDAO = new AtividadesDAO();

            if (!isset($_SESSION)) {
                $token = new Token($_POST["token"]);
            } else {
                $token = new Token($_SESSION["usuario"]["pessoa"]["token"]);
            }

            $idParticipante = $token->idUser();
            $idAtividade = $_POST["idAtividade"];

            if ($atividadesDAO->consultarInscricao($idParticipante, $idAtividade) == null) {
                echo $this->json->gerar($atividadesDAO->inscrever($idParticipante, $idAtividade));
                return;
            }

            $this->solicitacaoInvalida($atividadesDAO->desinscrever($idParticipante, $idAtividade));
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function consultarQuantidadeInscritosPorAtividade() {
        $atividadesDAO = new AtividadesDAO();
        $atividades = $atividadesDAO->consultarQuantidadeInscritosPorAtividade();
        return json_encode($atividades, JSON_UNESCAPED_UNICODE);
    }

    public function listaCredenciarParticipantes() {
        $usuarioDAO = new UsuarioDAO();
        $naoCredenciados = $usuarioDAO->consultarNaoCredenciados();
        return json_encode($naoCredenciados, JSON_UNESCAPED_UNICODE);
    }

    public function pessoasConfirmadas() {
        $usuarioDAO = new UsuarioDAO();
        $confirmadas = $usuarioDAO->pessoasConfirmadas();

        return json_encode($confirmadas, JSON_UNESCAPED_UNICODE);
    }

    public function valorArrecadado() {
        $usuarioDAO = new UsuarioDAO();
        $valor = $usuarioDAO->valorArrecadado();

        return json_encode($valor, JSON_UNESCAPED_UNICODE);
    }

    public function participantesPorAtividade() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["idAtividade"])) {

            $idAtividade = $_POST["idAtividade"];

            $atividadesDAO = new AtividadesDAO();
            $atv = $atividadesDAO->participantesPorAtividade($idAtividade);
            $limite = $atividadesDAO->limiteVagas($idAtividade);

            $json = array("limite" => $limite);
            $json["participantes"] = $atv;

            return json_encode($json, JSON_UNESCAPED_UNICODE);
        } else {
            $parametros = parent::getParametros();
            
            if (count($parametros) == 0) {
                $arr = array("retorno" => "Informe o ID da atividade.");
                header("Content-type:application/json; charset=UTF-8");
                die(json_encode($arr, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
            } else {
                if (isset($parametros) && (is_numeric($parametros[0]))) {
                    $idAtividade = ((int) $parametros[0]);

                    $atividadesDAO = new AtividadesDAO();
                    $atv = $atividadesDAO->participantesPorAtividade($idAtividade);
                    header("Content-type:application/json; charset=UTF-8");
                    if ($atv == null){
                        $atv = array("retorno" => "Atividade não encontrada com o ID informado.");
                    }
                    die(json_encode($atv, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
                } else {
                    $arr = array("retorno" => "Informe um ID de atividade válido.");
                    header("Content-type:application/json; charset=UTF-8");
                    die(json_encode($arr, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
                }
            }
        }
    }

    public function credenciarParticipanteAtividade() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["tokenCredenciador"]) && isset($_POST["tokenParticipante"]) && isset($_POST["idAtividade"])) {
            $tokenCredenciador = $_POST["tokenCredenciador"];
            $tokenParticipante = $_POST["tokenParticipante"];
            $atividade = $_POST["idAtividade"];

            $TCredenciador = new Token($tokenCredenciador);
            $TParticipante = new Token($tokenParticipante);

            try {
                $idCredenciador = ((int) $TCredenciador->idUser());
            } catch (\InvalidArgumentException $ex) {
                $this->solicitacaoInvalida("Token credenciador inválido");
            }

            try {
                $idParticipante = ((int) $TParticipante->idUser());
            } catch (\InvalidArgumentException $ex) {
                $this->solicitacaoInvalida("Token participante inválido");
            }

            $atividadesDAO = new AtividadesDAO();
            if (!$atividadesDAO->verificarParticipanteCredenciadoAtividade($idParticipante, $atividade)) {
                $registra = $atividadesDAO->credenciarParticipanteAtividade($idCredenciador, $idParticipante, $atividade);
            } else {
                $this->solicitacaoInvalida("Participante jÃ¡ estÃ¡ credenciado nessa atividade.");
            }

            return $this->json->gerar($registra);
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function iniciarAtividade() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["tokenCredenciador"]) && isset($_POST["idAtividade"])) {
//            $tokenCredenciador = $_POST["tokenCredenciador"];
//            $atividade = $_POST["idAtividade"];
//            
//            $atividadesDAO = new AtividadesDAO();
//            $iniciar = $atividadesDAO->iniciarAtividade();
//            
//            return json_encode($registra, JSON_UNESCAPED_UNICODE);
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function finalizarAtividade() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["tokenCredenciador"]) && isset($_POST["idAtividade"])) {
            //IMPLEMENTAR
        } else {
            $this->solicitacaoInvalida();
        }
    }

    /* Metodos de verificação e resposta de erro */

    private function verificarCampoVazio() {
        foreach ($_POST as $chave => $valor) {
            $$chave = trim(strip_tags($valor));
            if (empty($valor)) {
                $this->solicitacaoInvalida("Por favor, preencha todos os campos.");
            }
        }
    }

    private function solicitacaoInvalida(string $mensagem = "Solicitação inválida.") {
        throw new InvalidArgumentException($this->json->gerar($mensagem), 400);
    }

}
