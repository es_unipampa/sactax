<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

/**
 * Description of LocalAtividade
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class LocalAtividade {
    private $idLocalAtividade;
    private $localAtividade;
    private $vagas;
    
    public function __construct(int $idLocalAtividade, string $localAtividade, int $vagas) {
        $this->setIdLocalAtividade($idLocalAtividade);
        $this->setLocalAtividade($localAtividade);
        $this->setVagas($vagas);
    }

    public function getIdLocalAtividade(): int {
        return $this->idLocalAtividade;
    }

    public function getLocalAtividade(): string {
        return $this->localAtividade;
    }

    public function getVagas(): int {
        return $this->vagas;
    }

    public function setIdLocalAtividade(int $idLocalAtividade): void {
        $this->idLocalAtividade = $idLocalAtividade;
    }

    public function setLocalAtividade(string $localAtividade): void {
        $this->localAtividade = $localAtividade;
    }

    public function setVagas(int $vagas): void {
        $this->vagas = $vagas;
    }

}
