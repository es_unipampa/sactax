<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

/**
 * Description of TipoAtividade
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class TipoAtividade {

    private $idTipoAtividade;
    private $tipoAtividade;

    public function __construct(int $idTipoAtividade, string $tipoAtividade) {
        $this->setIdTipoAtividade($idTipoAtividade);
        $this->setTipoAtividade($tipoAtividade);
    }

    public function getIdTipoAtividade(): int {
        return $this->idTipoAtividade;
    }

    public function getTipoAtividade(): string {
        return $this->tipoAtividade;
    }

    public function setIdTipoAtividade(int $idTipoAtividade): void {
        $this->idTipoAtividade = $idTipoAtividade;
    }

    public function setTipoAtividade(string $tipoAtividade): void {
        $this->tipoAtividade = $tipoAtividade;
    }

}
