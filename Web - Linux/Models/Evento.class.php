<?php

namespace Models;

use DateTime;

class Evento {

    private $titulo;
    private $dataInicio;
    private $dataFim;
    private $local;
    private $estado;
    private $cidade;
    private $responsavel;
    private $email;
    private $telefone;
    private $descricao;
    private $logo;
    private $cargaHoraria;
    private $idiomaEvento;
    private $sigla;

    public function getTitulo(): string {
        return $this->titulo;
    }

    public function getDataInicio(): DateTime {
        return $this->dataInicio;
    }

    public function getDataFim(): DateTime {
        return $this->dataFim;
    }

    public function getStringDataInicio(): string {
        return $this->dataInicio->format('d/m/Y');
    }

    public function getStringHorarioInicio(): string {
        return $this->dataInicio->format('H:i');
    }

    public function getStringDataFim(): string {
        return $this->dataFim->format('d/m/Y');
    }

    public function getStringHorarioFim(): string {
        return $this->dataFim->format('H:i');
    }

    public function getLocal(): string {
        return $this->local;
    }

    public function getEstado(): string {
        return $this->estado;
    }

    public function getCidade(): string {
        return $this->cidade;
    }

    public function getResponsavel(): string {
        return $this->responsavel;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function getTelefone(): string {
        return $this->telefone;
    }

    public function getDescricao(): string {
        return $this->descricao;
    }

    public function getLogo(): string {
        return $this->logo;
    }

    public function getCargaHoraria(): string {
        return $this->cargaHoraria;
    }

    public function getIdiomaEvento(): string {
        return $this->idiomaEvento;
    }

    public function getSigla(): string {
        return $this->sigla;
    }

    public function setTitulo($titulo): void {
        $this->titulo = $titulo;
    }

    public function setDataInicio($dataInicio): void {
        $this->dataInicio = $dataInicio;
    }

    public function setDataFim($dataFim): void {
        $this->dataFim = $dataFim;
    }

    public function setLocal($local): void {
        $this->local = $local;
    }

    public function setEstado($estado): void {
        $this->estado = $estado;
    }

    public function setCidade($cidade): void {
        $this->cidade = $cidade;
    }

    public function setResponsavel($responsavel): void {
        $this->responsavel = $responsavel;
    }

    public function setEmail($email): void {
        $this->email = $email;
    }

    public function setTelefone($telefone): void {
        $this->telefone = $telefone;
    }

    public function setDescricao($descricao): void {
        $this->descricao = $descricao;
    }

    public function setLogo($logo): void {
        $this->logo = $logo;
    }

    public function setCargaHoraria($cargaHoraria): void {
        $this->cargaHoraria = $cargaHoraria;
    }

    public function setIdiomaEvento($idiomaEvento): void {
        $this->idiomaEvento = $idiomaEvento;
    }

    public function setSigla($sigla): void {
        $this->sigla = $sigla;
    }

}
