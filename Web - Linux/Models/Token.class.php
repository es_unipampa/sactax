<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

use Models\DAO\UsuarioDAO;
use InvalidArgumentException;

/**
 * Description of Token
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class Token {

    private $token;

    public function __construct(string $token = null) {
        $this->token = $token;
    }

    /**
     * Gera um token de usuário
     *
     * @param int $var1 ID DO USUÁRIO
     * @param string $var2 NOME DO USUÁRIO
     *
     * @return string Token gerado
     */
    public function gerar(int $var1, string $var2): string {
        if ((isset($var1) && isset($var2)) && $var1 != 0 && $var2 != "") {
            $var2 = strtoupper(substr(sha1($var2), 0, 24));
            switch ((int) log10($var1) + 1) {
                case 0:
                    $var1 = 'V' . $var1;
                case 1:
                    $var1 = 'E' . $var1;
                case 2:
                    $var1 = 'D' . $var1;
                case 3:
                    $var1 = 'M' . $var1;
                case 4:
                    $var1 = 'G' . $var1;
            }

            if ($this->token === null) {
                return $this->token = $var2 .= base64_encode($var1);
            }

            return $var2 .= base64_encode($var1);
        }
    }

    /**
     * Valida o token
     *
     * @return bool true caso seja válido, caso contrário false
     */
    public function validar(): bool {
        if ($this->token != null && strlen($this->token) == 32) {
            $var1 = substr($this->token, 0, 24);
            $var2 = substr($this->token, 24, 8);
            $var2 = preg_replace('/[^0-9]/i', '', base64_decode($var2));
            
            $usuarioDAO = new UsuarioDAO();
            $nomeUsuario = $usuarioDAO->consultarNome($var2);
            
            $encode = $this->gerar($var2, $nomeUsuario);
            return ($encode == $this->token);
        }
    }

    public function encurtar(): string {
        $aleatorio = rand(0, 28);
        $encurtar = substr($this->token, $aleatorio, 4);

        $encurtar1 = substr($encurtar, 0, 2);
        $encurtar2 = substr($encurtar, 2, 2);

        if (strlen($aleatorio) == 1) {
            $aleatorio = "0" . $aleatorio;
        }
        return strtoupper($encurtar1 . $aleatorio . $encurtar2);
    }

    public function validarEncurtado(string $encurtado) {
        $encurtar1 = substr($encurtado, 0, 2);
        $aleatorio = substr($encurtado, 4, 2);
        $encurtar2 = substr($encurtado, 2, 2);

        $encurtado3 = $encurtado1 . $encurtar2;
    }

    public function idUser() {
        if (isset($this->token) && strlen($this->token) == 32 && $this->validar()) {
            return (int) preg_replace('/[^0-9]/i', '', base64_decode(substr($this->token, 24, 8)));
        }
        
        throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Token inválido"), 400);
    }

    public function getToken() {
        return $this->token;
    }

}
