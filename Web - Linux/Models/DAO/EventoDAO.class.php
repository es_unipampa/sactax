<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models\DAO;

use Models\Evento;
use \DateTime;

/**
 * Description of EventoDAO
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class EventoDAO extends DAO {

    public function consultar() {
        $stmt = parent::getConexao()->prepare("SELECT evento.*, estados.nome as nomeEstado FROM evento, estados WHERE idEvento = 1 AND estados.idEstado = evento.idEstado--;");
        $stmt->execute();
        $rs = $stmt->fetch();
        if (!$rs)
            return(null);

        $evento = new Evento();
        $evento->setTitulo($rs["titulo"]);
        $evento->setDataInicio(new DateTime($rs["dataInicio"]));
        $evento->setDataFim(new DateTime($rs["dataFim"]));
        $evento->setLocal($rs["local"]);
        $evento->setEstado($rs["nomeEstado"]);
        $evento->setCidade($rs["cidade"]);
        $evento->setResponsavel($rs["responsavel"]);
        $evento->setEmail($rs["email"]);
        $evento->setTelefone($rs["telefone"]);
        $evento->setDescricao($rs["descricao"]);
        $evento->setLogo($rs["logo"]);
        $evento->setCargaHoraria($rs["cargaHoraria"]);
        $evento->setIdiomaEvento($rs["idiomaEvento"]);
        $evento->setSigla($rs["sigla"]);
        
        $stmt = null;
        
        return $evento;
    }
}
