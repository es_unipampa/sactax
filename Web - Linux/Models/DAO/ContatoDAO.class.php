<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models\DAO;

/**
 * Description of ContatoDAO
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class ContatoDAO extends DAO {

    public function registar(string $nome, string $email, string $assunto, string $mensagem) {
        $stmt = parent::getConexao()->prepare("INSERT INTO `contatos` (`nome`, `email`, `assunto`, `mensagem`) VALUES (:nome, :email, :assunto, :mensagem)--;");
        $stmt->bindValue(":nome", $nome);
        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":assunto", $assunto);
        $stmt->bindValue(":mensagem", $mensagem);
        $stmt->execute();
        $this->conexao = null;
        
        return "Contato recebido com sucesso.";
    }

}
