<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models\DAO;

use Models\Database;
use Models\DAO\UsuarioDAO;
use \InvalidArgumentException;

/**
 * Description of CredenciamentoEventoDAO
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class CredenciamentoEventoDAO extends DAO {

    public function credenciarParticipante(int $idCredenciador, int $idParticipante, int $entregue) {
        if ($idCredenciador > 0 && $idParticipante > 0) {
            $usuarioDAO = new UsuarioDAO();
            if ($usuarioDAO->consultarPrivilegio($idCredenciador) > 1) {
                $stmt = parent::getConexao()->prepare("INSERT INTO credenciamentoevento (`credenciador_idpessoa`, `participante_idpessoa`, `horario`, `materialEntregue`) VALUES (:idCredenciador, :idParticipante, NOW(), :entregue)--;");
                $stmt->bindValue(":idCredenciador", $idCredenciador);
                $stmt->bindValue(":idParticipante", $idParticipante);
                $stmt->bindValue(":entregue", $entregue);
                $stmt->execute();
                $this->conexao = null;
            } else {
                throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Privilégios insuficientes"), 401);
            }
        }
    }
}
