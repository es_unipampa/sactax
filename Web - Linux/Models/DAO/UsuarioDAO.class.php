<?php

namespace Models\DAO;

use Models\Usuario;
use Models\Pessoa;
use Models\Categoria;
use Models\Privilegio;
use Models\Situacao;
use Models\Token;
use Models\DAO\PessoaDAO;
use InvalidArgumentException;
use \PDO;

/**
 * Description of UsuarioDAO
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class UsuarioDAO extends DAO {

    public function consultarInformacoes(int $id): Usuario {
        $stmt = parent::getConexao()->prepare("SELECT pessoas.idpessoa, pessoas.nome, pessoas.email, usuarios.telefone, privilegios.idprivilegio, privilegios.privilegio, categorias.idcategoria, categorias.categoria, situacoes.idsituacao, situacoes.situacao FROM pessoas, usuarios, privilegios, categorias, situacoes WHERE pessoas.idpessoa = :id AND pessoas.idpessoa = usuarios.idpessoa AND usuarios.idprivilegio = privilegios.idprivilegio AND usuarios.idcategoria = categorias.idcategoria AND usuarios.idsituacao = situacoes.idsituacao--;");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $rs = $stmt->fetch();
        if (!$rs)
            return(null);

        $pessoa = new Pessoa($rs["nome"], $rs["email"]);
        $privilegio = new Privilegio((int) $rs["idprivilegio"], $rs["privilegio"]);
        $categoria = new Categoria((int) $rs["idcategoria"], $rs["categoria"]);
        $situacao = new Situacao((int) $rs["idsituacao"], $rs["situacao"]);
        $usuario = new Usuario($pessoa, $rs["telefone"], $privilegio, $categoria, $situacao);

        $stmt = null;

        return $usuario;
    }

    public function consultarNome(int $id) {
        $stmt = parent::getConexao()->prepare("SELECT nome FROM pessoas WHERE pessoas.idpessoa = :id--;");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $rs = $stmt->fetch();
        if (!$rs)
            return(null);

        $stmt = null;

        return $rs[0];
    }

    public function consultarPrivilegio(int $id) {
        $stmt = parent::getConexao()->prepare("SELECT idPrivilegio FROM usuarios WHERE idpessoa = :id--;");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $rs = $stmt->fetch();
        if (!$rs)
            return(null);

        $stmt = null;

        return $rs[0];
    }

    public function consultarCategoria(int $id) {
        $stmt = parent::getConexao()->prepare("SELECT categoria FROM usuarios, categorias WHERE usuarios.idPessoa = :id AND usuarios.idCategoria = categorias.idCategoria--;");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $rs = $stmt->fetch();
        if (!$rs)
            return(null);

        $stmt = null;

        return $rs[0];
    }

    public function consultarSituacao(int $id) {
        $stmt = parent::getConexao()->prepare("SELECT situacao FROM usuarios, situacoes WHERE usuarios.idPessoa = :id AND usuarios.idSituacao = situacoes.idSituacao--;");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $rs = $stmt->fetch();
        if (!$rs)
            return(null);

        $stmt = null;

        return $rs[0];
    }

    public function inserir(Usuario $usuario) {
        if ($usuario != null) {

            $pessoaDAO = new PessoaDAO();
            $id = $pessoaDAO->inserir($usuario->getPessoa());

            $stmt = parent::getConexao()->prepare("INSERT INTO `usuarios` (`idpessoa`, `idprivilegio`, `senha`, `telefone`, `idcategoria`, `idsituacao`) VALUES (:id, 1, :senha, :telefone, 1, 1)--;");
            $stmt->bindValue(":id", $id);
            $stmt->bindValue(":senha", sha1($usuario->getSenha()));
            $stmt->bindValue(":telefone", $usuario->getTelefone());
            $stmt->execute();

            $this->conexao = null;

            return "Participante cadastrado com sucesso.";
        } else {
            throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Preencha todos os dados."), 400);
        }
    }

    public function autenticar(string $email, string $senha) {
        if ($email != null && $senha != null) {
            $stmt = parent::getConexao()->prepare("SELECT pessoas.idpessoa, pessoas.nome, pessoas.email, usuarios.telefone, privilegios.idprivilegio, privilegios.privilegio, categorias.idcategoria, categorias.categoria, situacoes.idsituacao, situacoes.situacao FROM pessoas, usuarios, privilegios, categorias, situacoes WHERE pessoas.email = :email AND BINARY usuarios.senha = :senha AND pessoas.idpessoa = usuarios.idpessoa AND usuarios.idprivilegio = privilegios.idprivilegio AND usuarios.idcategoria = categorias.idcategoria AND usuarios.idsituacao = situacoes.idsituacao--;");
            $stmt->bindValue(":email", $email);
            $stmt->bindValue(":senha", $senha);
            $stmt->execute();
            $rs = $stmt->fetch();
            if (!$rs)
                throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Email e/ou senha estão incorretos."), 401);

            $token = new Token();

            $info = array(
                "pessoa" => array(
                    "token" => $token->gerar($rs["idpessoa"], $rs["nome"]),
                    "nome" => $rs["nome"],
                    "email" => $rs["email"]
                ),
                "telefone" => $rs["telefone"],
                "privilegio" => array(
                    "idPrivilegio" => ((int) $rs["idprivilegio"]),
                    "privilegio" => $rs["privilegio"]
                ),
                "categoria" => array(
                    "idCategoria" => ((int) $rs["idcategoria"]),
                    "categoria" => $rs["categoria"]
                ),
                "situacao" => array(
                    "idSituacao" => ((int) $rs["idsituacao"]),
                    "situacao" => $rs["situacao"]
                )
            );

            $this->conexao = null;

            return $info;
        } else {
            throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Email e/ou senha estão incorretos."), 401);
        }
    }

    public function consultarNaoCredenciados() {
        $stmt = parent::getConexao()->prepare("SELECT pessoas.idpessoa, pessoas.nome, pessoas.email, situacoes.situacao FROM pessoas, usuarios, situacoes WHERE pessoas.idpessoa = usuarios.idpessoa AND usuarios.idsituacao = situacoes.idsituacao ORDER BY situacoes.situacao DESC--;");
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!$rs) {
            return(null);
        }

        return $rs;
    }

    public function credenciar($idparticipante) {
        $stmt = parent::getConexao()->prepare("UPDATE `usuarios` SET `idsituacao` = '2' WHERE `usuarios`.`idpessoa` = :id--;");
        $stmt->bindValue(":id", $idparticipante);
        $stmt->execute();

        return "Participante credenciado com sucesso.";
    }

    public function pessoasConfirmadas() {
        $stmt = parent::getConexao()->prepare("SELECT count(*) as quantidade FROM usuarios WHERE idsituacao = 2--;");
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!$rs) {
            return(null);
        }

        return $rs[0]["quantidade"];
    }

    public function valorArrecadado() {
        $stmt = parent::getConexao()->prepare("SELECT SUM(categorias.valor) as valor FROM usuarios LEFT JOIN categorias ON usuarios.idcategoria = categorias.idcategoria AND usuarios.idsituacao = 2    --;");
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!$rs) {
            return(null);
        }

        return $rs[0]["valor"];
    }

    public function verificarParticipanteCredenciadoEvento(int $idParticipante) {
        $stmt = parent::getConexao()->prepare("SELECT (usuarios.idsituacao = 2) as credenciado FROM usuarios WHERE usuarios.idpessoa = :idParticipante--;");
        $stmt->bindValue(":idParticipante", $idParticipante);
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!$rs) {
            return(null);
        }

        return ((bool) $rs[0]["credenciado"]);
    }

    public function consultarInformacoesEmail($idparticipante) {
        $stmt = parent::getConexao()->prepare("SELECT pessoas.nome, pessoas.email, categorias.valor FROM pessoas, usuarios, categorias WHERE pessoas.idpessoa = :id AND pessoas.idpessoa = usuarios.idpessoa AND usuarios.idcategoria = categorias.idcategoria--;");
        $stmt->bindParam(":id", $idparticipante);
        $stmt->execute();
        $rs = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$rs)
            return(null);

        return $rs;
    }
}
