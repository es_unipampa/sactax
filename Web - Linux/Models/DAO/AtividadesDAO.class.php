<?php

namespace Models\DAO;

use Models\Atividades;
use \PDO;
use \InvalidArgumentException;

/**
 * Description of AtividadesDAO
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class AtividadesDAO extends DAO {

    public function consultarFormatado() {
        $stmt = parent::getConexao()->prepare("SELECT atividades.idAtividade, atividades.nome, atividades.descricao, atividades.inicio, atividades.fim, tiposatividade.tipoatividade, localatividades.local, localatividades.limiteVagas, atividades.ministrante FROM atividades, tiposatividade, localatividades WHERE atividades.idtipoAtividade = tiposatividade.idtipoAtividade AND atividades.idlocalAtividade = localatividades.idlocalAtividade ORDER BY atividades.inicio, atividades.idAtividade--;");
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!$rs)
            return(null);

        return $rs;
    }

    public function consultarFormatadoParticipante(int $id) {
        $stmt = parent::getConexao()->prepare("SELECT atividades.idAtividade,
                                                    atividades.nome, 
                                                    atividades.descricao, 
                                                    atividades.inicio, 
                                                    atividades.fim, 
                                                    tiposatividade.tipoatividade, 
                                                    localatividades.local, 
                                                    localatividades.limiteVagas, 
                                                    atividades.ministrante,
                                                    (usuarios.idpessoa = :id) as estaInscrito

                                            FROM usuarios
                                            LEFT JOIN participantes_has_atividades ON usuarios.idpessoa = :id AND participantes_has_atividades.idpessoa = usuarios.idpessoa
                                            RIGHT JOIN atividades ON participantes_has_atividades.idAtividade = atividades.idatividade

                                            LEFT JOIN tiposatividade ON atividades.idtipoatividade = tiposatividade.idtipoatividade
                                            LEFT JOIN localatividades ON atividades.idlocalAtividade = localatividades.idlocalAtividade

                                            ORDER BY atividades.inicio, atividades.idAtividade--;");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!$rs)
            return(null);

        return $rs;
    }

    public function consultarQuantidadeInscritosPorAtividade() {
        $stmt = parent::getConexao()->prepare("SELECT atividades.nome, count(participantes_has_atividades.idatividade) as vagas FROM atividades, participantes_has_atividades WHERE atividades.idatividade = participantes_has_atividades.idatividade GROUP BY atividades.nome ORDER BY atividades.inicio, atividades.idatividade ASC--;");
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!$rs)
            return(null);

        return $rs;
    }

    public function inscrever(int $id, int $idatividade) {
        $stmt = parent::getConexao()->prepare("INSERT INTO participantes_has_atividades (`idpessoa`, `idatividade`, `inscricao`) VALUES (:id, :idAtividade, NOW())--;");
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":idAtividade", $idatividade);
        $stmt->execute();

        return "Você foi inscrito na atividade com sucesso.";
    }

    public function desinscrever(int $id, int $idatividade) {
        $stmt = parent::getConexao()->prepare(" DELETE FROM `participantes_has_atividades` WHERE `idpessoa` = :id and `idatividade` = :idAtividade--;");
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":idAtividade", $idatividade);
        $stmt->execute();

        return "Você cancelou sua inscrição na atividade com sucesso.";
    }

    public function consultarInscricao(int $id, int $idatividade) {
        $stmt = parent::getConexao()->prepare("SELECT * FROM participantes_has_atividades WHERE idpessoa = :id AND idatividade = :idAtividade;--;");
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":idAtividade", $idatividade);
        $stmt->execute();
        $rs = $stmt->fetch();
        if (!$rs)
            return(null);

        return $rs;
    }

    public function participantesPorAtividade(int $idatividade) {
        $stmt = parent::getConexao()->prepare("SELECT pessoas.idpessoa, pessoas.nome, pessoas.email, participantes_has_atividades.inscricao, @contador := @contador + 1 AS posicao FROM pessoas, usuarios, participantes_has_atividades, (SELECT @contador := 0) AS nada WHERE participantes_has_atividades.idatividade = :idAtividade AND pessoas.idpessoa = usuarios.idpessoa AND usuarios.idpessoa = participantes_has_atividades.idpessoa ORDER BY participantes_has_atividades.inscricao ASC--;");
        $stmt->bindParam(":idAtividade", $idatividade);
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!$rs)
            return(null);

        return $rs;
    }

    public function limiteVagas($idAtividade) {
        $stmt = parent::getConexao()->prepare("SELECT localatividades.limiteVagas FROM atividades, localatividades WHERE atividades.idatividade = :idAtividade AND atividades.idlocalAtividade = localatividades.idlocalAtividade--;");
        $stmt->bindParam(":idAtividade", $idAtividade);
        $stmt->execute();
        $rs = $stmt->fetch();
        if (!$rs)
            return(null);

        return $rs[0];
    }

    public function verificarParticipanteInscritoAtividade(int $idParticipante, int $idAtividade) {
        $stmt = parent::getConexao()->prepare("SELECT (usuarios.idpessoa = :idParticipante) as inscrito FROM usuarios, participantes_has_atividades WHERE usuarios.idpessoa = :idParticipante AND participantes_has_atividades.idatividade = :idAtividade AND usuarios.idpessoa = participantes_has_atividades.idpessoa--;");
        $stmt->bindParam(":idParticipante", $idParticipante);
        $stmt->bindParam(":idAtividade", $idAtividade);
        $stmt->execute();
        $rs = $stmt->fetch();
        if (!$rs)
            return(null);

        return ((bool)$rs[0]);
    }

    public function credenciarParticipanteAtividade(int $idCredenciador, int $idParticipante, int $idAtividade) {
        $usuarioDAO = new UsuarioDAO();
        $nomeParticipante = $usuarioDAO->consultarNome($idParticipante);
        $quantidadeVagas = $this->limiteVagas($idAtividade);

        // VERIFICAR SE O CREDENCIADOR TEM PRIVILÉGIO SUFICIENTE
        if ($usuarioDAO->consultarPrivilegio($idCredenciador) > 1) {
            //VERIFICAR SE O PARTICIPANTE ELE ESTÁ CREDENCIADO NO EVENTO
            if ($usuarioDAO->verificarParticipanteCredenciadoEvento($idParticipante)) {
                // VERIFICAR SE ELE ESTÁ INSCRITO NA ATIVIDADE
                if ($this->verificarParticipanteInscritoAtividade($idParticipante, $idAtividade)) {
                    // VERIFICAR SE ELE ESTÁ NA FILA PRIORITÁRIA -> POSIÇÃO NA FILA E QUANTIDADE DE VAGAS
//                    if ($this->estaFilaRegular($idParticipante, $idAtividade) <= $quantidadeVagas) {
                        $stmt = parent::getConexao()->prepare("INSERT INTO credenciamentoatividades (`credenciador_idpessoa`, `participante_idpessoa`, `idatividade`, `horario`) VALUES (:idCredenciador, :idParticipante, :idAtividade, NOW())--;");
                        $stmt->bindParam(":idCredenciador", $idCredenciador);
                        $stmt->bindParam(":idParticipante", $idParticipante);
                        $stmt->bindParam(":idAtividade", $idAtividade);
                        $stmt->execute();

                        return ((string) $nomeParticipante . " credenciado na atividade com sucesso.");
//                    } else {
//                        throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar($nomeParticipante . " estÃ¡ na fila de espera. Aguarde."), 400);
//                    }
                } else {
                    throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar($nomeParticipante . " não estÃ¡ inscrito na atividade."), 400);
                }
            } else {
                throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar($nomeParticipante . " não estÃ¡ credenciado no evento."), 400);
            }
        } else {
            throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("PrivilÃ©gios insuficientes."), 401);
        }
    }

    public function estaFilaRegular($idParticipante, $idAtividade) {
        $participantes = $this->participantesPorAtividade($idAtividade);

        foreach ($participantes as $key => $value) {
            if ($participantes[$key]["idpessoa"] == $idParticipante) {
                return $participantes[$key]["posicao"];
            }
        }
    }

    public function verificarParticipanteCredenciadoAtividade($idParticipante, $idAtividade) {
        $stmt = parent::getConexao()->prepare("SELECT (usuarios.idpessoa = :idParticipante) as inscrito FROM credenciamentoatividades, usuarios WHERE usuarios.idpessoa = :idParticipante AND credenciamentoatividades.idatividade = :idAtividade AND usuarios.idpessoa = credenciamentoatividades.participante_idpessoa--;");
        $stmt->bindParam(":idParticipante", $idParticipante);
        $stmt->bindParam(":idAtividade", $idAtividade);
        $stmt->execute();
        $rs = $stmt->fetch();
        if (!$rs)
            return(null);

        return ((bool)$rs[0]);
    }

    public function iniciarAtividade() {
        
        
    }
    
    public function finalizarAtividade() {
        
        
    }

}
