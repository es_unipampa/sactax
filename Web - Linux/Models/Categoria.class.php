<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

/**
 * Description of Categoria
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class Categoria {

    private $idCategoria;
    private $categoria;
    private $valor;

    public function __construct(int $idCategoria, string $categoria, float $valor = 15.00) {
        $this->setIdCategoria($idCategoria);
        $this->setCategoria($categoria);
        $this->setValor($valor);
    }

    public function getIdCategoria(): int {
        return $this->idCategoria;
    }

    public function getCategoria(): string {
        return $this->categoria;
    }

    public function getValor(): float {
        return $this->valor;
    }

    public function setIdCategoria(int $idCategoria) {
        $this->idCategoria = $idCategoria;
    }

    public function setCategoria(string $categoria) {
        $this->categoria = $categoria;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }

    public function toJSON(): array {
        return array(
            "idCategoria" => $this->getIdCategoria(),
            "categoria" => $this->getCategoria()
        );
    }

    public function toJSONcomValor(): array {
        return array(
            "idCategoria" => $this->getIdCategoria(),
            "categoria" => $this->getCategoria(),
            "valor" => $this->getValor()
        );
    }

}
