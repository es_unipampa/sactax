<body class="">
    <div class="wrapper ">
        <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo PATH_CDN ?>/imagens/sidebar-1.jpg">
            <div class="logo">
                <a href="#" class="simple-text logo-normal">
                    <img class="img logoTop" src="<?php echo PATH_CDN ?>/imagens/logo.png" />
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="nav-item active ">
                        <a class="nav-link" href="<?php echo URL ?>painel">
                            <i class="material-icons">dashboard</i>
                            <p>Resumo</p>
                        </a>
                    </li>
                    <p class="card-description">
                    <li class="nav-item ">
                        <a class="nav-link" href="<?php echo URL ?>painel/atividades">
                            <i class="material-icons">content_paste</i>
                            <p>Atividades</p>
                        </a>
                    </li>
                    </p>
                </ul>
            </div>
        </div>
        <div class="main-panel">
        <?php require_once("PainelTop.php"); ?>