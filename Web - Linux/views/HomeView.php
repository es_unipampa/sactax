<?php
defined("ACESSO_DIRETO") or die("<h1>Access denied</h1>");

$api = new \Controllers\APIController();
$atividades = json_decode($api->consultarAtividades(), true);

//header("Content-Type: text/html");
?>
<body>
    <header id="header">
        <div class="container">

            <div id="logo" class="pull-left">
                <h1>
                    <a href="#intro" class="scrollto">SACTA X</a>
                </h1>
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active">
                        <a href="#intro">Inicio</a>
                    </li>
                    <li>
                        <a href="#about">Sobre o evento</a>
                    </li>
                    <li>
                        <a href="#pricing">Pré-Inscrições</a>
                    </li>
                    <li>
                        <a href="#more-features">Programação</a>
                    </li>
                    <li>
                        <a href="#gallery">Divulgação</a>
                    </li>
                    <li>
                        <a href="#contact">Contato</a>
                    </li>
                    <li>
                        <a href="<?php echo URL ?>painel" class="ihc">Acessar painel</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    <section id="intro">

        <div class="intro-text">
            <h2>10ª Semana Acadêmica do Campus Tecnológico de Alegrete</h2>
            <p>25/06/2018 - 29/06/2018</p>
            <a href="#about" class="btn-get-started scrollto">Sobre o evento</a>
        </div>

        <div class="product-screens">

            <div class="product-screen-1 wow fadeInUp" data-wow-delay="0.4s" data-wow-duration="0.6s">
                <img src="<?php echo PATH_CDN; ?>/imagens/app1.jpg" alt="">
            </div>

            <div class="product-screen-2 wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="0.6s">
                <img src="<?php echo PATH_CDN; ?>/imagens/app2.jpg" alt="">
            </div>

            <div class="product-screen-3 wow fadeInUp" data-wow-duration="0.6s">
                <img src="<?php echo PATH_CDN; ?>/imagens/app3.jpg" alt="">
            </div>

        </div>

    </section>

    <main id="main">
        <section id="about" class="section-bg">
            <div class="container-fluid">
                <div class="section-header">
                    <h3 class="section-title">Sobre o evento</h3>
                    <span class="section-divider"></span>
                    <br />
                </div>

                <div class="row">
                    <div class="col-lg-6 about-img wow fadeInLeft">
                        <img src="<?php echo PATH_CDN; ?>/imagens/about-img.jpg" alt="">
                    </div>

                    <div class="col-lg-6 content wow fadeInRight">
                        <p>
                            &nbsp;&nbsp;&nbsp;&nbsp;A Semana Acadêmica do Campus Tecnológico de Alegrete - SACTA, busca estimular a iniciação dos alunos
                            de graduação no meio acadêmico e profissional, além de promover a troca de conhecimento, visão
                            do curso e percepção do meio de atuação profissional, despertando no participante o interesse
                            pela capacitação abrangente, continuada e responsável com o meio socioambiental. O foco de atuação
                            da 10ª Semana Acadêmica Campus Alegrete - UNIPAMPA é a classe de alunos de graduação e de pós-graduação
                            e profissionais da área de tecnologia, bem como pessoas da comunidade com espírito tecnológico
                            sustentável e científico da região oeste do Rio Grande do Sul e também busca estimular o interesse
                            de participação de estudantes de escolas de ensino médio da cidade, aproximando a comunidade
                            e incentivando a busca pelo saber tecnológico.
                        </p>
                        <p>
                            &nbsp;&nbsp;&nbsp;&nbsp;A Semana Acadêmica do Campus Tecnológico de Alegrete - SACTA, é um evento organizado em conjunto
                            entre discentes dos sete cursos do campus, com duração de 1 semana. A sua Primeira edição ocorreu
                            em 2007, com a integração e esforços dos 3 cursos pioneiros, com aproximadamente 300 participantes.
                            A segunda, em 2008, com aproximadamente 350 participantes. A Terceira em 2010 com aproximadamente
                            400 participantes. Sua quarta edição ocorreu em 2011 com aproximadamente 500 participantes. Já
                            em 2013, em sua 5ª edição, contou com aproximadamente 600 inscritos e com aproximadamente 60
                            palestras e minicursos nesta semana de atividades.
                        </p>
                        <p>
                            &nbsp;&nbsp;&nbsp;&nbsp;Neste ano de 2018 esperamos superar em quantidade e qualidade o evento do ano anterior, buscando
                            sempre oportunizar a troca de saberes entre os mais capacitados profissionais das áreas afins
                            e nossos discentes, para que possamos cada vez mais levar e elevar o nome da nossa instituição
                            UNIPAMPA na região do pampa e fronteira oeste do Rio Grande do Sul, no Brasil e no mundo.
                        </p>
                    </div>
                </div>

            </div>
        </section>

        <section id="pricing" class="section-bg">
            <div class="container">

                <div class="section-header">
                    <h3 class="section-title">Pré-Inscrições</h3>
                    <span class="section-divider"></span>
                    <p class="section-description">A pré-inscrição realizada no site não garante participação e certificação no evento. Para garantí-la
                        realize o pagamento no credenciamento.</p>
                </div>

                <div class="row">

                    <div class="col-lg-4 col-md-6">

                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="box featured wow fadeInUp">
                            <h3>Ouvinte</h3>
                            <h4>
                                <sup>R$</sup>15
                                <span> reais</span>
                            </h4>
                            <ul>
                                <li>
                                    <i class="ion-android-checkmark-circle"></i> Participar de todas as atividades</li>
                            </ul>
                            <a href="#" class="get-started-btn" data-toggle="modal" data-target="#modalRealizarInscricao">Realizar pré-inscrição</a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">

                    </div>

                </div>
            </div>
        </section>

        <section id="more-features">
            <div class="container">

                <div class="section-header">
                    <h3 class="section-title">Programação</h3>
                    <span class="section-divider"></span>
                </div>

                <div clas="row">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                <li class="nav-item custom-nav">
                                    <a class="nav-link active" href="#segunda" data-toggle="tab">Segunda-feira (25/06)</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#terca" data-toggle="tab">Terça-feira (26/06)</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#quarta" data-toggle="tab">Quarta-feira (27/06)</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#quinta" data-toggle="tab">Quinta-feira (28/06)</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sexta" data-toggle="tab">Sexta-feira (29/06)</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="tab-content text-center">
                            <div class="tab-pane active" id="segunda">
                                <ul class="list-group">
                                    <?php
                                    for ($i = 0; $i < count($atividades); $i++) {
                                        if ((new DateTime($atividades[$i]["inicio"]))->format("d") == "25") {
                                            echo '<li class="list-group-item list-group-item-action text-left">' . (new DateTime($atividades[$i]["inicio"]))->format("H:i") . ' - ' . $atividades[$i]["nome"] . '</li>';
                                        }
                                    }
                                    ?>

                                </ul>
                            </div>
                            <div class="tab-pane" id="terca">
                                <?php
                                for ($i = 0; $i < count($atividades); $i++) {
                                    if ((new DateTime($atividades[$i]["inicio"]))->format("d") == "26") {
                                        echo '<li class="list-group-item list-group-item-action text-left">' . (new DateTime($atividades[$i]["inicio"]))->format("H:i") . ' - ' . $atividades[$i]["nome"] . '</li>';
                                    }
                                }
                                ?>
                            </div>
                            <div class="tab-pane" id="quarta">
                                <?php
                                for ($i = 0; $i < count($atividades); $i++) {
                                    if ((new DateTime($atividades[$i]["inicio"]))->format("d") == "27") {
                                        echo '<li class="list-group-item list-group-item-action text-left">' . (new DateTime($atividades[$i]["inicio"]))->format("H:i") . ' - ' . $atividades[$i]["nome"] . '</li>';
                                    }
                                }
                                ?>
                            </div>
                            <div class="tab-pane" id="quinta">
                                <?php
                                for ($i = 0; $i < count($atividades); $i++) {
                                    if ((new DateTime($atividades[$i]["inicio"]))->format("d") == "28") {
                                        echo '<li class="list-group-item list-group-item-action text-left">' . (new DateTime($atividades[$i]["inicio"]))->format("H:i") . ' - ' . $atividades[$i]["nome"] . '</li>';
                                    }
                                }
                                ?>
                            </div>
                            <div class="tab-pane" id="sexta">
                                <?php
                                for ($i = 0; $i < count($atividades); $i++) {
                                    if ((new DateTime($atividades[$i]["inicio"]))->format("d") == "29") {
                                        echo '<li class="list-group-item list-group-item-action text-left">' . (new DateTime($atividades[$i]["inicio"]))->format("H:i") . ' - ' . $atividades[$i]["nome"] . '</li>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section id="gallery">
            <div class="container-fluid">
                <div class="section-header">
                    <h3 class="section-title">Divulgação</h3>
                    <span class="section-divider"></span>
                </div>

                <div class="row no-gutters">

                    <div class="col-lg-4 col-md-6">

                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="gallery-item wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            <a href="<?php echo PATH_CDN; ?>/imagens/banners/Competicao-IA.jpg" class="gallery-popup">
                                <img src="<?php echo PATH_CDN; ?>/imagens/banners/Competicao-IA.jpg" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">

                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="gallery-item wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            <a href="<?php echo PATH_CDN; ?>/imagens/banners/Abolição-Escravatura.jpg" class="gallery-popup">
                                <img src="<?php echo PATH_CDN; ?>/imagens/banners/Abolição-Escravatura.jpg" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="gallery-item wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            <a href="<?php echo PATH_CDN; ?>/imagens/banners/Investimento-Anjo.jpg" class="gallery-popup">
                                <img src="<?php echo PATH_CDN; ?>/imagens/banners/Investimento-Anjo.jpg" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="gallery-item wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            <a href="<?php echo PATH_CDN; ?>/imagens/banners/Capitalismo-Consciente.jpg" class="gallery-popup">
                                <img src="<?php echo PATH_CDN; ?>/imagens/banners/Capitalismo-Consciente.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="contact">
            <div class="container">
                <h3 class="section-title">Contatos</h3>
                <div class="row wow fadeInUp">
                    <div class="col-lg-4 col-md-4">
                        <div class="product-screen-3 wow fadeInUp" data-wow-duration="0.6s">
                            <img src="<?php echo PATH_CDN; ?>/imagens/logo.png" class="t-logo" alt="">
                        </div>

                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="info">
                            <div>
                                <i class="ion-ios-location-outline"></i>
                                <p> Av. Tiaraju, 810 - Ibirapuitã,
                                    <br>Alegrete - RS, 97546-550</p>
                            </div>

                            <div>
                                <i class="ion-ios-email-outline"></i>
                                <p>sactaunipampa@gmail.com</p>
                            </div>

                            <div>
                                <i class="ion-ios-telephone-outline"></i>
                                <p>+55 55 9 9976-9281</p>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-5 col-md-8">
                        <div class="form">
                            <div id="sendmessage"></div>
                            <div id="errormessage"></div>
                            <form id="contactForm" role="form" class="contactForm">
                                <div class="form-row">
                                    <div class="form-group col-lg-6">
                                        <input type="text" name="name" class="form-control" id="nomeContato" placeholder="Nome completo" data-rule="minlen:4" data-msg="Por favor, insira pelo menos 4 digitos"
                                               />
                                        <div class="validation"></div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="email" class="form-control" name="email" id="emailContato" placeholder="Email" data-rule="email" data-msg="Por favor, insira um email válido."
                                               />
                                        <div class="validation"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="subject" id="assuntoContato" placeholder="Assunto" data-rule="minlen:8" data-msg="Por favor, insira um titulo com pelo menos 8 digitos."
                                           />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="message" id="mensagemContato" rows="5" data-rule="required" data-msg="Insira uma mensagem" placeholder="Mensagem"></textarea>
                                    <div class="validation"></div>
                                </div>
                                <div class="text-center">
                                    <button type="button" id="enviarContato" title="Enviar mensagem">Enviar mensagem</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-lg-left text-center">
                    <div class="copyright">
                        &copy; Copyright
                        <strong>SACTA X</strong>. Todos os direitos reservado.
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
                        <a href="#intro" class="scrollto">Inicio</a>
                        <a href="#about" class="scrollto">Sobre</a>
                    </nav>
                </div>
            </div>
        </div>
    </footer>

    <a href="#" class="back-to-top">
        <i class="fa fa-chevron-up"></i>
    </a>

    <div class="modal fade" id="modalRealizarInscricao" tabindex="-1" role="dialog" aria-labelledby="modalRealizarInscricaoLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalRealizarInscricaoLabel">Realizar pré-inscrição</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <center>
                            <p>Atenção! O nome e o email informado deverá ser autenticos, pois os mesmos serão utilizados para gerar o certificado.</p>
                        </center>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Nome completo</label>
                            <input type="text" maxlength="100" class="form-control" id="participante_nome" required="required">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">E-mail</label>
                            <input type="email" maxlength="100" class="form-control" id="participante_email" required="required">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Telefone celular</label>
                            <input type="tel" maxlength="11" class="form-control" id="participante_telefone" required="required">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm">
                                    <label for="recipient-name" class="col-form-label">Senha</label>
                                    <input type="password" maxlength="20" class="form-control" id="participante_senha" required="required">
                                </div>
                                <div class="col-sm">
                                    <label for="recipient-name" class="col-form-label">Confirmação da senha</label>
                                    <input type="password" maxlength="20" class="form-control" id="participante_confirmar_senha" required="required">
                                </div>
                            </div>
                            <p id="retornoErro">&nbsp;</p>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" id="inscrever" class="btn btn-primary">Inscrever-se</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalInscricaoRealizada" tabindex="-1" role="dialog" aria-labelledby="modalInscricaoRealizada" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalInscricaoRealizadaLabel">Pré-inscrição realizada com sucesso!</h5>
                    <h6>Você está sendo redirecionado.</h6>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>