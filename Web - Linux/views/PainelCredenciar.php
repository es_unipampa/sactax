<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-success">
                    <h4 class="card-title">Credenciamento</h4>
                </div>
                <div class="card-body has-success">
                    <center><a href="#" class="btn btn-info btn-space" data-toggle="modal" data-target="#modalRealizarInscricao">Realizar pré-inscrição</a></center>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tabelaCredenciar"class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="text-center"> Situação </th>
                                        <th> Nome </th>
                                        <th> Email </th>
                                        <th> Material </th>
                                        <th class="text-center"> Credenciamento </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $api = new \Controllers\APIController();
                                    $participantes = json_decode($api->listaCredenciarParticipantes(), true);
                                    $classSituacao;

                                    for ($i = 0; $i < count($participantes); $i++) {

                                        if ($participantes[$i]["situacao"] == "Pré-Inscrito") {
                                            $classSituacao = "pre-inscrito";
                                        } else {
                                            $classSituacao = "confirmado";
                                        }

                                        echo '<tr>';
                                        echo '  <td class="text-center"><a labelCred="' . $participantes[$i]["idpessoa"] . '" class="' . $classSituacao . '">' . $participantes[$i]["situacao"] . '</a></td>';
                                        echo '  <td>' . $participantes[$i]["nome"] . '</td>';
                                        echo '  <td>' . $participantes[$i]["email"] . '</td>';
                                        echo '  <td><div class="form-check text-center"><label class="form-check-label"><input name="entregue-' . $participantes[$i]["idpessoa"] . '" id="entregue-' . $participantes[$i]["idpessoa"] . '" class="form-check-input" type="checkbox">Entregue<span class="form-check-sign"><span class="check has-success"></span></span></label></div></td>';
                                        echo '  <td class="text-center"><input type="button" onclick=credenciarParticipante(' . $participantes[$i]["idpessoa"] . ') value="Credenciar"></td>';
                                        echo '</tr>';
                                    }
                                    
                                    
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th> Situação </th>
                                        <th> Nome </th>
                                        <th> Email </th>
                                        <th> Material </th>
                                        <th> Credenciamento </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>                                    
</div>
</body>

<div class="modal fade" id="modalRealizarInscricao" tabindex="-1" role="dialog" aria-labelledby="modalRealizarInscricaoLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalRealizarInscricaoLabel">Realizar pré-inscrição</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <center>
                        <p>Atenção! O nome e o email informado deverá ser autenticos, pois os mesmos serão utilizados para gerar o certificado.</p>
                    </center>
                    <div class="form-group">
                        <label>Nome completo</label>
                        <input type="text" maxlength="100" class="form-control" id="participante_nome" required="required">
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="email" maxlength="100" class="form-control" id="participante_email" required="required">
                    </div>
                    <div class="form-group">
                        <label>Telefone celular</label>
                        <input type="tel" maxlength="11" class="form-control" id="participante_telefone" required="required">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm">
                                <label>Senha</label>
                                <input type="password" maxlength="20" class="form-control" id="participante_senha" required="required">
                            </div>
                            <div class="col-sm">
                                <label>Confirmação da senha</label>
                                <input type="password" name="s" maxlength="20" class="form-control" id="participante_confirmar_senha" required="required">
                            </div>
                        </div>
                        <p id="retornoErro" style="color: red; text-align:center">&nbsp;</p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="inscrever" class="btn btn-primary">Inscrever-se</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInscricaoRealizada" tabindex="-1" role="dialog" aria-labelledby="modalInscricaoRealizada" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalInscricaoRealizadaLabel">Pré-inscrição realizada com sucesso!</h5>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>