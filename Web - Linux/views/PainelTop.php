<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#">Resumo</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <?php
                if ($_SESSION["usuario"]["privilegio"]["idPrivilegio"] > 1) {
                    echo '<li class="nav-item dropdown">
                                <a class="nav-link" id="navbarDropdownMenuArea" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                    <i class="material-icons">assignment_ind</i>
                                    <p class="d-lg-none d-md-block">Área</p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuArea">';
                }
                
                switch ($_SESSION["usuario"]["privilegio"]["idPrivilegio"]) {
                    case 3:
                        echo '<a class="dropdown-item label-verde" href="' . URL . 'painel/area/organizador">Área do organizador</a>';
                    case 2:
                        echo '<a class="dropdown-item label-verde" href="' . URL . 'painel/area/credenciador">Área do credenciador</a>';
                        echo '<a class="dropdown-item label-verde" href="' . URL . 'painel/area/participante">Área do participante</a>
                            </div>';
                }
                
                if ($_SESSION["usuario"]["privilegio"]["idPrivilegio"] > 1) {
                    echo '</li>';
                }
                ?>

                <li class="nav-item dropdown">
                    <a class="nav-link" id="navbarDropdownMenuUsuario" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                        <i class="material-icons">person</i>
                        <p class="d-lg-none d-md-block">Usuário</p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuUsuario">
                        <a class="dropdown-item label-verde" href="<?php echo URL ?>painel/sair">Sair</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="content" id="jspage">