                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-success">
                                    <h4 class="card-title">Informações do evento</h4>
                                    <p class="card-category"><?php echo EVENTO_TITULO ?></p>
                                </div>
                                <div class="card-body has-success">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="form-inline">
                                                    <i class="material-icons">calendar_today</i>
                                                    <h4 class="adjust"><?php printf("%s às %s a %s às %s", EVENTO_DATA_INICIO, EVENTO_HORARIO_INICIO, EVENTO_DATA_FIM, EVENTO_HORARIO_FIM) ?></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="form-inline">
                                                    <i class="material-icons">location_on</i>
                                                    <h4 class="adjust">Av. Tiaraju, 810 - Ibirapuitã, Alegrete - RS, Brasil</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body mapa">
                                    <div class="map mapa">
                                        <iframe frameborder="0" class="map-frame" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAF75jldi7BVLeH6LQXIKtuPkIsvyWSfKk&amp;q=Av. Tiaraju, 810 - Ibirapuitã, Alegrete - RS, Brasil" allowfullscreen="allowfullscreen" style="border: 0px;"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                                    
    </div>
</body>
