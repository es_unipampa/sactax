<?php

namespace Views\Estruturas;

/**
 * Description of Head
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class Head {

    private $titulo;
    private $descricao;
    private $palavrasChave;
    private $listaCSS;
    private $corTema;
    private $icone;

    public function __construct(string $titulo = null, string $descricao = null) {
        $this->titulo = $titulo;
        $this->descricao = $descricao;
        $this->palavrasChave = array();
        $this->listaCSS = array();
        $this->corTema = "#1dc8cd";
    }

    public function addCSS(string $css, bool $externo = false) {
        if ($externo) {
            return array_push($this->listaCSS, $css);
        }
        return array_push($this->listaCSS, PATH_CDN . "/" . $css);
    }

    public function addPalavrasChave(string $palavra) {
        array_push($this->palavrasChave, $palavra);
    }

    public function gerar() {
        $estrutura = "<html lang=\"pt-br\">";
        $estrutura .= "\n\t<head>";
        $estrutura .= "\n\t\t<meta charset=\"UTF-8\">";
        $estrutura .= "\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
        $estrutura .= "\n\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">";
        $estrutura .= "\n\t\t<title>" . sprintf("%s :: %s", $this->titulo, EVENTO_SIGLA) . "</title>";

        if ($this->descricao != null) {
            $estrutura .= "\n\t\t<meta name=\"description\" content=\"" . $this->descricao . "\"/>";
        }

        if ($this->palavrasChave != null) {
            $estrutura .= "\n\t\t<meta name=\"keywords\" content=\"" . implode($this->palavrasChave, ", ") . "\" />";
        }

        $estrutura .= "\n\t\t<meta name=\"author\" content=\"Gustavo Bittencourt Satheler e Michael Luis de N. Martins | GM Development\" />";
//        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
//<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
//<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
//<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
//<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
//<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
//<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
//<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
//<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
//<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
//<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
//<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
//<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
//<link rel="manifest" href="/manifest.json">
//<meta name="msapplication-TileColor" content="#ffffff">
//<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
//        $estrutura .= "\n\t\t<link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"" . PATH_CDN . "/apple-touch-icon.png\"/>";
        $estrutura .= "\n\t\t<link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"" . PATH_CDN . "/favicons/favicon-32x32.png\"/>";
        $estrutura .= "\n\t\t<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"" . PATH_CDN . "/favicons/favicon-16x16.png\"/>";
//        $estrutura .= "\n\t\t<link rel=\"manifest\" href=\"" . PATH_CDN . "/favicons/manifest.json\"/>";
//        $estrutura .= "\n\t\t<link rel=\"mask-icon\" href=\"" . PATH_CDN . "/imagens/favicons/safari-pinned-tab.svg\" color=\"" . $this->corTema . "\"/>";
        $estrutura .= "\n\t\t<meta name=\"theme-color\" content=\"" . $this->corTema . "\"/>";

        if (count($this->listaCSS) > 0) {
            $estrutura .= "\n\n\t\t<!-- Cascading Style Sheets -->";
        }
        foreach ($this->listaCSS as $css) {
            $estrutura .= "\n\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $css . "\">";
        }

        $estrutura .= "\n\t</head>";

        return $estrutura;
    }

    public function getCorTema() {
        return $this->corTema;
    }

    public function setCorTema(string $corTema) {
        $this->corTema = $corTema;
    }

    public function setIcone(string $icone) {
        $this->icone = $icone;
    }

    public function setTitulo(string $titulo) {
        $this->titulo = $titulo;
    }

}
