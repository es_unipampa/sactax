<?php

namespace Views\Estruturas;

use Models\Token;

/**
 * Description of JSON
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class JSON {

    public function __construct() {
//        header("Content-Type: application/json; charset=utf-8");
    }

    public function gerar($retorno, Token $token = null) {
        $json = array();
        $json["retorno"] = $retorno;
        return json_encode($json, JSON_UNESCAPED_UNICODE);
    }
}
