<body class="">
    <div class="wrapper ">
        <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo PATH_CDN ?>/imagens/sidebar-1.jpg">
            <div class="logo">
                <a href="/" class="simple-text logo-normal">
                    <img class="img logoTop" src="<?php echo PATH_CDN ?>/imagens/logo.png" />
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="nav-item active ">
                        <a class="nav-link" href="<?php echo URL ?>painel">
                            <i class="material-icons">dashboard</i>
                            <p>Credenciar</p>
                        </a>
                    </li>
                </ul>
                <div class="card side-menu">
                    <div class="card-header card-header-info">
                        <h4 class="card-title">Total arrecadado</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-description">
                        <h3 class="text-center">R$ <a class="text-center" id="valorArrecadado"><?php
                        
                        $api = new Controllers\APIController;
                        $valor = json_decode($api->valorArrecadado(), true);
                        if($valor == NULL){
                            echo "0.00";
                        } else {
                            echo $valor;
                        }
                        
                        
                        ?></a></h3>
                        </p>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title">Pessoas confirmadas</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-description">
                        <h3 class="text-center" id="participantesConfirmados"><?php echo json_decode($api->pessoasConfirmadas(), true); ?></h3>
                        </p>
                    </div>
                </div>
            </div>

        </div>
        <div class="main-panel">
            <?php require_once("PainelTop.php"); ?>