$("[ref]").on("click", function () {
    $(".nav-item.active").removeClass("active");
    $("#jspage").html("");
    url = $(this).attr("ref");
    $(this).parent().addClass("active");
    $("#jspage").load(url);
    $(".navbar-brand").text($(this).children().get(1).textContent);
    $("title").text($(this).children().get(1).textContent + " :: SACTA X")
});

var resposta;
var credenciar;

function inscrever(idAtv, me) {
    $.ajax({
        type: 'POST',
        cache: false,
        timeout: 30000,
        data: {
            idAtividade: idAtv
        },
        error: function (response) {
            element = jQuery(me);
            element.removeClass("btn-danger");
            element.addClass("btn-success");
            element.html("<i class=\"material-icons\">check</i> Inscrever-se");
            notificacao(JSON.parse(response.statusText).retorno, "warning");
        },
        success: function (response) {
            resposta = response;
            element = jQuery(me);
            element.removeClass("btn-success");
            element.addClass("btn-danger");
            element.html("<i class=\"material-icons\">clear</i> Cancelar inscrição");
            notificacao(JSON.parse(response).retorno, "success");
        }
    });
}

function notificacao(mensagem, tipo) {

    $.notify({
        icon: "add_alert",
        message: mensagem

    }, {
        type: tipo,
        delay: 3000,
        timer: 1000,
        placement: {
            from: "top",
            align: "right"
        }
    });
}

$(document).ready(function () {
    $(window.location.hash).click();
    credenciar = $('#tabelaCredenciar').DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ participantes por página",
            "zeroRecords": "Nenhum participante foi encontrado com o nome informado",
            "info": "Mostrando página _PAGE_ de _PAGES_ no total de _MAX_ participantes pré-inscritos",
            "infoEmpty": "Sem registros do participante",
            "infoFiltered": "(filtrado)",
            "search": "Pesquisar",
            "paginate": {
                "first": "Primeiro",
                "last": "Último",
                "next": "Próximo",
                "previous": "Anterior"
            }
        },
        "order": [[0, 'desc'], [1, 'asc']]
    });
    
});

function credenciarParticipante(idPart) {
    $.ajax({
        type: 'POST',
        cache: false,
        timeout: 30000,
        data: {
            idParticipante: idPart,
            entregue: $("#entregue-63").is(":checked")
        },
        error: function (response) {
            notificacao(JSON.parse(response.statusText).retorno, "warning");
        },
        success: function (response) {
            resposta = response;
            
            $("[labelCred=" + idPart + "]").removeClass("pre-inscrito");
            $("[labelCred=" + idPart + "]").addClass("confirmado");
            $("[labelCred=" + idPart + "]").text("Confirmado");

            notificacao((JSON.parse(response).retorno).mensagem, "success");
            $("#valorArrecadado").text((JSON.parse(response).retorno).arrecadado);
            $("#participantesConfirmados").text((JSON.parse(response).retorno).confirmadas);
            credenciar.search("").draw();
            credenciar.order([[1, 'desc']]);
        }
    });
}

$("#participante_telefone").mask("(00) 00000-0009");
$(document).ready(function () {
    $('#inscrever').on("click", function () {
        $.ajax({
            type: 'POST',
            data: {nome: $("#participante_nome").val(),
                email: $("#participante_email").val(),
                telefone: $("#participante_telefone").val(),
                senha: $("#participante_senha").val(),
                confsenha: $("#participante_confirmar_senha").val()
            },
            error: function (response) {
                resposta = response;
                $("#retornoErro").text(JSON.parse(response.statusText).retorno);
            },
            success: function (response) {
                resposta = response;
                $("#modalRealizarInscricao").modal("hide");
                $("#modalRealizarInscricao").find("input,textarea,select")
                        .val('')
                        .end()
                        .find("input[type=checkbox], input[type=radio]")
                        .prop("checked", "")
                        .end();
                $("#retorno").text("");
                $("#modalInscricaoRealizada").modal();
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        });
    });

    $("#modalRealizarInscricao").on("keyup", function (e) {
        if (e.keyCode === 13) {
            $("#inscrever").click();
        }
    });
});