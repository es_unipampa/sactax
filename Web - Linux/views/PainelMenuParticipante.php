<body class="">
    <div class="wrapper ">
        <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo PATH_CDN ?>/imagens/sidebar-1.jpg">
            <div class="logo">
                <a href="<?php echo URL; ?>" class="simple-text logo-normal">
                    <img class="img logoTop" src="<?php echo PATH_CDN ?>/imagens/logo.png" />
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="nav-item active ">
                        <a class="nav-link" id="resumo" href="#resumo" ref="<?php echo URL ?>painel/resumo">
                            <i class="material-icons">dashboard</i>
                            <p>Resumo</p>
                        </a>
                    </li>
                    <p class="card-description">
                    <li class="nav-item ">
                        <a class="nav-link" id="atividades" href="#atividades" ref="<?php echo URL ?>painel/atividades">
                            <i class="material-icons">content_paste</i>
                            <p>Atividades</p>
                        </a>
                    </li>
                    </p>
                </ul>

                <div class="card card-profile card-menu">
                    <div class="card-avatar">
                        <a href="#">
                            <img class="img" src="<?php echo PATH_CDN ?>/imagens/icones/usuario.svg" />
                        </a>
                    </div>
                    <div class="card-body">
                        <h4 class="card-title"><?php echo $this->getUsuario()->getPessoa()->getNome() ?></h4>
                        <h6 class="card-category text-gray"> <?php
                            $categoria = $this->getUsuario()->getCategoria()->getCategoria();
                            $situacao = $this->getUsuario()->getSituacao()->getSituacao();

                            printf("%s - %s", $categoria, $situacao);
                            ?> </h6>    
                    </div>
                </div>
                <?php if ($this->getUsuario()->getSituacao()->getIdSituacao() > 1) { ?>
                    <div class="card">
                        <div class="card-header card-header-success">
                            <h4 class="card-title">QRCode para autenticação</h4>
                        </div>
                        <div class="card-body">
                            <p class="card-description">

                                <img class="img qr-code" src="https://chart.googleapis.com/chart?chs=400x400&cht=qr&chl=<?php echo $_SESSION["usuario"]["pessoa"]["token"]; ?>" />
                            </p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="main-panel">
        <?php require_once("PainelTop.php"); ?>