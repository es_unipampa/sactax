<?php

/**
 * @param string $class Nome completo da classe.
 * @return void
 */
spl_autoload_register(function ($classe) {
    $prefix = '';
    $base_dir = __DIR__ . DIRECTORY_SEPARATOR;
    $len = strlen($prefix);
    if (strncmp($prefix, $classe, $len) !== 0) {
        return;
    }

    $relative_class = substr($classe, $len);
    $file = $base_dir . str_replace('\\', DIRECTORY_SEPARATOR, $relative_class) . '.class.php';

    if (file_exists($file)) {
        require_once $file;
    }
});