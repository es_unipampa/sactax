<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

use \DateTime;

/**
 * Description of CredenciamentoEvento
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class CredenciamentoEvento {

    private $idCredenciamentoEvento;
    private $credenciador;
    private $participante;
    private $horario;
    
    private $materialEntregue;

    function __construct(int $idCredenciamentoEvento = 0, int $credenciador = 0, int $participante = 0, DateTime $horario = null, bool $materialEntregue = false) {
        $this->setIdCredenciamentoEvento($idCredenciamentoEvento);
        $this->setCredenciador($credenciador);
        $this->setParticipante($participante);
        $this->setHorario($horario);
        $this->setMaterialEntregue($materialEntregue);
    }

    function getIdCredenciamentoEvento(): int {
        return $this->idCredenciamentoEvento;
    }

    function getCredenciador(): int {
        return $this->credenciador;
    }

    function getParticipante(): int {
        return $this->participante;
    }

    function getHorario(): DateTime {
        return $this->horario;
    }

    public function getMaterialEntregue(): bool {
        return $this->materialEntregue;
    }
    
    function setIdCredenciamentoEvento(int $idCredenciamentoEvento): void {
        $this->idCredenciamentoEvento = $idCredenciamentoEvento;
    }

    function setCredenciador(int $credenciador): void {
        $this->credenciador = $credenciador;
    }

    function setParticipante(int $participante): void {
        $this->participante = $participante;
    }

    function setHorario(DateTime $horario = null): void {
        $this->horario = $horario;
    }

    public function setMaterialEntregue(bool $materialEntregue = false): void {
        $this->materialEntregue = $materialEntregue;
    }

}
