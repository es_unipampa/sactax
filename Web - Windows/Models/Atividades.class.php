<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

use \DateTime;

/**
 * Description of Atividades
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class Atividades {

    private $idAtividade;
    private $nome;
    private $descricao;
    private $inicio;
    private $fim;
    private $cargaHoraria;
    private $tipoAtividade;
    private $intervaloAtividade;
    private $localAtividade;
    private $ministrante;

    public function __construct(int $idAtividade, string $nome, string $descricao, DateTime $inicio, DateTime $fim, DateTime $cargaHoraria, TipoAtividade $tipoAtividade, LocalAtividade $localAtividade, string $ministrante, IntervaloAtividade $intervaloAtividade = null) {
       $this->setIdAtividade($idAtividade);
       $this->setNome($nome);
       $this->setDescricao($descricao);
       $this->setInicio($inicio);
       $this->setFim($fim);
       $this->setCargaHoraria($cargaHoraria);
       $this->setTipoAtividade($tipoAtividade);
       $this->setLocalAtividade($localAtividade);
       $this->setMinistrante($ministrante);
       $this->setIntervaloAtividade($intervaloAtividade);
    }
    
    public function getIdAtividade(): int {
        return $this->idAtividade;
    }

    public function getNome(): string {
        return $this->nome;
    }

    public function getDescricao(): string {
        return $this->descricao;
    }

    public function getInicio(): DateTime {
        return $this->inicio;
    }

    public function getFim(): DateTime {
        return $this->fim;
    }

    public function getCargaHoraria(): DateTime {
        return $this->cargaHoraria;
    }

    public function getTipoAtividade(): TipoAtividade {
        return $this->tipoAtividade;
    }

    public function getIntervaloAtividade(): IntervaloAtividade {
        return $this->intervaloAtividade;
    }

    public function getLocalAtividade(): LocalAtividade {
        return $this->localAtividade;
    }

    public function getMinistrante(): string {
        return $this->ministrante;
    }

    public function setIdAtividade($idAtividade): void {
        $this->idAtividade = $idAtividade;
    }

    public function setNome($nome): void {
        $this->nome = $nome;
    }

    public function setDescricao($descricao): void {
        $this->descricao = $descricao;
    }

    public function setInicio($inicio): void {
        $this->inicio = $inicio;
    }

    public function setFim($fim): void {
        $this->fim = $fim;
    }

    public function setCargaHoraria($cargaHoraria): void {
        $this->cargaHoraria = $cargaHoraria;
    }

    public function setTipoAtividade($tipoAtividade): void {
        $this->tipoAtividade = $tipoAtividade;
    }

    public function setIntervaloAtividade($intervaloAtividade): void {
        $this->intervaloAtividade = $intervaloAtividade;
    }

    public function setLocalAtividade($localAtividade): void {
        $this->localAtividade = $localAtividade;
    }

    public function setMinistrante($ministrante): void {
        $this->ministrante = $ministrante;
    }

}
