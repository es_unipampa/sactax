<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

use DateTime;

/**
 * Description of IntervaloAtividade
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class IntervaloAtividade {

    private $idIntervaloAtividade;
    private $tipoIntervalo;
    private $inicio;
    private $fim;

    public function __construct(int $idIntervaloAtividade, TipoIntervalo $tipoIntervalo, DateTime $inicio, DateTime $fim) {
        $this->setIdIntervaloAtividade($idIntervaloAtividade);
        $this->setTipoIntervalo($tipoIntervalo);
        $this->setInicio($inicio);
        $this->setFim($fim);
    }

    public function getIdIntervaloAtividade(): int {
        return $this->idIntervaloAtividade;
    }

    public function getTipoIntervalo(): TipoIntervalo {
        return $this->tipoIntervalo;
    }

    public function getInicio(): DateTime {
        return $this->inicio;
    }

    public function getFim(): DateTime {
        return $this->fim;
    }

    public function setIdIntervaloAtividade($idIntervaloAtividade): void {
        $this->idIntervaloAtividade = $idIntervaloAtividade;
    }

    public function setTipoIntervalo($tipoIntervalo): void {
        $this->tipoIntervalo = $tipoIntervalo;
    }

    public function setInicio($inicio): void {
        $this->inicio = $inicio;
    }

    public function setFim($fim): void {
        $this->fim = $fim;
    }

}
