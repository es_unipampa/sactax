<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

/**
 * Description of TipoIntervalo
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class TipoIntervalo {
    private $idTipoIntervalo;
    private $tipoIntervalo;
    
    public function __construct(int $idTipoIntervalo, string $tipoIntervalo = null) {
        $this->setIdTipoIntervalo($idTipoIntervalo);
        $this->setTipoIntervalo($tipoIntervalo);
    }
    
    public function getIdTipoIntervalo(): int {
        return $this->idTipoIntervalo;
    }

    public function getTipoIntervalo(): string {
        return $this->tipoIntervalo;
    }

    public function setIdTipoIntervalo($idTipoIntervalo): void {
        $this->idTipoIntervalo = $idTipoIntervalo;
    }

    public function setTipoIntervalo($tipoIntervalo): void {
        $this->tipoIntervalo = $tipoIntervalo;
    }

}
