<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

use Models\Privilegio;
use Models\Pessoa;
use \InvalidArgumentException;

/**
 * Description of Usuario
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class Usuario {
    
    private $pessoa;
    private $senha;
    private $telefone;
    private $privilegio;
    private $categoria;
    private $situacao;

    function __construct(Pessoa $pessoa, string $telefone, Privilegio $privilegio, Categoria $categoria, Situacao $situacao) {
        $this->setPessoa($pessoa);
        $this->setTelefone($telefone);
        $this->setPrivilegio($privilegio);
        $this->setCategoria($categoria);
        $this->setSituacao($situacao);
    }

    function getPessoa(): Pessoa {
        return $this->pessoa;
    }

    function getSenha(): string {
        return $this->senha;
    }

    public function getTelefone(): string {
        return $this->telefone;
    }

    function getPrivilegio(): Privilegio {
        return $this->privilegio;
    }

    public function getCategoria(): Categoria {
        return $this->categoria;
    }

    public function getSituacao(): Situacao {
        return $this->situacao;
    }

    function setSenha(string $senha = null, string $confsenha = null): void {
        if (!isset($senha) || strlen($senha) < 6) {
            throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Informe a senha válida. Minimo de 6 dígitos."), 400);
        }

        if ($confsenha != null) {
            if ($senha != $confsenha) {
                throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("A confirmação da senha não coincide com a sua senha digitada."), 400);
            }
        }

        $this->senha = $senha;
    }

    function setPessoa(Pessoa $pessoa): void {
        $this->pessoa = $pessoa;
    }

    function setPrivilegio(Privilegio $privilegio): void {
        $this->privilegio = $privilegio;
    }

    public function setTelefone(string $telefone): void {
        if ((!isset($telefone) || !is_numeric(preg_replace('/[^0-9a-zA-Z]/i', '', $telefone))) || strlen(preg_replace('/[^0-9a-zA-Z]/i', '', $telefone)) < 11) {
            throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Informe um telefone válido."), 400);
        }

        $this->telefone = preg_replace('/[^0-9]/i', '', $telefone);
    }

    public function setCategoria(Categoria $categoria): void {
        $this->categoria = $categoria;
    }

    public function setSituacao(Situacao $situacao): void {
        $this->situacao = $situacao;
    }

    public function toJSON(): array {
        return array(        
            "pessoa"        => $this->getPessoa()->toJSON(),
            "telefone"      => $this->getTelefone(),
            "privilegio"    => $this->getPrivilegio()->toJSON(),
            "categoria"     => $this->getCategoria()->toJSON(),
            "situacao"      => $this->getSituacao()->toJSON()
        );
    }

}
