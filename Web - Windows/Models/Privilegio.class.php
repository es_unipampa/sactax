<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

/**
 * Description of Privilegio
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class Privilegio {
    private $idPrivilegio;
    private $privilegio;
    
    public function __construct(int $idPrivilegio, string $privilegio) {
        $this->setIdPrivilegio($idPrivilegio);
        $this->setPrivilegio($privilegio);
    }
    
    public function getIdPrivilegio(): int {
        return $this->idPrivilegio;
    }

    public function getPrivilegio(): string {
        return $this->privilegio;
    }

    public function setIdPrivilegio(int $idPrivilegio): void {
        $this->idPrivilegio = $idPrivilegio;
    }

    public function setPrivilegio(string $privilegio): void {
        $this->privilegio = $privilegio;
    }
    
    public function toJSON(): array {
        return array(
          "idPrivilegio" => $this->getIdPrivilegio(),
          "privilegio"  => $this->getPrivilegio()
        );
    }
}
