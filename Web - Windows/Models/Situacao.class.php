<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

/**
 * Description of Situacao
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class Situacao {
    private $idSituacao;
    private $situacao;
    
    public function __construct(int $idSituacao, string $situacao) {
        $this->setIdSituacao($idSituacao);
        $this->setSituacao($situacao);
    }
    
    public function getIdSituacao(): int {
        return $this->idSituacao;
    }

    public function getSituacao(): string {
        return $this->situacao;
    }

    public function setIdSituacao(int $idSituacao): void {
        $this->idSituacao = $idSituacao;
    }

    public function setSituacao(string $situacao): void {
        $this->situacao = $situacao;
    }

    public function toJSON(): array {
        return array(
            "idSitucao" => $this->getIdSituacao(),
            "situacao" => $this->getSituacao()
        );
    }

}
