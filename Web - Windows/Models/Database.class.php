<?php

namespace Models;

use \PDO;

defined("ACESSO_DIRETO") or die("<h1>Access denied</h1>");

class Database {

    private $host = DB_HOST;
    private $usuario = DB_USER;
    private $senha = DB_PASSWORD;
    private $banco = DB_NAME;
    private $charset = DB_CHARSET;
    private $debug = DB_DEBUG;
    private $conexao = null;

    public function __construct(string $host = null, string $usuario = null, string $senha = null, string $banco = null, string $charset = null, bool $debug = false) {
        $this->host = defined("DB_HOST") ? DB_HOST : $host;
        $this->usuario = defined("DB_USER") ? DB_USER : $usuario;
        $this->senha = defined("DB_PASSWORD") ? DB_PASSWORD : $senha;
        $this->banco = defined("DB_NAME") ? DB_NAME : $banco;
        $this->charset = defined("DB_CHARSET") ? DB_CHARSET : $charset;
        $this->debug = defined("DB_DEBUG") ? DB_DEBUG : $debug;

        $this->conectar();
    }

    final private function conectar(): void {
        $con = "mysql:host={$this->host};";
        $con .= "dbname={$this->banco};";
        $con .= "charset={$this->charset};";
        try {
            $this->conexao = new PDO($con, $this->usuario, $this->senha);

            if ($this->debug === true) {
                $this->conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }

            unset($this->host);
            unset($this->usuario);
            unset($this->senha);
            unset($this->banco);
            unset($this->charset);

        } catch (PDOException $e) {
            if ($this->debug === true) {
                die("Erro: " . $e->getMessage());
            }
        }
    }

    public function getConexao(): PDO {
        return $this->conexao;
    }

}
