<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

use \InvalidArgumentException;

/**
 * Description of Pessoa
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class Pessoa {
    
    private $idPessoa;
    private $nome;
    private $email;

    public function __construct(string $nome, string $email) {
        $this->setNome($nome);
        $this->setEmail($email);
    }
    
    public function getIdPessoa(): int {
        return $this->idPessoa;
    }

    public function setIdPessoa(int $idPessoa): void {
        if(!is_numeric($idPessoa)){
            throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Identificador de usuário inválido."), 400);
        }
        
        $this->idPessoa = $idPessoa;
    }

    public function getNome(): string {
        return $this->nome;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function setNome($nome): void {
        if ((!isset($nome) || (count(explode(" ", $nome)) < 2) || explode(" ", $nome)[1] == "")) {
            throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Informe seu nome completo."), 400);
        }

        $this->nome = $nome;
    }

    public function setEmail($email): void {
        if ((!isset($email) || !filter_var($email, FILTER_VALIDATE_EMAIL))) {
            throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Informe um email válido."), 400);
        }
        
        $this->email = $email;
    }
    
    public function toJSON(): array {
        return array(
            "nome"      => $this->nome,
            "email"     => $this->email
        );
    }

}
