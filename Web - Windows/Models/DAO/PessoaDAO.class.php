<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models\DAO;

use Models\Pessoa;

/**
 * Description of PessoaDAO
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class PessoaDAO extends DAO {

    public function inserir(Pessoa $pessoa): int {
        if ($pessoa != null) {
            $stmt = parent::getConexao()->prepare("INSERT INTO `pessoas` (`nome`, `email`) VALUES (:nome, :email)--;");
            $stmt->bindValue(":nome", $pessoa->getNome());
            $stmt->bindValue(":email", $pessoa->getEmail());
            $stmt->execute();
            $id = parent::getConexao()->lastInsertId();

            $this->conexao = null;

            return (int) $id;
        } else {
            throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Preencha todos os dados."), 400);
        }
    }

}
