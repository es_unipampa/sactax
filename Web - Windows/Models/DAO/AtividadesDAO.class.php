<?php

namespace Models\DAO;

use Models\Atividades;
use \PDO;

/**
 * Description of AtividadesDAO
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class AtividadesDAO extends DAO {

    public function consultarFormatado(): array {
        $stmt = parent::getConexao()->prepare("SELECT atividades.idAtividade, atividades.nome, atividades.descricao, atividades.inicio, atividades.fim, tiposatividade.tipoatividade, localatividades.local, localatividades.limiteVagas, atividades.ministrante FROM atividades, tiposatividade, localatividades WHERE atividades.idtipoAtividade = tiposatividade.idtipoAtividade AND atividades.idlocalAtividade = localatividades.idlocalAtividade ORDER BY atividades.inicio, atividades.idatividade--;");
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!$rs)
            return(null);

        return $rs;
    }

    public function consultarFormatadoParticipante(int $id): array {
        $stmt = parent::getConexao()->prepare("SELECT atividades.idAtividade,
                                                    atividades.nome, 
                                                    atividades.descricao, 
                                                    atividades.inicio, 
                                                    atividades.fim, 
                                                    tiposatividade.tipoatividade, 
                                                    localatividades.local, 
                                                    localatividades.limiteVagas, 
                                                    atividades.ministrante,
                                                    (usuarios.idpessoa = :id) as estaInscrito

                                            FROM usuarios
                                            LEFT JOIN participantes_has_atividades ON usuarios.idpessoa = :id AND participantes_has_atividades.idpessoa = usuarios.idpessoa
                                            RIGHT JOIN atividades ON participantes_has_atividades.idAtividade = atividades.idatividade

                                            LEFT JOIN tiposatividade ON atividades.idtipoatividade = tiposatividade.idtipoatividade
                                            LEFT JOIN localatividades ON atividades.idlocalAtividade = localatividades.idlocalAtividade

                                            ORDER BY atividades.idAtividade--;");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!$rs)
            return(null);

        return $rs;
    }

    public function consultarQuantidadeInscritosPorAtividade() {
        $stmt = parent::getConexao()->prepare("SELECT atividades.nome, count(participantes_has_atividades.idatividade) as vagas FROM atividades LEFT JOIN participantes_has_atividades ON atividades.idatividade = participantes_has_atividades.idatividade GROUP BY atividades.nome ORDER BY atividades.inicio, atividades.idatividade--;");
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!$rs)
            return(null);

        return $rs;
    }

    public function inscrever(int $id, int $idatividade) {
        $stmt = parent::getConexao()->prepare("INSERT INTO participantes_has_atividades (`idpessoa`, `idatividade`, `inscricao`) VALUES (:id, :idAtividade, NOW())--;");
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":idAtividade", $idatividade);
        $stmt->execute();

        return "Você foi inscrito na atividade com sucesso.";
    }

    public function desinscrever(int $id, int $idatividade) {

        $stmt = parent::getConexao()->prepare(" DELETE FROM `participantes_has_atividades` WHERE `idpessoa` = :id and `idatividade` = :idAtividade--;");
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":idAtividade", $idatividade);
        $stmt->execute();

        return "Você cancelou sua inscrição na atividade com sucesso.";
    }

    public function consultarInscricao(int $id, int $idatividade) {
        $stmt = parent::getConexao()->prepare("SELECT * FROM participantes_has_atividades WHERE idpessoa = :id AND idatividade = :idAtividade;--;");
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":idAtividade", $idatividade);
        $stmt->execute();
        $rs = $stmt->fetch();
        if (!$rs)
            return(null);

        return $rs;
    }

}
