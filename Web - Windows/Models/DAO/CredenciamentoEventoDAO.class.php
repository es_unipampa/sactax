<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models\DAO;

use Models\Database;
use Models\DAO\UsuarioDAO;
use \InvalidArgumentException;

/**
 * Description of CredenciamentoEventoDAO
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class CredenciamentoEventoDAO extends DAO {

    public function credenciarParticipante(int $idCredenciador, int $idParticipante) {
        if ($idCredenciador > 0 && $idParticipante > 0) {
            $usuarioDAO = new UsuarioDAO();
            if ($usuarioDAO->consultarPrivilegio($idCredenciador) > 1) {

                $stmt = $this->conexao->prepare("INSERT INTO credenciamentoevento (usuarios_idpessoa, participantes_idpessoa, horario, materialEntregue) VALUES (:idCredenciador, :idParticipante, NOW(), 1)");
                $stmt->bindParam(":idCredenciador", $idCredenciador);
                $stmt->bindParam(":idParticipante", $idParticipante);
                $stmt->execute();
                $this->conexao = null;

                return $usuarioDAO->consultarNome($idParticipante);
            } else {
                throw new InvalidArgumentException((new \Views\Estruturas\JSON())->gerar("Usuário sem privilégio suficiente"), 400);
            }
        }
    }
}
