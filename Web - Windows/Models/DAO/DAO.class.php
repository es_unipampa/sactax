<?php

namespace Models\DAO;

use Models\Database;

/**
 * Description of DAO
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
abstract class DAO {

    private $conexao;

    public function __construct() {
        $this->conexao = new Database();
        $this->conexao = $this->conexao->getConexao();
    }
    
    public function getConexao() {
        return $this->conexao;
    }
    
    public function __destruct() {
        $this->conexao = null;
    }
}
