<?php

use Core\Sistema;

/**
 * Defina a versão mínima suportada do PHP do aplicativo como uma constante
 * para que ela possa ser referenciada dentro do sistema.
 */
define("SYSTEM_MINIMUM_PHP", "7.2.4");

if (version_compare(PHP_VERSION, SYSTEM_MINIMUM_PHP, '<')) {
    die("Configure o PHP para a versão " . SYSTEM_MINIMUM_PHP . " ou mais recente para este sistema funcionar corretamente.");
}

/**
 * Constante que está marcada nos arquivos incluídos para evitar acesso direto.
 */
define("ACESSO_DIRETO", true);

if (!defined("DEFINES")) {
    define("PATH_BASE", __DIR__);
    require_once PATH_BASE . "\\autoload.php";
    require_once PATH_BASE . "\\defines.php";
}

$app = new Sistema("Home");
$app->exibir();