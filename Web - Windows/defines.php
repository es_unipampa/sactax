<?php

use Models\DAO\EventoDAO;
use Models\Evento;

function debug($titulo, $objeto) {
    if ((defined("DESENVOLVIMENTO") && DESENVOLVIMENTO)) {
        $saida = $objeto;

        if (is_array($saida)) {
            $array = "[";

            for ($i = 0; $i < count($saida); $i++) {
                $array .= $saida[$i];

                if (count($saida) != ($i + 1)) {
                    $array .= ", ";
                }
            }

            $saida = $array . ']';
        }

        echo "<script>console.log('PHP Debug -> " . $titulo . ": " . $saida . "');</script>";
    }
}

defined("ACESSO_DIRETO") or die("<h1>Access denied</h1>");

date_default_timezone_set('America/Sao_Paulo');

// Definição global
$partes = explode(DIRECTORY_SEPARATOR, PATH_BASE);

// Definições de caminho
define("PATH_ROOT", implode(DIRECTORY_SEPARATOR, $partes));
define("PATH_CONTROLLERS", PATH_ROOT . DIRECTORY_SEPARATOR . "Controllers");
define("PATH_CORE", PATH_ROOT . DIRECTORY_SEPARATOR . "Core");
define("PATH_MODELS", PATH_ROOT . DIRECTORY_SEPARATOR . "Models");
define("PATH_VIEWS", PATH_ROOT . DIRECTORY_SEPARATOR . "Views");

define("URL", "/SACTAX/Web/");
define("PATH_CDN", URL . "views/cdn");

// Definições de desenvolvedor
define("DESENVOLVIMENTO", true);

if (DESENVOLVIMENTO) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

// Definições banco de dados
define("DB_HOST", "localhost");
define("DB_NAME", "cecalegr_sacta");
define("DB_USER", "root");
define("DB_PASSWORD", "caramboladev");
define("DB_CHARSET", "UTF8");
define("DB_DEBUG", defined("DESENVOLVIMENTO") ? DESENVOLVIMENTO : false);

// Informações do Sistema
$evento = (new EventoDAO())->consultar();

define("EVENTO_TITULO", $evento->getTitulo());
define("EVENTO_DATA_INICIO", $evento->getStringDataInicio());
define("EVENTO_HORARIO_INICIO", $evento->getStringHorarioInicio());
define("EVENTO_DATA_FIM", $evento->getStringDataFim());
define("EVENTO_HORARIO_FIM", $evento->getStringHorarioFim());
define("EVENTO_LOCAL", $evento->getLocal());
define("EVENTO_ESTADO", $evento->getEstado());
define("EVENTO_CIDADE", $evento->getCidade());
define("EVENTO_RESPONSAVEL", $evento->getResponsavel());
define("EVENTO_EMAIL", $evento->getEmail());
define("EVENTO_TELEFONE", $evento->getTelefone());
define("EVENTO_DESCRICAO", $evento->getDescricao());
define("EVENTO_LOGO", $evento->getLogo());
define("EVENTO_CARGA_HORARIA", $evento->getCargaHoraria());
define("EVENTO_IDIOMA_EVENTO", $evento->getIdiomaEvento());
define("EVENTO_SIGLA", $evento->getSigla());

function diaSemana(DateTime $data){
    switch ($data->format("l")){
        case "Monday":
            return "Segunda-feira";
        case "Tuesday":
            return "Terça-feira";
        case "Wednesday":
            return "Quarta-feira";
        case "Thursday":
            return "Quinta-feira";
        case "Friday":
            return "Sexta-feira";
        case "Saturday":
            return "Sábado";
        default:
            return "Domingo";
    }
}

function nomeMes(DateTime $data){
    switch ($data->format("M")){
        case "Jan":
            return "Janeiro";
        case "Feb":
            return "Fevereiro";
        case "Mar":
            return "Março";
        case "Apr":
            return "Abril";
        case "May":
            return "Maio";
        case "Jun":
            return "Junho";
        case "Jul":
            return "Julho";
        case "Aug":
            return "Agosto";
        case "Sep":
            return "Setembro";
        case "Oct":
            return "Outubro";
        case "Nov":
            return "Novembro";
        default:
            return "Dezembro";
    }
}