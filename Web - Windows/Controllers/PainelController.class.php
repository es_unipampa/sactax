<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controllers;

use Views\Estruturas\HTML;
use \Models\DAO\UsuarioDAO;
use Models\Token;
use Models\Usuario;

/**
 * Description of PainelController
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class PainelController extends Controller {

    private $privilegio;
    private $html;
    
    public function __construct() {
        parent::__construct(true);
        $this->html = new HTML();
        $this->html->addCSS("https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons", true);
        $this->html->addCSS("font/FontAwesome/font-awesome.min.css");
        $this->html->addCSS("material-design/css/material-dashboard.css?v=2.1.0");
        $this->html->addCSS("css/painel.css");

//        $this->html->setIcone($icone);

        $this->html->addJavascript("jquery/js/jquery.min.js");
        $this->html->addJavascript("js/popper.min.js");
        $this->html->addJavascript("bootstrap/js/bootstrap-material-design.min.js");
        $this->html->addJavascript("js/perfect-scrollbar.jquery.min.js");
        $this->html->addJavascript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDN5VSDkNWHlLpt-EdqMORs5RHP65pXTRo", true);
        $this->html->addJavascript("js/chartist.min.js");
        $this->html->addJavascript("bootstrap/js/bootstrap-notify.js");
        $this->html->addJavascript("material-design/js/material-dashboard.min.js?v=2.1.0");
        $this->html->addJavascript("js/painel.js");


        $token = new Token($_SESSION["usuario"]["pessoa"]["token"]);

        $usuarioDAO = new UsuarioDAO();

        try {
            $usuario = $usuarioDAO->consultarInformacoes($token->idUser());
        } catch (TypeError $ex) {
            parent::fecharSessao();
            echo header("Location: Login");
            return;
        }

        if (!isset($_SESSION["privilegio"])) {
            $_SESSION["privilegio"] = $usuario->getPrivilegio()->getIdPrivilegio();
        }

        $this->html->setUsuario($usuario);
    }

    public function index() {
        if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) {
            $api = new APIController();
            $api->inscreverAtividade();
            exit;
        }

        switch ($_SESSION["privilegio"]) {
            case 2:
                if ($this->html->getUsuario()->getPrivilegio()->getIdPrivilegio() >= $_SESSION["privilegio"]) {
                    $this->html->setPainel("PainelMenuCredenciador");
                    $this->credenciar();
                    break;
                }

            default:
                $this->html->setPainel("PainelMenuParticipante");
                $this->html->setTitulo("Resumo");
                $this->html->setBody("PainelResumoParticipante");
                break;
        }
        $this->html->gerar();
    }

    public function resumo() {

        require_once PATH_VIEWS . "/PainelResumoParticipante.php";
    }

    public function credenciar() {
        $this->html->setBody("PainelCredenciar");
    }

    public function atividades() {
        require_once PATH_VIEWS . "/PainelAtividadesParticipante.php";
    }

    public function sair() {
        parent::fecharSessao();
        header("Location: " . URL);
    }

    public function area() {
        $parametros = parent::getParametros();

        switch ($parametros[0]) {
            case "organizador":
            case "credenciador":
                $_SESSION["privilegio"] = 2;
                break;
            case "participante":
                $_SESSION["privilegio"] = 1;
                break;
        }

        header("Refresh:0, url=" . URL . "painel");
    }

}
