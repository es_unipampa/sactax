<?php

namespace Controllers;

use Views\Estruturas\JSON;
use \InvalidArgumentException;
use Models\Categoria;
use Models\Usuario;
use Models\Pessoa;
use Models\Privilegio;
use Models\Situacao;
use Models\Token;
use Models\DAO\CredenciamentoEventoDAO;
use Models\DAO\UsuarioDAO;
use Models\DAO\AtividadesDAO;
use Models\DAO\ContatoDAO;

/**
 * Description of LoginController
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class APIController extends Controller {

    private $json;

    public function __construct() {
        $this->json = new JSON();
    }

    public function index() {
        return $this->solicitacaoInvalida();
    }

    public function credenciarEvento() {
        $parametros = parent::getParametros();
        $token = new Token($parametros[0]);
        if (count($parametros) === 2 && $token->validar()) {
            $credenciamentoEventoDAO = new CredenciamentoEventoDAO();

            try {
                $this->solicitacaoInvalida();
//                return $this->exibirJSON($credenciamentoEventoDAO->credenciarParticipante($token->idUser(), $parametros[1]), $token, true);
            } catch (InvalidArgumentException $ex) {
                return $this->solicitacaoInvalida();
//                $this->exibirJSON($ex->getMessage(), $token);
            } catch (PDOException $ex) {
                switch ($ex->errorInfo[1]) {
                    default:
                        $mensagem = $ex->getMessage();
                        break;
                    case 1062:
                        $mensagem = "Usuario ja esta credenciado no evento";
                        break;
                    case 1452:
                        $mensagem = "Usuario nao encontrado";
                        break;
                }

                return $this->solicitacaoInvalida();
//                $this->exibirJSON($mensagem, $token);
            } catch (TypeError $ex) {
                return $this->solicitacaoInvalida();
//                $this->exibirJSON("Solicitacao invalida", $token);
            }
        }

        return $this->solicitacaoInvalida();
    }

    public function credenciarAtividade() {
        $parametros = parent::getParametros();
    }

    public function cadastrarParticipante() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["nome"]) && isset($_POST["email"]) && isset($_POST["telefone"]) && isset($_POST["senha"]) && isset($_POST["confsenha"])) {

            $this->verificarCampoVazio();

            $nome = $_POST["nome"];
            $email = $_POST["email"];
            $telefone = $_POST["telefone"];
            $senha = $_POST["senha"];
            $confsenha = $_POST["confsenha"];

            $pessoa = new Pessoa($nome, $email);
            $privilegio = new Privilegio(1, "");
            $categoria = new Categoria(1, "");
            $situacao = new Situacao(1, "");

            $usuario = new Usuario($pessoa, $telefone, $privilegio, $categoria, $situacao);
            $usuario->setSenha($senha, $confsenha);
            $usuarioDAO = new UsuarioDAO();

            return $this->json->gerar($usuarioDAO->inserir($usuario));
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function autenticarUsuario(string $email = null, string $senha = null) {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && ((isset($_POST["email"]) && isset($_POST["senha"])) || (($email != null) && ($senha != null)))) {

            $this->verificarCampoVazio();

            if (isset($_POST)) {
                $email = $_POST["email"];
                $senha = sha1($_POST["senha"]);
            }

            $usuarioDAO = new UsuarioDAO();
            $usuario = $usuarioDAO->autenticar($email, $senha);
            return $this->json->gerar($usuario);
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function validarCredenciador() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["codigoAcesso"])) {

            $this->verificarCampoVazio();

            $codigoAcesso = $_POST["codigoAcesso"];

            $usuarioDAO = new UsuarioDAO();
            $usuario = $usuarioDAO->autenticar($email, $senha);
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function gerarCodigoAcessoCredenciador() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["idPessoa"])) {

            $this->verificarCampoVazio();

            $id = $_POST["idPessoa"];

            $usuarioDAO = new UsuarioDAO();
            $usuario = $usuarioDAO->consultar();
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function consultarInformacoesUsuario() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["token"])) {

            $this->verificarCampoVazio();

            $token = new Token($_POST["token"]);

            $usuarioDAO = new UsuarioDAO();
            $usuario = $usuarioDAO->consultarInformacoes($token->idUser());

            return $this->json->gerar($usuario->toJSON());
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function consultarAtividades() {
        $atividadesDAO = new AtividadesDAO();

        $atividades = $atividadesDAO->consultarFormatado();

        return json_encode($atividades, JSON_UNESCAPED_UNICODE);
    }

    public function consultarTodasAtividadesParticipante(string $token = null) {

        if (isset($_POST) && !empty($_POST) && !empty($_POST["token"])) {
            $token = new Token($_POST["token"]);
        } else {
            $token = new Token($token);
        }

        $iduser = $token->idUser();

        if ($token != null) {
            $atividadesDAO = new AtividadesDAO();
            $atividades = $atividadesDAO->consultarFormatadoParticipante($iduser);
            return json_encode($atividades, JSON_UNESCAPED_UNICODE);
        }

        $this->solicitacaoInvalida();
    }

    public function inscreverAtividade() {
        if (($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) && isset($_POST["idAtividade"])) {
            $atividadesDAO = new AtividadesDAO();

            if (!isset($_SESSION)) {
                $token = new Token($_POST["token"]);
            } else {
                $token = new Token($_SESSION["usuario"]["pessoa"]["token"]);
            }

            $idParticipante = $token->idUser();
            $idAtividade = $_POST["idAtividade"];

            if ($atividadesDAO->consultarInscricao($idParticipante, $idAtividade) == null) {
                echo $this->json->gerar($atividadesDAO->inscrever($idParticipante, $idAtividade));
                return;
            }

            $this->solicitacaoInvalida($atividadesDAO->desinscrever($idParticipante, $idAtividade));
        } else {
            $this->solicitacaoInvalida();
        }
    }

    public function consultarQuantidadeInscritosPorAtividade() {
        $atividadesDAO = new AtividadesDAO();

        $atividades = $atividadesDAO->consultarQuantidadeInscritosPorAtividade();

        return json_encode($atividades, JSON_UNESCAPED_UNICODE);
    }

    /* Metodos de verificação e resposta de erro */

    private function verificarCampoVazio() {
        foreach ($_POST as $chave => $valor) {
            $$chave = trim(strip_tags($valor));
            if (empty($valor)) {
                $this->solicitacaoInvalida("Por favor, preencha todos os campos.");
            }
        }
    }

    private function solicitacaoInvalida(string $mensagem = "Solicitação inválida.") {
        throw new InvalidArgumentException($this->json->gerar($mensagem), 400);
    }

    public function testes() {
//        var_dump($_SESSION);  
    }

}
