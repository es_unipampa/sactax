<?php

namespace Controllers;

use Controllers\Controller;
use Views\Estruturas\HTML;

/**
 * Description of LoginController
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class LoginController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        if (parent::verificarLogado()) {
            header("Location: painel");
        }

        $html = new HTML("LoginView", "Painel do Participante");
        $html->addCSS("jquery/css/jquery-ui.css");
        $html->addCSS("css/login.css");

        $html->addJavascript("jquery/js/jquery.js");
        $html->addJavascript("jquery/js/jquery-ui.min.js");
        $html->addJavascript("js/login.js");

        if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST) && !empty($_POST)) {
            $api = new APIController();
            $autenticar = $api->autenticarUsuario();

            $usuario = json_decode($autenticar, true);
            parent::iniciarSessao($usuario["retorno"]);

            return $autenticar;
        } else {
            $html->gerar();
        }
    }
}
