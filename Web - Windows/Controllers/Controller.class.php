<?php

namespace Controllers;

use Models\Database;
use Models\Usuario;
use Models\Session;

defined("ACESSO_DIRETO") or die("<h1>Access denied</h1>");

abstract class Controller {

    private $permissaoMinima;
    private $necessitaLogin;
    private $parametros;
    private $myModel;
    private $estaLogado;
    private $usuario;

    public function __construct(bool $necessitaLogin = false, int $permissaoMinima = 1) {
        $this->necessitaLogin = $necessitaLogin;
        $this->permissaoMinima = $permissaoMinima;

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if ($necessitaLogin && !$this->verificarLogado()) {
            echo "entrei";
            return header("Location: " . URL . "login");
        }
    }

    protected function getModel() {
        return $this->myModel;
    }

    protected function setModel($myModel) {
        $this->myModel = $myModel;
    }

    public function setParametros($parametros = array()) {
        $this->parametros = $parametros;
    }

    protected function getParametros() {
        return $this->parametros;
    }

    public function getPermissaoMinima() {
        return $this->permissaoMinima;
    }

    public function getNecessitaLogin() {
        return $this->necessitaLogin;
    }

    public function setPermissaoMinima($permissaoMinima) {
        $this->permissaoMinima = $permissaoMinima;
    }

    public function setNecessitaLogin($necessitaLogin) {
        $this->necessitaLogin = $necessitaLogin;
    }

    public function iniciarSessao(array $usuario) {
        $this->estaLogado = true;
        $_SESSION["usuario"] = $usuario;
        $this->usuario = $usuario;
    }

    public function fecharSessao() {
        $this->estaLogado = false;
        $this->usuario = null;
        session_destroy();
    }

    public function verificarLogado() {
        return (isset($_SESSION["usuario"]));
    }

    public function verificarPermissao() {
        if ($this->usuario->getPermissao() < $this->permissaoMinima) {
            return die("Permissão insificiente.");
        }
    }

}
