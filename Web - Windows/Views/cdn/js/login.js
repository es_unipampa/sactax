var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var resposta;
$('input[type="submit"]').on("click", function (e) {
    entrar();
});
$('.login_fields').on("keyup", function (e) {
    if (e.keyCode === 13) {
        entrar();
    }
});

$('input[type="text"],input[type="password"]').focus(function () {
    $(this).prev().animate({'opacity': '1'}, 200)
});
$('input[type="text"],input[type="password"]').blur(function () {
    $(this).prev().animate({'opacity': '.5'}, 200)
});

$('input[type="text"]').keyup(function () {
    if (!$(this).val() == '' && filter.test($(this).val())) {
        $(this).next().animate({'opacity': '1', 'right': '30'}, 200);
    } else {
        $(this).next().animate({'opacity': '0', 'right': '20'}, 200);
    }
});
$('input[type="password"]').keyup(function () {
    if (!$(this).val() == '' && $(this).val().length > 5) {
        $(this).next().animate({'opacity': '1', 'right': '30'}, 200);
    } else {
        $(this).next().animate({'opacity': '0', 'right': '20'}, 200);
    }
});

function entrar() {
    if (filter.test($("#participante_email").val())) {
        if ($("#participante_senha").val().length > 5) {

            $("#retorno").fadeOut();
            $('.login').addClass('test')
            setTimeout(function () {
                $('.login').addClass('testtwo')
            }, 300);
            setTimeout(function () {
                $(".authent").show().animate({bottom: 450, left: 45}, {easing: 'easeOutQuint', duration: 600, queue: false});
                $(".authent").animate({opacity: 1}, {duration: 200, queue: false}).addClass('visible');
            }, 500);

            $.ajax({
                type: 'POST',
                async: false,
                cache: false,
                timeout: 30000,
                data: {email: $("#participante_email").val(),
                    senha: $("#participante_senha").val()
                },
                error: function (response) {
                    resposta = response;
                    setTimeout(function () {
                        $(".authent").show().animate({top: 300}, {easing: 'easeOutQuint', duration: 600, queue: false});
                        $(".authent").animate({opacity: 0}, {duration: 200, queue: false}).addClass('visible');
                        $('.login').removeClass('testtwo');
                    }, 2500);
                    setTimeout(function () {
                        $('.login').removeClass('test');
                        $('.login').fadeIn();
                    }, 2800);
                    setTimeout(function () {
                        $(".authent").removeClass('visible');
                        $(".authent").attr("style", '');
                        $('.login').removeClass('test');
                        $("#retorno").text(JSON.parse(resposta.statusText).retorno).fadeIn();
                    }, 3000);

                },
                success: function (response) {
                    resposta = response;
                    setTimeout(function () {
                        $(".authent").show().animate({top: 300}, {easing: 'easeOutQuint', duration: 600, queue: false});
                        $(".authent").animate({opacity: 0}, {duration: 200, queue: false}).addClass('visible');
                        $('.login').removeClass('testtwo');
                    }, 2500);
                    setTimeout(function () {
                        $('.login').removeClass('test');
                        $('.login div').fadeOut(123);
                    }, 2800);
                    setTimeout(function () {
                        $("#msgaut").text("Autenticação feita com sucesso.");
                        $("#bemvindo").text("Bem-vindo, ".concat(resposta.retorno.pessoa.nome));
                        $('.success').fadeIn();
                    }, 3200);
                    setTimeout(function () {
                        $('html').fadeOut();
                    }, 3900);
                    setTimeout(function () {
                        location.href="Painel";
                    }, 4800);
                }
            });
        } else {
            $("#retorno").text("Informe uma senha válida.");
        }
    } else {
        $("#retorno").text("Informe um e-mail válido.");
    }
}