$("[ref]").on("click", function () {
    $(".nav-item.active").removeClass("active");
    $("#jspage").html("");
    url = $(this).attr("ref");
    $(this).parent().addClass("active");
    $("#jspage").load(url);
    $(".navbar-brand").text($(this).children().get(1).textContent);
    $("title").text($(this).children().get(1).textContent + " :: SACTA X")
});

var resposta;

function inscrever(idAtv, me) {
    $.ajax({
        type: 'POST',
        cache: false,
        timeout: 30000,
        data: {
            idAtividade: idAtv
        },
        error: function (response) {
            element = jQuery(me);
            element.removeClass("btn-danger");
            element.addClass("btn-success");
            element.html("<i class=\"material-icons\">check</i> Inscrever-se");
            notificacao(JSON.parse(response.statusText).retorno, "warning");
        },
        success: function (response) {
            element = jQuery(me);
            element.removeClass("btn-success");
            element.addClass("btn-danger");
            element.html("<i class=\"material-icons\">clear</i> Cancelar inscrição");
            notificacao(response.retorno, "success");
        }
    });
}

function notificacao(mensagem, tipo) {

    $.notify({
        icon: "add_alert",
        message: mensagem

    }, {
        type: tipo,
        delay: 3000,
        timer: 1000,
        placement: {
            from: "top",
            align: "right"
        }
    });
}

$(document).ready(function () {
    $(window.location.hash).click();
})