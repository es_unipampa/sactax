<?php

namespace Views\Estruturas;

use Views\Estruturas\Head;
use Views\Estruturas\Javascript;
use \Models\Usuario;
use \Models\Bibliotecas\QRCode\QRCode;
/**
 * Description of HTML
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class HTML {

    private $head;
    private $painel;
    private $body;
    private $javascript;
    private $sessao;

    public function __construct(string $view = null, string $titulo = null, string $descricao = null) {
//        header("Content-Type: text/html");
        $this->head = new Head($titulo, $descricao);
        if($view != null)
            $this->body = $view;
        $this->javascript = new Javascript();
    }

    public function gerar() {
        echo $this->head->gerar();
        echo "\n\r";
        if($this->painel != null)
            $this->loadPainel();
        
        $this->loadView();
        echo "\n\r";
        echo $this->javascript->gerar();
    }

    public function addCSS(string $css, bool $externo = false) {
        $this->head->addCSS($css, $externo);
    }

    public function addPalavrasChave(string $palavra) {
        $this->head->addPalavrasChave($palavra);
    }

    public function addJavascript(string $javascript, bool $externo = false) {
        $this->javascript->addJavascript($javascript, $externo);
    }

    public function setBody(string $view) {
        $this->body = $view;
    }

    public function loadView() {
        return require_once PATH_VIEWS . '/' . $this->body . ".php";
    }

    public function setCorTema(string $corTema) {
        $this->head->setCorTema($corTema);
    }

    public function setIcone(string $icone) {
        $this->head->setIcone($icone);
    }

    public function setUsuario($usuario) {
        $this->sessao = $usuario;
    }

    public function getUsuario() {
        return $this->sessao;
    }

    public function setPainel($painel) {
        $this->painel = $painel;
    }
    
    public function loadPainel() {
        return require_once PATH_VIEWS . '/' . $this->painel . ".php";
    }
    
    public function setTitulo(string $titulo){
        $this->head->setTitulo($titulo);
    }

}
