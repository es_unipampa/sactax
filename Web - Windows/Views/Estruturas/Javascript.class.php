<?php

namespace Views\Estruturas;

/**
 * Description of Javascript
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class Javascript {

    private $listaJavascript;

    public function __construct() {
        $this->listaJavascript = array();
    }

    private function gerarEstrutura() {
        if (count($this->listaJavascript) > 0) {
            $estrutura = "\t<!-- Javascript -->";
        }

        foreach ($this->listaJavascript as $javascript) {
            $estrutura .= "\n\t<script src=\"" . $javascript . "\"></script>";
        }

        $estrutura .= "\n</html>";
        
        return $estrutura;
    }

    public function addJavascript(string $javascript, bool $externo = false) {
        if ($externo) {
            return array_push($this->listaJavascript, $javascript);
        }
        return array_push($this->listaJavascript, PATH_CDN . "/" . $javascript);
    }

    public function gerar() {
        return $this->gerarEstrutura();
    }

}
