<?php

namespace Views\Estruturas;

/**
 * Description of Head
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class Head {

    private $titulo;
    private $descricao;
    private $palavrasChave;
    private $listaCSS;
    private $corTema;

    private $icone;
    
    public function __construct(string $titulo = null, string $descricao = null) {
        $this->titulo = $titulo;
        $this->descricao = $descricao;
        $this->palavrasChave = array();
        $this->listaCSS = array();
        $this->corTema = "#ffffff";
    }

    public function addCSS(string $css, bool $externo = false) {
        if ($externo) {
            return array_push($this->listaCSS, $css);
        }
        return array_push($this->listaCSS, PATH_CDN . "/" . $css);
    }

    public function addPalavrasChave(string $palavra) {
        array_push($this->palavrasChave, $palavra);
    }

    public function gerar() {
        $estrutura = "<html lang=\"pt-br\">";
        $estrutura .= "\n\t<head>";
        $estrutura .= "\n\t\t<meta charset=\"UTF-8\">";
        $estrutura .= "\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
        $estrutura .= "\n\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">";
        $estrutura .= "\n\t\t<title>" . sprintf("%s :: %s", $this->titulo, EVENTO_SIGLA) . "</title>";

        if ($this->descricao != null) {
            $estrutura .= "\n\t\t<meta name=\"description\" content=\"" . $this->descricao . "\"/>";
        }

        if ($this->palavrasChave != null) {
            $estrutura .= "\n\t\t<meta name=\"keywords\" content=\"" . implode($this->palavrasChave, ", ") . "\" />";
        }

        $estrutura .= "\n\t\t<meta name=\"author\" content=\"Gustavo Bittencourt Satheler e Michael Luis de N. Martins | GM Development\" />";
//        $estrutura .= "\n\t\t<link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"" . PATH_CDN . "/imagens/favicons/apple-touch-icon.png\"/>";
//        $estrutura .= "\n\t\t<link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"" . PATH_CDN . "/imagens/favicons/favicon-32x32.png\"/>";
//        $estrutura .= "\n\t\t<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"" . PATH_CDN . "/imagens/favicons/favicon-16x16.png\"/>";
//        $estrutura .= "\n\t\t<link rel=\"manifest\" href=\"" . PATH_CDN . "/imagens/favicons/manifest.json\"/>";
//        $estrutura .= "\n\t\t<link rel=\"mask-icon\" href=\"" . PATH_CDN . "/imagens/favicons/safari-pinned-tab.svg\" color=\"" . $this->corTema . "\"/>";
//        $estrutura .= "\n\t\t<meta name=\"theme-color\" content=\"" . $this->corTema . "\"/>";
        
        if (count($this->listaCSS) > 0) {
            $estrutura .= "\n\n\t\t<!-- Cascading Style Sheets -->";
        }
        foreach ($this->listaCSS as $css) {
            $estrutura .= "\n\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $css . "\">";
        }

        $estrutura .= "\n\t</head>";

        return $estrutura;
    }

    public function getCorTema() {
        return $this->corTema;
    }

    public function setCorTema(string $corTema) {
        $this->corTema = $corTema;
    }
    
    public function setIcone(string $icone){
        $this->icone = $icone;
    }
    
    public function setTitulo(string $titulo){
        $this->titulo = $titulo;
    }
}
