    <body>
        <div class="login">
            <div class="login_title">
                <span>Painel do Participante</span>
            </div>
            <div class="login_fields">
                <div class="login_fields__user">
                    <div class="icon">
                        <img src="<?php echo URL . "views/cdn/imagens/icones/" ?>user.png">
                    </div>
                    <input name="participante_email" class="hidden" type="text">
                    <input type="text" name="participante_email" id="participante_email" placeholder="Insira seu e-mail">
                    <div class="validation">
                        <img src="<?php echo URL . "views/cdn/imagens/icones/" ?>check.png">
                    </div>
                </div>
                <div class="login_fields__password">
                    <div class="icon">
                        <img src="<?php echo URL . "views/cdn/imagens/icones/" ?>lock.png">
                    </div>
                    <input type="password" name="participante_senha" id="participante_senha" placeholder="Insira sua senha">
                    <div class="validation">
                        <img src="<?php echo URL . "views/cdn/imagens/icones/" ?>check.png">
                    </div>
                </div>
                <div class="login_fields__message">
                    <a id="retorno">&nbsp;</a>
                </div>
                <div class="login_fields__submit">
                    <input type="submit" value="Entrar">
                    <div class="forgot">
                        <a data-toggle="modal" class="pointer" data-target="#modalRealizarInscricao">Esqueceu sua senha?</a>
                    </div>
                </div>
            </div>
            <div class="success">
                <h2 id="msgaut"></h2>
                <p id="bemvindo"></p>
            </div>
        </div>
        <div class="authent">
            <img src="<?php echo URL . "views/cdn/svg/" ?>puff.svg">
            <p>Autenticando...</p>
        </div>
    </body>
