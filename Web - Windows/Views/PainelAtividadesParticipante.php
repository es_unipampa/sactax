<div class="container-fluid">
    <div class="card card-nav-tabs card-plain">
        <div class="card-header card-header-success">
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">

                        <?php
                        $api = new \Controllers\APIController();

                        $atividades = json_decode($api->consultarTodasAtividadesParticipante($_SESSION["usuario"]["pessoa"]["token"]), true);
                        $vagasAtividades = json_decode($api->consultarQuantidadeInscritosPorAtividade(), true);

                        $separador = 0;

                        $diaSemanaAtual = new DateTime($atividades[0]["inicio"]);
                        $diaSemanaAtual = diaSemana($diaSemanaAtual);

                        echo '<li class = "nav-item">';
                        echo '<a class="nav-link active" href="#' . $diaSemanaAtual . '" data-toggle="tab">' . $diaSemanaAtual . '</a>';
                        echo '</li>';

                        for ($i = 0; $i < count($atividades); $i++) {
                            if (diaSemana(new DateTime($atividades[$i]["inicio"])) != $diaSemanaAtual) {
                                $diaSemanaAtual = diaSemana(new DateTime($atividades[$i]["inicio"]));
                                echo '<li class = "nav-item">';
                                echo '<a class="nav-link" href="#' . $diaSemanaAtual . '" data-toggle="tab">' . $diaSemanaAtual . '</a>';
                                echo '</li>';
                            }
                        }

                        echo '</div>';
                        echo '</div>';

                        echo '</ul></div></div></div><div class="card-body "><div class="tab-content text-center">';

                        $diaSemanaAtual = new DateTime($atividades[0]["inicio"]);
                        $diaSemanaAtual = diaSemana($diaSemanaAtual);

                        echo '<div class="tab-pane active" id="' . $diaSemanaAtual . '">';
                        echo '<div class="row">';

                        for ($i = 0; $i < count($atividades); $i++) {

                            if (diaSemana(new DateTime($atividades[$i]["inicio"])) == $diaSemanaAtual) {
                                $separador++;

                                $idatividade = $atividades[$i]["idAtividade"];
                                $nome = $atividades[$i]["nome"];
                                $tipoAtividade = $atividades[$i]["tipoatividade"];
                                $descricao = $atividades[$i]["descricao"];
                                $ministrante = $atividades[$i]["ministrante"];

                                $inicio = new DateTime($atividades[$i]["inicio"]);

                                $diaSemana = diaSemana($inicio);
                                $dia = $inicio->format("d");
                                $mes = nomeMes($inicio);
                                $ano = $inicio->format("Y");

                                $horarioInicio = $inicio->format("H:i");
                                $horarioFim = (new DateTime($atividades[$i]["fim"]))->format("H:i");

                                $local = $atividades[$i]["local"];
                                $vagas = $atividades[$i]["limiteVagas"];

                                $vagasRestantes = (((int) $vagas) - ((int) $vagasAtividades[$i]["vagas"]));

                                if ($vagasRestantes > 0) {
                                    $msgVagas = "Vagas restantes: " . $vagasRestantes;
                                } else {
                                    $msgVagas = "Vagas restantes: 0 - porém há fila de espera.";
                                }

                                $estaInscrito = $atividades[$i]["estaInscrito"];

                                echo '<div class="col-md-4">
                                <div class="card">
                                    <div class="card-header card-header-success">
                                        <h4 class="card-title">' . $nome . '</h4>
                                    </div>
                                    <div class="card-body">
                                        <p class="card-description">
                                            ' . sprintf("%s - %s", $tipoAtividade, $descricao) . '
                                        </p>
                                        <div class="form-inline">
                                            <i class="material-icons">person</i>
                                            <h6 class="adjust">Com ' . $ministrante . '</h6>
                                        </div>
                                        <div class="form-inline">
                                            <i class="material-icons">calendar_today</i>
                                            <h6 class="adjust">' . sprintf("%s, %s de %s de %s", $diaSemana, $dia, $mes, $ano) . '</h6>
                                        </div>
                                        <div class="form-inline">
                                            <i class="material-icons">alarm</i>
                                            <h6 class="adjust"> ' . sprintf("%s - %s", $horarioInicio, $horarioFim) . ' </h6>
                                        </div>
                                        <div class="form-inline">
                                            <i class="material-icons">location_on</i>
                                            <h6 class="adjust">' . $local . '</h6>
                                        </div>
                                        <div class="form-inline">
                                            <i class="material-icons">people</i>
                                            <h6 class="adjust">' . $msgVagas . '</h6>
                                        </div>
                                        <div class="form-inline">
                                            <i class="material-icons">people</i>
                                            <h6 class="adjust">Total de vagas: ' . $vagas . '</h6>
                                        </div>
                                        <br />';
                                if ($estaInscrito) {
                                    echo '<button onclick="inscrever(' . $idatividade . ', this)" type="submit" class="btn btn-danger pull-right"><i class="material-icons">clear</i> Cancelar inscrição<div class="ripple-container"></div></button>';
                                } else {
                                    echo '<button onclick="inscrever(' . $idatividade . ', this)" type="submit" class="btn btn-success pull-right"><i class="material-icons">check</i> Inscrever-se<div class="ripple-container"></div></button>';
                                }

                                echo '</div>
                                        </div>
                                    </div>';

                                if ($separador == 3) {
                                    echo '</div>';
                                    echo '<div class="row">';
                                    $separador = 0;
                                }
                            }
                        }

                        echo '</div>';
                        echo '</div>';

                        for ($i = 0; $i < count($atividades); $i++) {
                            if (diaSemana(new DateTime($atividades[$i]["inicio"])) != $diaSemanaAtual) {
                                $diaSemanaAtual = diaSemana(new DateTime($atividades[$i]["inicio"]));
                                echo '<div class="tab-pane" id="' . $diaSemanaAtual . '">';
                                echo '<div class="row">';
                                for ($j = 0; $j < count($atividades); $j++) {

                                    if (diaSemana(new DateTime($atividades[$j]["inicio"])) == $diaSemanaAtual) {
                                        $separador++;

                                        $idatividade = $atividades[$j]["idAtividade"];
                                        $nome = $atividades[$j]["nome"];
                                        $tipoAtividade = $atividades[$j]["tipoatividade"];
                                        $descricao = $atividades[$j]["descricao"];
                                        $ministrante = $atividades[$j]["ministrante"];

                                        $inicio = new DateTime($atividades[$j]["inicio"]);

                                        $diaSemana = diaSemana($inicio);
                                        $dia = $inicio->format("d");
                                        $mes = nomeMes($inicio);
                                        $ano = $inicio->format("Y");

                                        $horarioInicio = $inicio->format("H:i");
                                        $horarioFim = (new DateTime($atividades[$j]["fim"]))->format("H:i");

                                        $local = $atividades[$j]["local"];

                                        $vagas = $atividades[$j]["limiteVagas"];
                                        $vagasRestantes = (((int) $vagas) - ((int) $vagasAtividades[$i]["vagas"]));

                                        if ($vagasRestantes > 0) {
                                            $msgVagas = "Vagas restantes: " . $vagasRestantes;
                                        } else {
                                            $msgVagas = "Vagas restantes: 0 - porém há fila de espera.";
                                        }

                                        $estaInscrito = $atividades[$j]["estaInscrito"];

                                        echo '<div class="col-md-4">
                                <div class="card">
                                    <div class="card-header card-header-success">
                                        <h4 class="card-title">' . $nome . '</h4>
                                    </div>
                                    <div class="card-body">
                                        <p class="card-description">
                                            ' . sprintf("%s - %s", $tipoAtividade, $descricao) . '
                                        </p>
                                        <div class="form-inline">
                                            <i class="material-icons">person</i>
                                            <h6 class="adjust">Com ' . $ministrante . '</h6>
                                        </div>
                                        <div class="form-inline">
                                            <i class="material-icons">calendar_today</i>
                                            <h6 class="adjust">' . sprintf("%s, %s de %s de %s", $diaSemana, $dia, $mes, $ano) . '</h6>
                                        </div>
                                        <div class="form-inline">
                                            <i class="material-icons">alarm</i>
                                            <h6 class="adjust"> ' . sprintf("%s - %s", $horarioInicio, $horarioFim) . ' </h6>
                                        </div>
                                        <div class="form-inline">
                                            <i class="material-icons">location_on</i>
                                            <h6 class="adjust">' . $local . '</h6>
                                        </div>
                                        <div class="form-inline">
                                            <i class="material-icons">people</i>
                                            <h6 class="adjust">' . $msgVagas . '</h6>
                                        </div>
                                        <div class="form-inline">
                                            <i class="material-icons">people</i>
                                            <h6 class="adjust">Vagas: ' . $vagas . '</h6>
                                        </div>
                                        <br />';
                                        if ($estaInscrito) {
                                            echo '<button onclick="inscrever(' . $idatividade . ', this)" type="submit" class="btn btn-danger pull-right"><i class="material-icons">clear</i> Cancelar inscrição<div class="ripple-container"></div></button>';
                                        } else {
                                            echo '<button onclick="inscrever(' . $idatividade . ', this)" type="submit" class="btn btn-success pull-right"><i class="material-icons">check</i> Inscrever-se<div class="ripple-container"></div></button>';
                                        }

                                        echo '</div>
                                        </div>
                                    </div>';

                                        if ($separador == 3) {
                                            echo '</div>';
                                            echo '<div class="row">';
                                            $separador = 0;
                                        }
                                    }
                                }
                                $separador = 0;
                                echo '</div></div>';
                            }
                        }
                        ?>
                </div>
            </div>
        </div>


    </div>
</div>
</div>
</div>
</div>                                    
</div>
</body>