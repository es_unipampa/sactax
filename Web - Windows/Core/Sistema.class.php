<?php

namespace Core;

use Models\Evento;
use \InvalidArgumentException;
use \PDOException;

defined("ACESSO_DIRETO") or die("<h1>Access denied</h1>");

class Sistema {

    private $controlador;
    private $acao;
    private $parametros;

    public function __construct(string $controlador = "Home") {
        $this->controlador = $controlador;
    }

    private function makeController() {
        $this->get_url_data();

        $controlador = "Controllers\\" . $this->classeController();
        $caminhoControlador = $this->caminhoController($this->classeController());

        if (!file_exists($caminhoControlador)) {
            return "Caminho do controlador não encontrado: " . $caminhoControlador;
        }

        require_once($caminhoControlador);

        if (!class_exists($controlador)) {
            return "Classe do controlador não encontrado";
        }

        $this->controlador = new $controlador(); //HomeController();
        $this->controlador->setParametros($this->parametros);

        $this->acao = preg_replace('/[^a-zA-Z]/i', '', $this->acao);

        try {
            if (method_exists($this->controlador, $this->acao)) {
                return $this->controlador->{$this->acao}();
            }

            if (!$this->acao && method_exists($this->controlador, 'index')) {
                return $this->controlador->index();
            }
        } catch (InvalidArgumentException $ex) {
            return header(sprintf("HTTP/1.1 %d %s", $ex->getCode(), utf8_decode($ex->getMessage())));
        } catch (PDOException $ex) {
            switch ($ex->errorInfo[1]) {
                default:
                    $mensagem = $ex->getMessage();
                    break;
                case 1062:
                    $mensagem = "Já existe um participante cadastrado com o e-mail informado.";
                    break;
            }
            
            return header(sprintf("HTTP/1.1 400 %s", utf8_decode((new \Views\Estruturas\JSON())->gerar($mensagem))));
        }

        // GERAR UMA PÁGINA DE ERRO
        echo("Página não encontrada.");
    }

    private function classeController() {
        $this->controlador = preg_replace('/[^a-zA-Z]/i', '', $this->controlador);
        return $this->controlador . "Controller";
    }

    private function caminhoController(string $controller) {
        return PATH_CONTROLLERS . '\\' . $controller . '.class.php';
    }

    private function debugSistema(string $caminhoControlador, string $area) {
        debug("Dados Sistema", $area);
        debug("Controller", ($this->controlador instanceof Controller) ? __NAMESPACE__ . "/" . get_class($this->controlador) : __NAMESPACE__ . "/" . $this->controlador);
        debug("Ação", $this->acao);
        debug("Parâmetros", $this->parametros);
        debug("Caminho controlador", preg_replace("/\\\\/", "/", $caminhoControlador));
    }

    public function get_url_data() {
        if (isset($_GET['caminho'])) {
            $caminho = $_GET['caminho'];
            $caminho = rtrim($caminho, '/');
            $caminho = filter_var($caminho, FILTER_SANITIZE_URL);
            $caminho = explode('/', $caminho);
            $this->controlador = chk_array($caminho, 0);
            $this->acao = chk_array($caminho, 1);

            if (chk_array($caminho, 2)) {
                unset($caminho[0]);
                unset($caminho[1]);

                $this->parametros = array_values($caminho);
            }
        }
    }

    public function exibir() {
        echo($this->makeController());
    }
}