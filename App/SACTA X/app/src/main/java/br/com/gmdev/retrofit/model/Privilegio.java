package br.com.gmdev.retrofit.model;

import com.google.gson.annotations.SerializedName;

public class Privilegio {
    @SerializedName("idPrivilegio")
    private String idPrivilegio;

    @SerializedName("privilegio")
    private String privilegio;

    public Privilegio(String idPrivilegio, String privilegio) {
        this.idPrivilegio = idPrivilegio;
        this.privilegio = privilegio;
    }

    public String getIdPrivilegio() {
        return idPrivilegio;
    }

    public void setIdPrivilegio(String idPrivilegio) {
        this.idPrivilegio = idPrivilegio;
    }

    public String getPrivilegio() {
        return privilegio;
    }

    public void setPrivilegio(String privilegio) {
        this.privilegio = privilegio;
    }

    @Override
    public String toString() {
        return "Privilegio{" +
                "idPrivilegio='" + idPrivilegio + '\'' +
                ", privilegio='" + privilegio + '\'' +
                '}';
    }
}
