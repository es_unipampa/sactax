package br.com.gmdev.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import retrofit2.http.FormUrlEncoded;

public class Atividade {

    @SerializedName("idAtividade")
    private String idAtividade;

    @SerializedName("nome")
    private String nome;

    @SerializedName("descricao")
    private String descricao;

    @SerializedName("inicio")
    private String inicio;

    @SerializedName("fim")
    private String fim;

    @SerializedName("tipoatividade")
    private String tipoatividade;

    @SerializedName("local")
    private String local;

    @SerializedName("limitevagas")
    private int limitevagas;

    @SerializedName("ministrante")
    private String ministrante;

    public Atividade(String idAtividade, String nome, String descricao, String inicio, String fim, String tipoatividade, String local, int limitevagas, String ministrante) {
        this.idAtividade = idAtividade;
        this.nome = nome;
        this.descricao = descricao;
        this.inicio = inicio;
        this.fim = fim;
        this.tipoatividade = tipoatividade;
        this.local = local;
        this.limitevagas = limitevagas;
        this.ministrante = ministrante;
    }

    public String getIdAtividade() {
        return idAtividade;
    }

    public void setIdAtividade(String idAtividade) {
        this.idAtividade = idAtividade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFim() {
        return fim;
    }

    public void setFim(String fim) {
        this.fim = fim;
    }

    public String getTipoatividade() {
        return tipoatividade;
    }

    public void setTipoatividade(String tipoatividade) {
        this.tipoatividade = tipoatividade;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public int getLimitevagas() {
        return limitevagas;
    }

    public void setLimitevagas(int limitevagas) {
        this.limitevagas = limitevagas;
    }

    public String getMinistrante() {
        return ministrante;
    }

    public void setMinistrante(String ministrante) {
        this.ministrante = ministrante;
    }

    @Override
    public String toString() {
        return "Atividade{" +
                "idAtividade='" + idAtividade + '\'' +
                ", nome='" + nome + '\'' +
                ", descricao='" + descricao + '\'' +
                ", inicio='" + inicio + '\'' +
                ", fim='" + fim + '\'' +
                ", tipoatividade='" + tipoatividade + '\'' +
                ", local='" + local + '\'' +
                ", limitevagas=" + limitevagas +
                ", ministrante='" + ministrante + '\'' +
                '}';
    }
}
