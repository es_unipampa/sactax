package br.com.gmdev.retrofit.model;

import com.google.gson.annotations.SerializedName;

public class Participantes {
    @SerializedName("idpessoa")
    private String idpessoa;

    @SerializedName("nome")
    private String nome;

    public Participantes(String idpessoa, String nome) {
        this.idpessoa = idpessoa;
        this.nome = nome;
    }

    public String getIdpessoa() {
        return idpessoa;
    }

    public void setIdpessoa(String idpessoa) {
        this.idpessoa = idpessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Participantes{" +
                "idpessoa='" + idpessoa + '\'' +
                ", nome='" + nome + '\'' +
                '}';
    }
}
