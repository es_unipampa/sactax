package br.com.gmdev.retrofit.activities;

import br.com.gmdev.retrofit.model.*;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.IllegalFormatCodePointException;

import br.com.gmdev.retrofit.R;
import br.com.gmdev.retrofit.api.APIService;
import br.com.gmdev.retrofit.api.APIUrl;
import br.com.gmdev.retrofit.helper.SharedPrefManager;
import br.com.gmdev.retrofit.model.Pessoa;
import br.com.gmdev.retrofit.model.RetornoUsuario;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private AnimationDrawable animationDrawable;
    private LinearLayout linearLayout;
    private EditText editTextEmail, editTextPassword;
    private Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //CONFIGURAÇÕES DE BACKGROUND
        linearLayout = (LinearLayout) findViewById(R.id.LinearAnimationLogin);
        animationDrawable = (AnimationDrawable) linearLayout.getBackground();
        animationDrawable.setEnterFadeDuration(4000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();

        if (Build.VERSION.SDK_INT > 19) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        } else {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        editTextEmail = (EditText) findViewById(R.id.emailLogin);
        editTextPassword = (EditText) findViewById(R.id.senhaLogin);

        buttonLogin = (Button) findViewById(R.id.btnEntrar);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userSignIn();
            }
        });
    }

    private void userSignIn() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Realizando login...");
        progressDialog.show();

        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<RetornoUsuario> call = service.realizarLogin(email, password);

        call.enqueue(new Callback<RetornoUsuario>() {
            @Override
            public void onResponse(Call<RetornoUsuario> call, Response<RetornoUsuario> response) {
                progressDialog.dismiss();

                if (response.code() == 200) {
                    call.cancel();

                    Privilegio privilegio = response.body().getUsuarioComp().getPrivilegio();
                    int idPrivilegioLogin = Integer.parseInt(privilegio.getIdPrivilegio());

                    if (idPrivilegioLogin != 1){
                        System.out.println(response.body().getUsuarioComp());
                        SharedPrefManager.getInstance(getApplicationContext()).loginUser(response.body().getUsuarioComp().getPessoa());
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        //starting profile activity
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(), "Esse recurso está disponível apenas para organização do evento.", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "Em breve terá uma área para participantes :)", Toast.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        JSONObject j = new JSONObject(response.message());
                        Toast.makeText(getApplicationContext(), j.getString("retorno"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<RetornoUsuario> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
