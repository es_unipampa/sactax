package br.com.gmdev.retrofit.model;

import com.google.gson.annotations.SerializedName;

public class ParticipantesPOST {
    @SerializedName("idpessoa")
    private String idpessoa;

    @SerializedName("nome")
    private String nome;

    @SerializedName("inscricao")
    private String inscricao;

    @SerializedName("posicao")
    private String posicao;

    public ParticipantesPOST(String idpessoa, String nome, String inscricao, String posicao) {
        this.idpessoa = idpessoa;
        this.nome = nome;
        this.inscricao = inscricao;
        this.posicao = posicao;
    }

    public String getIdpessoa() {
        return idpessoa;
    }

    public void setIdpessoa(String idpessoa) {
        this.idpessoa = idpessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getInscricao() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao = inscricao;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    @Override
    public String toString() {
        return "ParticipantesPOST{" +
                "idpessoa='" + idpessoa + '\'' +
                ", nome='" + nome + '\'' +
                ", inscricao='" + inscricao + '\'' +
                ", posicao='" + posicao + '\'' +
                '}';
    }
}
