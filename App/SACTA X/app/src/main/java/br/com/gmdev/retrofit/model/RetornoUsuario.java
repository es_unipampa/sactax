package br.com.gmdev.retrofit.model;

import com.google.gson.annotations.SerializedName;

public class RetornoUsuario {

    @SerializedName("retorno")
    private UsuarioComp usuarioComp;

    public RetornoUsuario(UsuarioComp usuarioComp) {
        this.usuarioComp = usuarioComp;
    }

    public UsuarioComp getUsuarioComp() {
        return usuarioComp;
    }
}