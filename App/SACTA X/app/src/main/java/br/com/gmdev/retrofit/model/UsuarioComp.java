package br.com.gmdev.retrofit.model;

public class UsuarioComp {
    private Pessoa pessoa;
    private String telefone;
    private Privilegio privilegio;
    private Categoria categoria;
    private Situacao situacao;

    public UsuarioComp(Pessoa pessoa, String telefone, Privilegio privilegio, Categoria categoria, Situacao situacao) {
        this.pessoa = pessoa;
        this.telefone = telefone;
        this.privilegio = privilegio;
        this.categoria = categoria;
        this.situacao = situacao;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Privilegio getPrivilegio() {
        return privilegio;
    }

    public void setPrivilegio(Privilegio privilegio) {
        this.privilegio = privilegio;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    @Override
    public String toString() {
        return "UsuarioComp{" +
                "pessoa=" + pessoa +
                ", telefone='" + telefone + '\'' +
                ", privilegio=" + privilegio +
                ", categoria=" + categoria +
                ", situacao=" + situacao +
                '}';
    }

}
