package br.com.gmdev.retrofit.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.gmdev.retrofit.R;
import br.com.gmdev.retrofit.api.APIService;
import br.com.gmdev.retrofit.api.APIUrl;
import br.com.gmdev.retrofit.helper.ParticipantesAdapter;
import br.com.gmdev.retrofit.helper.SharedPrefManager;
import br.com.gmdev.retrofit.model.ParticipantesPOST;
import br.com.gmdev.retrofit.model.ParticipantesPorAtividadePOST;
import br.com.gmdev.retrofit.model.Retorno;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ParticipantesPorAtividade extends AppCompatActivity {

    private Bundle bundle_receber;
    private TextView statusAtividade;
    private Button iniciarAtividade;
    private Button finalizarAtividade;
    private Button btnQr;

    private RecyclerView recyclerViewParticipantes;
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participantes_por_atividade);

        statusAtividade = (TextView) findViewById(R.id.statusAtividade);
        iniciarAtividade = (Button) findViewById(R.id.iniciarAtividade);
        finalizarAtividade = (Button) findViewById(R.id.finalizarAtividade);
        btnQr = (Button) findViewById(R.id.btnQr);

        recyclerViewParticipantes = (RecyclerView) findViewById(R.id.recyclerViewParticipantesPorAtividade);
        recyclerViewParticipantes.setHasFixedSize(true);
        recyclerViewParticipantes.setLayoutManager(new LinearLayoutManager(ParticipantesPorAtividade.this));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<ParticipantesPorAtividadePOST> call = service.getParticipantesPorAtividade(
                recuperarIdAtividade()
        );
        call.enqueue(new Callback<ParticipantesPorAtividadePOST>() {
            @Override
            public void onResponse(Call<ParticipantesPorAtividadePOST> call, Response<ParticipantesPorAtividadePOST> response) {
                if (response.code() == 200){
                    call.cancel();
                    if (response.body() != null){
                        List<ParticipantesPOST> participantesList = response.body().getParticipantesPorAtividade();
                        adapter = new ParticipantesAdapter(participantesList, getParent());
                        recyclerViewParticipantes.setAdapter(adapter);
                    }
                    System.out.println(response.body().toString());
                }else {
                    Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ParticipantesPorAtividadePOST> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Opa, " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        eventButton();
    }

    private String recuperarIdAtividade() {
        Intent intent = getIntent();
        bundle_receber = intent.getExtras();
        String id = bundle_receber.getString("idAtividade");
        return id;
    }

    private void eventButton(){
        iniciarAtividade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date dateAtual = Calendar.getInstance().getTime();
                String time = dateAtual.getHours() + ":" + dateAtual.getMinutes() + ":" + dateAtual.getSeconds();

                finalizarAtividade.setVisibility(View.VISIBLE);
                iniciarAtividade.setVisibility(View.GONE);
                statusAtividade.setText("INÍCIO: " + time);
            }
        });

        finalizarAtividade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date dateAtual = Calendar.getInstance().getTime();
                String time = dateAtual.getHours() + ":" + dateAtual.getMinutes() + ":" + dateAtual.getSeconds();
                finalizarAtividade.setVisibility(View.GONE);
                statusAtividade.setText("FIM: " + time);
            }
        });

        btnQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator intentIntegrator = new IntentIntegrator(ParticipantesPorAtividade.this);
                intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
                intentIntegrator.setOrientationLocked(false);
                intentIntegrator.setPrompt("CREDENCIAMENTO");
                intentIntegrator.setCameraId(0);
                intentIntegrator.initiateScan();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (intentResult != null){

            if (intentResult.getContents() != null){
                //Enviando token  para realizar credenciamento
                credenciarParticipanteNaAtividade( intentResult.getContents() );
            }else{
                Toast.makeText(getApplicationContext(), "Qr-Code Cancelado", Toast.LENGTH_LONG).show();
            }
        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void credenciarParticipanteNaAtividade(String tokenParticipante) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<Retorno> call = service.credenciarParticipanteAtividade(
                recuperarIdAtividade(),
                SharedPrefManager.recuperarUsuario().getToken(),
                tokenParticipante);

        call.enqueue(new Callback<Retorno>() {
            @Override
            public void onResponse(Call<Retorno> call, Response<Retorno> response) {
                if (response.code() == 200){
                    Toast.makeText(getApplicationContext(), response.body().getRetorno(), Toast.LENGTH_LONG).show();
                    call.cancel();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.message());
                        Toast.makeText(getApplicationContext(), jsonObject.getString("retorno"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), "Verifique se você está inscrito na Palestra/Minicurso.", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Retorno> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
