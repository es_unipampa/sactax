package br.com.gmdev.retrofit.model;

import com.google.gson.annotations.SerializedName;

public class Retorno {
    @SerializedName("retorno")
    private String retorno;

    public Retorno(String retorno){
        this.retorno = retorno;
    }

    public String getRetorno() {
        return retorno;
    }
}
