package br.com.gmdev.retrofit.helper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.gmdev.retrofit.R;
import br.com.gmdev.retrofit.activities.ParticipantesPorAtividade;
import br.com.gmdev.retrofit.model.Atividade;

public class AtividadeAdapter extends RecyclerView.Adapter<AtividadeAdapter.ViewHolder>{

    private List<Atividade> atividadesList;
    private Context mCtx;

    public AtividadeAdapter(List<Atividade> atividadesList, Context mCtx) {
        this.atividadesList = atividadesList;
        this.mCtx = mCtx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_atividades, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final AtividadeAdapter.ViewHolder holder, int position) {
        final Atividade atividades = atividadesList.get(position);

        holder.tituloAtividade.setText(atividades.getNome());
        holder.publicoAlvoAtividade.setText(atividades.getTipoatividade() + " - Público alvo: " + atividades.getNome());
        holder.ministranteAtividade.setText("COM: " + atividades.getMinistrante());
        holder.dataAtividade.setText("Início: " + atividades.getInicio() + "\n" + "Fim: " + atividades.getFim());
        holder.localAtividade.setText("Local: "+ atividades.getLocal());

        holder.visualizarAtividade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarIdAtividade(atividades.getIdAtividade());
            }
        });

    }


    @Override
    public int getItemCount() {
        return atividadesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tituloAtividade;
        public TextView publicoAlvoAtividade;
        public TextView ministranteAtividade;
        public TextView dataAtividade;
        public TextView localAtividade;
        public TextView vagasRestantesAtividade;
        public TextView vagasTotaisAtividade;
        public Button visualizarAtividade;

        public ViewHolder(View itemView) {
            super(itemView);

            tituloAtividade = (TextView) itemView.findViewById(R.id.tituloAtividade);
            publicoAlvoAtividade = (TextView) itemView.findViewById(R.id.publicoAlvoAtividade);
            ministranteAtividade = (TextView) itemView.findViewById(R.id.ministranteAtividade);
            dataAtividade = (TextView) itemView.findViewById(R.id.dataAtividade);
            localAtividade = (TextView) itemView.findViewById(R.id.localAtividade);
            visualizarAtividade = (Button) itemView.findViewById(R.id.visualizarAtividade);
        }
    }

    private void enviarIdAtividade(String idAtividade){
        Bundle bundle = new Bundle();
        bundle.putString("idAtividade", idAtividade);
        Intent intent = new Intent(mCtx, ParticipantesPorAtividade.class);
        intent.putExtras(bundle);
        mCtx.startActivity(intent);
    }
}
