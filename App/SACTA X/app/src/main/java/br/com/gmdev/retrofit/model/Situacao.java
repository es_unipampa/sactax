package br.com.gmdev.retrofit.model;

class Situacao {
    private String idSituacao;
    private String situacao;

    public Situacao(String idSituacao, String situacao) {
        this.idSituacao = idSituacao;
        this.situacao = situacao;
    }

    public String getIdSituacao() {
        return idSituacao;
    }

    public void setIdSituacao(String idSituacao) {
        this.idSituacao = idSituacao;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    @Override
    public String toString() {
        return "Situacao{" +
                "idSituacao=" + idSituacao +
                ", situacao='" + situacao + '\'' +
                '}';
    }
}
