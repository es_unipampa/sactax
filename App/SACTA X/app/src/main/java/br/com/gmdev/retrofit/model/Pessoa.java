package br.com.gmdev.retrofit.model;

public class Pessoa {

    private String token;
    private String nome;
    private String telefone;
    private String email;
    private String senha;
    private String confsenha;

    public Pessoa(String token, String nome, String telefone, String email, String senha, String confsenha) {
        this.token = token;
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.senha = senha;
        this.confsenha = confsenha;
    }

    public Pessoa(String nome, String telefone, String email, String senha, String confsenha) {
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.senha = senha;
        this.confsenha = confsenha;
    }

    public Pessoa(String token, String nome, String email){
        this.token = token;
        this.email = email;
        this.nome = nome;
    }

    public String getToken() {
        return token;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getSenha() {
        return senha;
    }

    public String getConfsenha() {
        return confsenha;
    }

    @Override
    public String toString() {
        return "Pessoa{" +
                "token='" + token + '\'' +
                ", nome='" + nome + '\'' +
                ", telefone='" + telefone + '\'' +
                ", email='" + email + '\'' +
                ", senha='" + senha + '\'' +
                ", confsenha='" + confsenha + '\'' +
                '}';
    }
}
