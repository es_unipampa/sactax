package br.com.gmdev.retrofit.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import br.com.gmdev.retrofit.R;
import br.com.gmdev.retrofit.api.APIService;
import br.com.gmdev.retrofit.api.APIUrl;
import br.com.gmdev.retrofit.helper.AtividadeAdapter;
import br.com.gmdev.retrofit.model.Atividade;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProgramacaoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class ProgramacaoFragment extends android.support.v4.app.Fragment {

    private RecyclerView recyclerViewAtividades;
    private RecyclerView.Adapter adapter;

    public ProgramacaoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_programacao, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("Programação");

        recyclerViewAtividades = (RecyclerView) view.findViewById(R.id.recyclerViewAtividades);
        recyclerViewAtividades.setHasFixedSize(true);
        recyclerViewAtividades.setLayoutManager(new LinearLayoutManager(getActivity()));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<List<Atividade>> call = service.getAtividades();

        call.enqueue(new Callback<List<Atividade>>() {
            @Override
            public void onResponse(Call<List<Atividade>> call, Response<List<Atividade>> response) {
                if (response.code() == 200) {
                    call.cancel();
                    if (response.body() != null){
                        adapter = new AtividadeAdapter(response.body(), getActivity());
                        recyclerViewAtividades.setAdapter(adapter);
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.message());
                        Toast.makeText(getContext(), jsonObject.getString("retorno"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        System.out.println("erro: " + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Atividade>> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
