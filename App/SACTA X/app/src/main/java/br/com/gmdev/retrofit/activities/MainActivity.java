package br.com.gmdev.retrofit.activities;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import br.com.gmdev.retrofit.R;
import br.com.gmdev.retrofit.helper.SharedPrefManager;

public class MainActivity extends AppCompatActivity {

    private Button buttonSignIn;
    private Button buttonSignUp;
    private Button buttonSobre;

    private AnimationDrawable animationDrawable;
    private LinearLayout LinearAnimationCadastro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {

            LinearAnimationCadastro = (LinearLayout) findViewById(R.id.LinearAnimationMain);
            animationDrawable = (AnimationDrawable) LinearAnimationCadastro.getBackground();
            animationDrawable.setEnterFadeDuration(4000);
            animationDrawable.setExitFadeDuration(4000);
            animationDrawable.start();

            if (Build.VERSION.SDK_INT > 19) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            } else {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            buttonSignIn = (Button) findViewById(R.id.buttonSignIn);
            buttonSignUp = (Button) findViewById(R.id.buttonSignUp);
            buttonSobre = (Button) findViewById(R.id.buttonAboutEvent);

            buttonSignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }
            });
            buttonSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), CadastroActivity.class));
                }
            });

            buttonSobre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), Sobre.class));
                }
            });

        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Opa, " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        if (SharedPrefManager.getInstance(this).estaLogado()) {
            finish();
            startActivity(new Intent(this, HomeActivity.class));
        }
        super.onStart();
    }

}
