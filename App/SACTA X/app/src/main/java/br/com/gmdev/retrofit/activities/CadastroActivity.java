package br.com.gmdev.retrofit.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.rtoshiro.util.format.MaskFormatter;
import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import br.com.gmdev.retrofit.R;
import br.com.gmdev.retrofit.api.APIService;
import br.com.gmdev.retrofit.api.APIUrl;
import br.com.gmdev.retrofit.helper.SharedPrefManager;
import br.com.gmdev.retrofit.model.Retorno;
import br.com.gmdev.retrofit.model.Pessoa;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CadastroActivity extends AppCompatActivity {

    private EditText editTextName,
            editTextTelefone,
            editTextEmail,
            editTextPassword,
            editTextConfirmPassword;

    private SimpleMaskFormatter simpleMaskFormatter;
    private MaskTextWatcher maskTextWatcher ;

    private Button buttonSignIn;

    private AnimationDrawable animationDrawable;
    private LinearLayout LinearAnimationCadastro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        try {

            LinearAnimationCadastro = (LinearLayout) findViewById(R.id.LinearAnimationCadastro);
            animationDrawable = (AnimationDrawable) LinearAnimationCadastro.getBackground();
            animationDrawable.setEnterFadeDuration(4000);
            animationDrawable.setExitFadeDuration(4000);
            animationDrawable.start();

            if (Build.VERSION.SDK_INT > 19 ){
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            }else {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            buttonSignIn = (Button) findViewById(R.id.buttonCadastro);

            editTextName = (EditText) findViewById(R.id.editTextName);
            editTextTelefone = (EditText) findViewById(R.id.editTextTelefone);
            editTextEmail = (EditText) findViewById(R.id.editTextEmail);
            editTextPassword = (EditText) findViewById(R.id.editTextPassword);
            editTextConfirmPassword = (EditText) findViewById(R.id.editTextConfirmPassword);

            //MASCARA DE FORMATAÇÃO TELEFONE
            simpleMaskFormatter = new SimpleMaskFormatter("(NN) N NNNN-NNNN");
            maskTextWatcher = new MaskTextWatcher(editTextTelefone, simpleMaskFormatter);
            editTextTelefone.addTextChangedListener(maskTextWatcher);

            buttonSignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    registrarUsuario();
                }
            });

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Opa, " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void registrarUsuario() {
        //defining a progress dialog to show while signing up
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Registrando usuário...");
        progressDialog.show();

        String nome = editTextName.getText().toString().trim();
        String telefone = editTextTelefone.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String senha = editTextPassword.getText().toString().trim();
        String confsenha = editTextConfirmPassword.getText().toString().trim();

        final OkHttpClient.Builder okHBuilder = new OkHttpClient.Builder();
        okHBuilder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                okhttp3.Response response = chain.proceed(request);

                MediaType mediaType = MediaType.parse("UTF-8");
                ResponseBody responseBody = ResponseBody.create(mediaType, response.body().bytes());
                okhttp3.Response responseModificado = response.newBuilder().body(responseBody).build();
                return responseModificado;
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHBuilder.build())
                .build();

        //Definindo API service RETROFIT
        APIService apiService = retrofit.create(APIService.class);

        //Contruindo objeto RETROFIT
        final Pessoa user = new Pessoa(
                nome,
                telefone,
                email,
                senha,
                confsenha
        );

        //Definindo a ligação
        Call<Retorno> call = apiService.registrarUsuario(
                user.getNome(),
                user.getTelefone(),
                user.getEmail(),
                user.getSenha(),
                user.getConfsenha()
        );

        //Ligando a API
        call.enqueue(new Callback<Retorno>() {
            @Override
            public void onResponse(Call<Retorno> call, Response<Retorno> response) {
                //Escondendo diálogo de processo
                progressDialog.dismiss();

                if (response.code() == 200) {
                    Toast.makeText(getApplicationContext(), response.body().getRetorno(), Toast.LENGTH_LONG).show();
                    call.cancel();
                    //starting profile activity
                    finish();
                }
                try {
                    JSONObject j = new JSONObject(response.message());
                    Toast.makeText(getApplicationContext(), j.getString("retorno"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Retorno> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Erro: " + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }
}
