package br.com.gmdev.retrofit.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.gmdev.retrofit.activities.ParticipantesPorAtividade;

public class ParticipantesPorAtividadePOST {
    @SerializedName("limite")
    private String limite;

    @SerializedName("participantes")
    private List<ParticipantesPOST> participantesPorAtividade;

    public ParticipantesPorAtividadePOST(String limite, List<ParticipantesPOST> participantesPorAtividade) {
        this.limite = limite;
        this.participantesPorAtividade = participantesPorAtividade;
    }

    public String getLimite() {
        return limite;
    }

    public void setLimite(String limite) {
        this.limite = limite;
    }

    public List<ParticipantesPOST> getParticipantesPorAtividade() {
        return participantesPorAtividade;
    }

    public void setParticipantesPorAtividade(List<ParticipantesPOST> participantesPorAtividade) {
        this.participantesPorAtividade = participantesPorAtividade;
    }

    @Override
    public String toString() {
        return "ParticipantesPorAtividadePOST{" +
                "limite='" + limite + '\'' +
                ", participantesPorAtividade=" + participantesPorAtividade +
                '}';
    }
}
