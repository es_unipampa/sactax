package br.com.gmdev.retrofit.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.gmdev.retrofit.R;
import br.com.gmdev.retrofit.model.Participantes;
import br.com.gmdev.retrofit.model.ParticipantesPOST;

public class ParticipantesAdapter extends RecyclerView.Adapter<ParticipantesAdapter.ViewHolder>{

    private List<ParticipantesPOST> participantes;
    private Context mCtx;

    public ParticipantesAdapter(List<ParticipantesPOST> participantes, Context mCtx) {
        this.participantes = participantes;
        this.mCtx = mCtx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_participantes, parent, false);
        return new ViewHolder(v);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ParticipantesAdapter.ViewHolder holder, int position) {
        ParticipantesPOST participantes = this.participantes.get(position);

        holder.posicaoParticipanteAtividade.setText(participantes.getPosicao());
        holder.nomeParticipanteAtividade.setText(participantes.getNome());

        return;
    }

    @Override
    public int getItemCount() {
        return participantes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView nomeParticipanteAtividade;
        public TextView posicaoParticipanteAtividade;

        public ViewHolder(View itemView) {
            super(itemView);

            posicaoParticipanteAtividade = (TextView) itemView.findViewById(R.id.posicaoParticipanteAtividade);
            nomeParticipanteAtividade = (TextView) itemView.findViewById(R.id.nomeParticipanteAtividade);
        }
    }
}
