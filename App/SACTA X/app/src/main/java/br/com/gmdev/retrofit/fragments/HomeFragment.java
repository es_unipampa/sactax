package br.com.gmdev.retrofit.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import br.com.gmdev.retrofit.R;
import br.com.gmdev.retrofit.model.Pessoa;


public class HomeFragment extends android.support.v4.app.Fragment {

    private RecyclerView recyclerViewParticipantes;
    private RecyclerView.Adapter adapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("SACTA X");

        super.onViewCreated(view, savedInstanceState);
    }
}
