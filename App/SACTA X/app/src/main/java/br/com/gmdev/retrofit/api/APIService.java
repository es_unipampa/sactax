package br.com.gmdev.retrofit.api;

import java.util.List;

import br.com.gmdev.retrofit.model.Atividade;
import br.com.gmdev.retrofit.model.ParticipantesPorAtividadePOST;
import br.com.gmdev.retrofit.model.Retorno;
import br.com.gmdev.retrofit.model.RetornoUsuario;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIService {
    @FormUrlEncoded
    @POST("cadastrarparticipante")
    Call<Retorno> registrarUsuario(
            @Field(value = "nome",      encoded = true) String nome,
            @Field(value = "telefone",  encoded = true) String telefone,
            @Field(value = "email",     encoded = true) String email,
            @Field(value = "senha",     encoded = true) String senha,
            @Field(value = "confsenha", encoded = true) String confsenha
    );

    @FormUrlEncoded
    @POST("autenticarusuario")
    Call<RetornoUsuario> realizarLogin(
            @Field(value = "email", encoded = true) String email,
            @Field(value = "senha", encoded = true) String senha
    );

    @FormUrlEncoded
    @POST("credenciarParticipanteAtividade")
    Call<Retorno> credenciarParticipanteAtividade(
            @Field(value = "idAtividade", encoded = true) String idAtividade,
            @Field(value = "tokenCredenciador", encoded = true) String tokenCredenciador,
            @Field(value = "tokenParticipante", encoded = true) String tokenParticipante
    );

    @FormUrlEncoded
    @POST("participantesPorAtividade")
    Call<ParticipantesPorAtividadePOST> getParticipantesPorAtividade(
            @Field(value = "idAtividade", encoded = true) String idAtividade
    );

    @GET("consultaratividades")
    Call<List<Atividade>> getAtividades();



}
